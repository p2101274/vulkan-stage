#version 460
#extension GL_EXT_ray_tracing : enable

struct RayPayloadType {
    vec3 color;  // unused
    vec3 normal;
    vec3 pos;
    float metal;
    float roughness;
    int hitType;
};  // type of the "payload" variable

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;



layout(set = 1, binding = 0) uniform writeonly image2D outTexture;
layout(set = 1, binding = 1, rgba8) uniform writeonly image2D dataTexture;

layout(set = 2, binding = 0) uniform accelerationStructureEXT topLevelAS;

layout(set = 3, binding = 0) uniform sampler basicSampler;

layout(set = 3, binding = 1) uniform Mat {
    vec4 color;
    float metallic;
    float roughness;
    int color_texture_id;
    int metallic_roughness_texture_id;
    int normalMap_texture_id;
    vec3 OFfset1;
    vec4 OFfset2;
}
materials[1000];

layout(set = 3, binding = 2) uniform texture2D textures[1000];

layout(location = 0) rayPayloadInEXT RayPayloadType payload;

void main() { /**** MISS SHADER ****/
    vec3 sun = normalize(vec3(-1, 6, -1));
    float ss = pow(max(dot(gl_WorldRayDirectionEXT, sun), 0.0), 2048.0);
    payload.color = mix(vec3(0.2, 0.6, 1), vec3(10.0, 10.0, 0.5), ss);
    payload.hitType = -1;
    payload.pos = vec3(1/0.0);
}
