#version 450
#extension GL_EXT_buffer_reference : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require

const float M_PI = 3.1415926538;

layout(location = 0) in vec3 fragPosWorld;
layout(location = 1) in vec3 fragNormalWorld;
layout(location = 2) in vec2 fraguv;
layout(location = 3) in flat uint fragmaterial;

layout(location = 0) out vec4 albedoAlpha;
layout(location = 1) out vec3 normal;
layout(location = 2) out vec3 worldPos;
layout(location = 3) out vec2 MR;

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;

layout(set = 1, binding = 0) uniform sampler basicSampler;

layout(set = 1, binding = 1) uniform Mat {
    vec4 color;
    float metallic;
    float roughness;
    int color_texture_id;
    int metallic_roughness_texture_id;
    int normalMap_texture_id;
    vec3 OFfset1;
    vec4 OFfset2;
}
materials[1000];

layout(set = 1, binding = 2) uniform texture2D textures[1000];

struct ObjectInfo {
    mat4 modelMatrix;
    mat4 normalMatrix;
};



layout(buffer_reference, scalar) readonly buffer InstanceBuffer2 { ObjectInfo objInfo[]; };
layout(push_constant) uniform constants { InstanceBuffer2 instances; }
PushConstants;

// http://www.thetenthplanet.de/archives/1180
mat3 cotangent_frame(vec3 N, vec3 p, vec2 uv) {
    vec3 dp1 = dFdx(p);
    vec3 dp2 = dFdy(p);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    vec3 dp2perp = cross(dp2, N);
    vec3 dp1perp = cross(N, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, N);
}

vec3 perturb_normal(vec3 N, vec3 V, int texId, vec2 texcoord) {
    // N, la normale interpolée et
    // V, le vecteur vue (vertex dirigé vers l'œil)
    vec3 map = textureLod(sampler2D(textures[texId], basicSampler), fraguv, 0).xyz;
    map = map * 255. / 127. - 128. / 127.;
    mat3 TBN = cotangent_frame(N, -V, texcoord);
    return normalize(TBN * map);
}

////////////////////////////////////////////////////////
//// PBR Function (from learnopengl) ///////////////////
////////////////////////////////////////////////////////

float distributionGGX(vec3 N, vec3 H, float roughness) {
    float a2 = roughness * roughness * roughness * roughness;
    float NdotH = max(dot(N, H), 0.0);
    float denom = (NdotH * NdotH * (a2 - 1.0) + 1.0);
    return a2 / (M_PI * denom * denom);
}

float geometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;
    return NdotV / (NdotV * (1.0 - k) + k);
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometrySchlickGGX(max(dot(N, L), 0.0), roughness) * geometrySchlickGGX(max(dot(N, V), 0.0), roughness);
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) { return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0); }

// Specular BRDF composition --------------------------------------------

vec3 PBR(vec3 L, vec3 V, vec3 N, vec3 lightColor, float metallic, float roughness, vec3 albedo, float attenuation) {
    vec3 H = normalize(V + L);
    vec3 F0 = mix(vec3(0.04), pow(albedo, vec3(2.2)), metallic);
    float NDF = distributionGGX(N, H, roughness);
    float G = geometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);
    vec3 kD = vec3(1.0) - F;
    kD *= 1.0 - metallic;

    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = numerator / max(denominator, 0.001);

    float NdotL = max(dot(N, L), 0.0);
    vec3 color = lightColor * attenuation * (kD * pow(albedo, vec3(2.2)) / M_PI + specular) * (NdotL);

    return color;
}
float dotClamp(vec3 v1, vec3 v2) { return clamp(dot(v1, v2), 0.0, 1.0); }

void main() {
    if (false) {
        mat4 test = PushConstants.instances.objInfo[0].modelMatrix;
    }
    vec4 textColor;
    vec2 metalRoughness;
    // vec3 ambiant = vec3(0.01);
    vec3 pos = ubo.invView[3].xyz;
    vec3 view = normalize(pos - fragPosWorld);
    // vec3 sun = normalize(vec3(-1, 1, -1));
    int texId = materials[fragmaterial].color_texture_id;
    // vec3 H = normalize(sun + view);
    if (texId != -1) {
        textColor = textureLod(sampler2D(textures[texId], basicSampler), fraguv, 0);
    } else {
        textColor = materials[fragmaterial].color;
    }
    vec3 surfaceNormal = normalize(fragNormalWorld);
    if (materials[fragmaterial].normalMap_texture_id != -1) {
        surfaceNormal = perturb_normal(surfaceNormal, view, materials[fragmaterial].normalMap_texture_id, fraguv);
    }
    if (materials[fragmaterial].metallic_roughness_texture_id != -1) {
        metalRoughness =
            textureLod(
                sampler2D(textures[materials[fragmaterial].metallic_roughness_texture_id], basicSampler), fraguv, 0)
                .rg;
    } else {
        metalRoughness = vec2(materials[fragmaterial].metallic, materials[fragmaterial].roughness);
    }
    if (textColor.a < 0.3) {
        discard;
    }
    albedoAlpha = vec4(textColor.xyz, 1);
    normal = surfaceNormal;
    worldPos = fragPosWorld.xyz;
    MR = metalRoughness;

    // color = PBR(sun, view, surfaceNormal, vec3(1, 1, 1), metalRoughness.r, metalRoughness.g, textColor.rgb, 2) + textColor.rgb * ambiant;
    // color = color / (color + vec3(1.0));
    // color = pow(color, vec3(1.6f / 2.2));
}