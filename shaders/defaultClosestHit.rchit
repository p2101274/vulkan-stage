#version 460
#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_buffer_reference : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require
#extension GL_EXT_debug_printf : enable
const float M_PI = 3.1415926538;

struct RayPayloadType {
    vec3 color;  // unused
    vec3 normal;
    vec3 pos;
    float metal;
    float roughness;
    int hitType;
};  // type of the "payload" variable

struct HitData {
    vec3 pos;
    vec3 normal;
    vec2 uv;
    int mat;
};

struct Ray {
    vec3 ro;
    vec3 rd;
};

struct Material {
    vec3 normal;
    float metal;
    vec3 color;
    float roughness;
};

struct normalData {
    vec3 dp1;
    vec3 dp2;
    vec2 duv1;
    vec2 duv2;
};

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;

// INPUT



layout(set = 1, binding = 0) uniform writeonly image2D outTexture;
layout(set = 1, binding = 1, rgba8) uniform writeonly image2D dataTexture;

layout(set = 2, binding = 0) uniform accelerationStructureEXT topLevelAS;

layout(set = 3, binding = 0) uniform sampler basicSampler;

layout(set = 3, binding = 1) uniform Mat {
    vec4 color;
    float metallic;
    float roughness;
    int color_texture_id;
    int metallic_roughness_texture_id;
    int normalMap_texture_id;
    vec3 OFfset1;
    vec4 OFfset2;
}
materials[1000];

struct ObjectInfo {
    mat4 modelMatrix;
    mat4 normalMatrix;
};

layout(set = 3, binding = 2) uniform texture2D textures[1000];

layout(buffer_reference, scalar) readonly buffer IndexBuffer { uint i[]; };
layout(buffer_reference, scalar) readonly buffer VertexBuffer { HitData d[]; };
layout(buffer_reference, scalar) readonly buffer InstanceBuffer2 { ObjectInfo objInfo[]; };

layout(push_constant) uniform constants {
    IndexBuffer indices;
    VertexBuffer vertexs;
    InstanceBuffer2 instances;
    int number_frame;
}
PushConstants;

// RT INOUT

layout(location = 0) rayPayloadInEXT RayPayloadType payload;
hitAttributeEXT vec2 attribs;

// This function will unpack our vertex buffer data into a single triangle and calculates uv coordinates
HitData getHitData(uint index, out normalData nd) {
    HitData outdata;
    HitData vertices[3];
    const uint triIndex = index * 3;

    // GeometryNode geometryNode = geometryNodes.nodes[gl_GeometryIndexEXT];

    // Indices indices   = Indices(geometryNode.indexBufferDeviceAddress);
    // Vertices vertices = Vertices(geometryNode.vertexBufferDeviceAddress);

    // Unpack vertices
    // Data is packed as vec4 so we can map to the glTF vertex structure from the host side
    // We match vkglTF::Vertex: pos.xyz+normal.x, normalyz+uv.xy
    // glm::vec3 pos;
    // glm::vec3 normal;
    // glm::vec2 uv;
    // ...
    uint triangle[3] = {PushConstants.indices.i[triIndex], PushConstants.indices.i[triIndex + 1], PushConstants.indices.i[triIndex + 2]};
    outdata.mat = PushConstants.vertexs.d[triangle[0]].mat;
    for (uint i = 0; i < 3; i++) {
        vertices[i] = PushConstants.vertexs.d[triangle[i]];
    }
    // Calculate values at barycentric coordinates
    vec3 barycentricCoords = vec3(1.0f - attribs.x - attribs.y, attribs.x, attribs.y);
    outdata.uv = vertices[0].uv * barycentricCoords.x + vertices[1].uv * barycentricCoords.y + vertices[2].uv * barycentricCoords.z;
    outdata.normal = normalize(
        vertices[0].normal * barycentricCoords.x + vertices[1].normal * barycentricCoords.y + vertices[2].normal * barycentricCoords.z);
    outdata.pos = vertices[0].pos * barycentricCoords.x + vertices[1].pos * barycentricCoords.y + vertices[2].pos * barycentricCoords.z;

    nd.dp1 = vertices[1].pos - vertices[0].pos;
    nd.dp2 = vertices[2].pos - vertices[0].pos;
    nd.duv1 = vertices[1].uv - vertices[0].uv;
    nd.duv2 = vertices[2].uv - vertices[0].uv;
    // /**
    // * uniquement pour du test
    // */
    // if(dot(outdata.normal, gl_WorldRayDirectionEXT) > 0){
    //     outdata.normal = - outdata.normal;
    // }

    return outdata;
}

mat3 cotangent_frame(vec3 N, vec2 uv, normalData nd) {
    vec3 dp1 = nd.dp1;
    vec3 dp2 = nd.dp2;
    vec2 duv1 = nd.duv1;
    vec2 duv2 = nd.duv2;

    vec3 dp2perp = cross(dp2, N);
    vec3 dp1perp = cross(N, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, N);
}

vec3 perturb_normal(vec3 N, int texId, vec2 texcoord, normalData nd) {
    // N, la normale interpolée et
    // V, le vecteur vue (vertex dirigé vers l'œil)
    vec3 map = textureLod(sampler2D(textures[texId], basicSampler), texcoord, 0).xyz;
    map = map * 255. / 127. - 128. / 127.;
    mat3 TBN = cotangent_frame(N, texcoord, nd);
    return normalize(TBN * map);
}

Material getMaterialData(int matID, vec3 normal, vec2 uv, normalData nd) {
    Material mat;
    int texId = materials[matID].color_texture_id;
    if (texId != -1) {
        mat.color = textureLod(sampler2D(textures[texId], basicSampler), uv, 0).rgb;
    } else {
        mat.color = materials[matID].color.rgb;
    }
    mat.normal = normalize(normal);
    if (materials[matID].normalMap_texture_id != -1) {
        mat.normal = perturb_normal(mat.normal, materials[matID].normalMap_texture_id, uv, nd);
    }
    if (materials[matID].metallic_roughness_texture_id != -1) {
        vec2 metalRoughness = textureLod(sampler2D(textures[materials[matID].metallic_roughness_texture_id], basicSampler), uv, 0).rg;
        mat.metal = metalRoughness.r;
        mat.roughness = metalRoughness.g;
    } else {
        vec2 metalRoughness = vec2(materials[matID].metallic, materials[matID].roughness);
        mat.metal = metalRoughness.r;
        mat.roughness = metalRoughness.g;
    }
    return mat;
}

void main() { /**** TRIANGLE CLOSEST HIT SHADER ****/
    // debugPrintfEXT("test");
    normalData nd;
    HitData d = getHitData(gl_PrimitiveID, nd);
    d.pos = vec3(PushConstants.instances.objInfo[gl_InstanceID].modelMatrix * vec4(d.pos, 1.0));
    d.normal = normalize(mat3(PushConstants.instances.objInfo[gl_InstanceID].normalMatrix) * d.normal);
    Material m = getMaterialData(d.mat, d.normal, d.uv, nd);

    vec3 sun = normalize(vec3(-0.5, 4, -1));

    uint rayFlags = gl_RayFlagsTerminateOnFirstHitEXT | gl_RayFlagsSkipClosestHitShaderEXT;
    float rayMin = 0.001;    // minimal distance for a ray hit
    float rayMax = 10000.0;  // maximum distance for a ray hit
    uint cullMask = 0xFFu;   // no culling
    Ray ray;

    ray.ro = d.pos + d.normal * 0.001;
    ray.rd = sun;
    traceRayEXT(topLevelAS, rayFlags, cullMask, 0u, 0u, 0u, ray.ro, rayMin, ray.rd, rayMax, 0);

    payload.color = m.color;
    payload.normal = m.normal;
    payload.pos = d.pos;
    payload.metal = m.metal;
    payload.roughness = m.roughness;
    if (payload.hitType != -1) {
        payload.hitType = 0;
    } else {
        payload.hitType = 1;
    }
}
