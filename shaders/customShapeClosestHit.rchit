#version 460
#extension GL_EXT_ray_tracing : enable
#extension GL_EXT_buffer_reference : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require
#extension GL_EXT_debug_printf : enable
const float M_PI = 3.1415926538;

struct RayPayloadType {
    vec3 color;  // unused
    vec3 normal;
    vec3 pos;
    float metal;
    float roughness;
    int hitType;
};  // type of the "payload" variable

struct Ray {
    vec3 ro;
    vec3 rd;
};

/**** COMMON END ****/

struct HitData {
    vec3 pos;
    vec3 normal;
    vec2 uv;
    uint mat;
};

struct Material {
    vec3 normal;
    float metal;
    vec3 color;
    float roughness;
};


layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;



layout(set = 1, binding = 0) uniform writeonly image2D outTexture;
layout(set = 1, binding = 1, rgba8) uniform writeonly image2D dataTexture;

layout(set = 2, binding = 0) uniform accelerationStructureEXT topLevelAS;

layout(set = 3, binding = 0) uniform sampler basicSampler;

layout(set = 3, binding = 1) uniform Mat {
    vec4 color;
    float metallic;
    float roughness;
    int color_texture_id;
    int metallic_roughness_texture_id;
    int normalMap_texture_id;
    vec3 OFfset1;
    vec4 OFfset2;
}
materials[1000];

struct ObjectInfo {
    mat4 modelMatrix;
    mat4 normalMatrix;
};

layout(set = 3, binding = 2) uniform texture2D textures[1000];

layout(buffer_reference, scalar) readonly buffer IndexBuffer { uint i[]; };
layout(buffer_reference, scalar) readonly buffer VertexBuffer { HitData d[]; };
layout(buffer_reference, scalar) readonly buffer InstanceBuffer2 { ObjectInfo objInfo[]; };

layout(push_constant) uniform constants {
    IndexBuffer indices;
    VertexBuffer vertexs;
    InstanceBuffer2 instances;
    int number_frame;
}
PushConstants;

struct hitAttribute {
    vec3 normal;
    vec3 pos;
    vec2 uv;
    uint material;
};

hitAttributeEXT hitAttribute hit;
// This function will unpack our vertex buffer data into a single triangle and calculates uv coordinates


// mat3 cotangent_frame(vec3 N, vec2 uv, normalData nd) {
//     vec3 dp1 = nd.dp1;
//     vec3 dp2 = nd.dp2;
//     vec2 duv1 = nd.duv1;
//     vec2 duv2 = nd.duv2;

//     vec3 dp2perp = cross(dp2, N);
//     vec3 dp1perp = cross(N, dp1);
//     vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
//     vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

//     float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
//     return mat3(T * invmax, B * invmax, N);
// }

// vec3 perturb_normal(vec3 N, int texId, vec2 texcoord, normalData nd) {
//     // N, la normale interpolée et
//     // V, le vecteur vue (vertex dirigé vers l'œil)
//     vec3 map = textureLod(sampler2D(textures[texId], basicSampler), texcoord, 0).xyz;
//     map = map * 255. / 127. - 128. / 127.;
//     mat3 TBN = cotangent_frame(N, texcoord, nd);
//     return normalize(TBN * map);
// }

Material getMaterialData(uint matID, vec3 normal, vec2 uv) {
    Material mat;
    int texId = materials[matID].color_texture_id;
    if (texId != -1) {
        mat.color = textureLod(sampler2D(textures[texId], basicSampler), uv, 0).rgb;
    } else {
        mat.color = materials[matID].color.rgb;
    }
    mat.normal = normalize(normal);
    // if (materials[matID].normalMap_texture_id != -1) {
    //     mat.normal = perturb_normal(mat.normal, materials[matID].normalMap_texture_id, uv, nd);
    // }
    if (materials[matID].metallic_roughness_texture_id != -1) {
        vec2 metalRoughness = textureLod(sampler2D(textures[materials[matID].metallic_roughness_texture_id], basicSampler), uv, 0).rg;
        mat.metal = metalRoughness.r;
        mat.roughness = metalRoughness.g;
    } else {
        vec2 metalRoughness = vec2(materials[matID].metallic, materials[matID].roughness);
        mat.metal = metalRoughness.r;
        mat.roughness = metalRoughness.g;
    }
    return mat;
}

layout(location = 0) rayPayloadInEXT RayPayloadType payload;


void main() { /**** CUSTOME SHAPE INTERSECTION SHADER ****/
    

 
    // d.pos = vec3(PushConstants.instances.objInfo[0].modelMatrix * vec4(hit.pos, 1.0));
    // d.normal = normalize(mat3(PushConstants.instances.objInfo[0].normalMatrix) * hit.normal);
    Material m = getMaterialData(hit.material, hit.normal, hit.uv);

    vec3 sun = normalize(vec3(-1, 6, -1));

    uint rayFlags = gl_RayFlagsTerminateOnFirstHitEXT | gl_RayFlagsSkipClosestHitShaderEXT; 
    float rayMin = 0.001;                // minimal distance for a ray hit
    float rayMax = 10000.0;              // maximum distance for a ray hit
    uint cullMask = 0xFFu;               // no culling
    Ray ray;

    ray.ro = hit.pos + hit.normal * 0.001;
    ray.rd = sun;

    traceRayEXT(topLevelAS, rayFlags, cullMask, 0u, 0u, 0u, ray.ro, rayMin, ray.rd, rayMax, 0);

    payload.color = m.color;
    payload.normal = m.normal;
    payload.pos = hit.pos;
    payload.metal =m.metal;
    payload.roughness = m.roughness;
    if (payload.hitType != -1){
        payload.hitType = 0;
    }else{
        payload.hitType = 1;
    }

}
