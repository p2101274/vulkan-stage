#version 450

#extension GL_EXT_buffer_reference : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require

struct Ray {
    vec3 ro;
    vec3 rd;
};

/**** COMMON END ****/

struct HitData {
    vec3 pos;
    vec3 normal;
    vec2 uv;
    uint mat;
};

struct ObjectInfo {
    mat4 modelMatrix;
    mat4 normalMatrix;
};

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;



layout(buffer_reference, scalar) readonly buffer IndexBuffer { uint i[]; };
layout(buffer_reference, scalar) readonly buffer VertexBuffer { HitData d[]; };
layout(buffer_reference, scalar) readonly buffer InstanceBuffer2 { ObjectInfo objInfo[]; };

layout(push_constant) uniform constants {
    IndexBuffer indices;
    VertexBuffer vertexs;
    InstanceBuffer2 instances;
    int numberOfIndex;
}
PushConstants;

const vec2 OFFSETS[3] = vec2[](
    vec2(-2, 2), vec2(0, -4.5), vec2(2, 2)

);

layout(location = 0) out vec2 fragOffset;

void main() {
    int m = PushConstants.numberOfIndex;
    uint z = PushConstants.indices.i[0];
    HitData b = PushConstants.vertexs.d[0];
    ObjectInfo zz = PushConstants.instances.objInfo[0];
    fragOffset = OFFSETS[gl_VertexIndex];
    gl_Position = vec4(fragOffset, 0, 1);
}