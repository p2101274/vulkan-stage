#version 460

const float M_PI = 3.1415926538;

layout(location = 0) in vec2 fragOffset;
layout(location = 0) out vec4 outColor;

struct PointLight {
    vec4 position;  // ignore w
    vec4 color;     // w is intensity
};

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;

layout(set = 0, binding = 1) uniform sampler2D albedoAlphaTexture;
layout(set = 0, binding = 2) uniform sampler2D normalTexture;
layout(set = 0, binding = 3) uniform sampler2D worldPosTexture;
layout(set = 0, binding = 4) uniform sampler2D MRTexture;

layout(push_constant) uniform Push { PointLight light; }
push;

mat3 rotationY(float angle) {
    float c = cos(angle);
    float s = sin(angle);
    return mat3(vec3(c, 0.0, -s), vec3(0.0, 1.0, 0.0), vec3(s, 0.0, c));
}
float dotClamp(vec3 v1, vec3 v2) { return clamp(dot(v1, v2), 0.0, 1.0); }

vec3 createRay(in ivec2 px) {
    // convert pixel to NDS
    vec2 pxNDS = ((vec2(px.x, 720 - px.y)) / vec2(1280, 720)) * 2. - 1.;

    // choose an arbitrary HitPoint in the viewing volume
    // z = -1 equals a HitPoint on the near plane, i.e. the screen
    vec3 HitPointNDS = vec3(pxNDS, 0.1);

    // as this is in homogenous space, add the last homogenous coordinate
    vec4 HitPointNDSH = vec4(HitPointNDS, 1.0);
    // transform by inverse projection to get the HitPoint in view space
    vec4 dirEye = inverse(ubo.projection) * HitPointNDSH;

    // since the camera is at the origin in view space by definition,
    // the current HitPoint is already the correct direction
    // (dir(0,P) = P - 0 = P as a direction, an infinite HitPoint,
    // the homogenous component becomes 0 the scaling done by the
    // w-division is not of interest, as the direction in xyz will
    // stay the same and we can just normalize it later
    dirEye.w = 0.;

    // compute world ray direction by multiplying the inverse view matrix
    vec3 rd = normalize((ubo.invView * dirEye).xyz);
    return rd;
}

////////////////////////////////////////////////////////
//// PBR Function (from learnopengl) ///////////////////
////////////////////////////////////////////////////////

float distributionGGX(vec3 N, vec3 H, float roughness) {
    float a2 = roughness * roughness * roughness * roughness;
    float NdotH = max(dot(N, H), 0.0);
    float denom = (NdotH * NdotH * (a2 - 1.0) + 1.0);
    return a2 / (M_PI * denom * denom);
}

float geometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;
    return NdotV / (NdotV * (1.0 - k) + k);
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    return geometrySchlickGGX(max(dot(N, L), 0.0), roughness) * geometrySchlickGGX(max(dot(N, V), 0.0), roughness);
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) { return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0); }

// Specular BRDF composition --------------------------------------------

vec3 PBR(vec3 L, vec3 V, vec3 N, vec3 lightColor, float metallic, float roughness, vec3 albedo, float attenuation) {
    vec3 H = normalize(V + L);
    vec3 F0 = mix(vec3(0.04), albedo, metallic);
    float NDF = distributionGGX(N, H, roughness);
    float G = geometrySmith(N, V, L, roughness);
    vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);
    vec3 kD = vec3(1.0) - F;
    kD *= 1.0 - metallic;

    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
    vec3 specular = numerator / max(denominator, 0.001);

    float NdotL = max(dot(N, L), 0.0);
    vec3 color = lightColor * attenuation * (kD * albedo / M_PI + specular) * (NdotL);

    return color;
}

void main() {
    ivec2 pixelPos = ivec2(gl_FragCoord.xy);
    vec4 basecolor = texelFetch(albedoAlphaTexture, pixelPos, 0);
    // if (basecolor.a < 0.3) {
    //     discard;
    // }
    vec3 normal = texelFetch(normalTexture, pixelPos, 0).rgb;
    vec3 fragPos = texelFetch(worldPosTexture, pixelPos, 0).rgb;
    vec2 MR = texelFetch(MRTexture, pixelPos, 0).rg;
    vec3 sun = normalize(vec3(push.light.position.xyz - fragPos));
    float d = distance(fragPos, push.light.position.xyz);
    vec3 pos = ubo.invView[3].xyz;
    vec3 view = normalize(pos - fragPos);
    vec3 color = vec3(0);
    vec3 ambiant = vec3(0.005);
    float attenuation = (1.0 / (d * d)) * push.light.color.w;

    if (basecolor.a > 0) {
        if (attenuation < 0.01) {
            discard;
        }

        // Traverse the acceleration structure and store information about the first intersection (if any)

        color = PBR(sun, view, normal, push.light.color.xyz, MR.r, MR.g, basecolor.rgb, attenuation);  //+ basecolor.rgb * ambiant;
        // If the intersection has hit a triangle, the fragment is shadowed

        outColor = vec4(max(color, vec3(0.0)), 1);

    } else {
        discard;
    }
}