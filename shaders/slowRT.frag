#version 450

#extension GL_EXT_buffer_reference : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require

struct Ray {
    vec3 ro;
    vec3 rd;
};

/**** COMMON END ****/

struct HitData {
    vec3 pos;
    vec3 normal;
    vec2 uv;
    uint mat;
};

struct ObjectInfo {
    mat4 modelMatrix;
    mat4 normalMatrix;
};

layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;

layout(buffer_reference, scalar) readonly buffer IndexBuffer { uint i[]; };
layout(buffer_reference, scalar) readonly buffer VertexBuffer { HitData d[]; };
layout(buffer_reference, scalar) readonly buffer InstanceBuffer2 { ObjectInfo objInfo[]; };

layout(push_constant) uniform constants {
    IndexBuffer indices;
    VertexBuffer vertexs;
    InstanceBuffer2 instances;
    int numberOfIndex;
}
PushConstants;

layout(location = 0) in vec2 fragOffset;
layout(location = 0) out vec4 outColor;

Ray createRay(in ivec2 px) {
    // convert pixel to NDS
    vec2 pxNDS = ((vec2(px.x, 720 - px.y)) / vec2(1280, 720)) * 2. - 1.;

    // choose an arbitrary HitPoint in the viewing volume
    // z = -1 equals a HitPoint on the near plane, i.e. the screen
    vec3 HitPointNDS = vec3(pxNDS, 0.1);

    // as this is in homogenous space, add the last homogenous coordinate
    vec4 HitPointNDSH = vec4(HitPointNDS, 1.0);
    // transform by inverse projection to get the HitPoint in view space
    vec4 dirEye = inverse(ubo.projection) * HitPointNDSH;

    // since the camera is at the origin in view space by definition,
    // the current HitPoint is already the correct direction
    // (dir(0,P) = P - 0 = P as a direction, an infinite HitPoint,
    // the homogenous component becomes 0 the scaling done by the
    // w-division is not of interest, as the direction in xyz will
    // stay the same and we can just normalize it later
    dirEye.w = 0.;
    vec3 ro = ubo.invView[3].xyz;
    // compute world ray direction by multiplying the inverse view matrix
    vec3 rd = normalize((ubo.invView * dirEye).xyz);
    return Ray(ro, rd);
}

// triangle degined by vertices v0, v1 and  v2
vec3 triIntersect(in vec3 ro, in vec3 rd, in vec3 v0, in vec3 v1, in vec3 v2) {
    vec3 v1v0 = v1 - v0;
    vec3 v2v0 = v2 - v0;
    vec3 rov0 = ro - v0;
    vec3 n = cross(v1v0, v2v0);
    vec3 q = cross(rov0, rd);
    float d = 1.0 / dot(rd, n);
    float u = d * dot(-q, v2v0);
    float v = d * dot(q, v1v0);
    float t = d * dot(-n, rov0);
    if (u < 0.0 || v < 0.0 || (u + v) > 1.0) t = -1.0;
    return vec3(t, u, v);
}

float intersectMesh(Ray ray, ObjectInfo zz) {
    float dist = 1.0 / 0.0;
    
    for (int i = 0; i < PushConstants.numberOfIndex; i += 3) {
        HitData outdata;
        HitData vertices[3];
        uint triangle[3] = {PushConstants.indices.i[i], PushConstants.indices.i[i + 1], PushConstants.indices.i[i + 2]};

        for (uint j = 0; j < 3; j++) {
            vertices[j] = PushConstants.vertexs.d[triangle[j]];
             vertices[j].pos = vec3(PushConstants.instances.objInfo[0].modelMatrix * vec4(vertices[j].pos, 1.0));
        }
        vec3 res = triIntersect(ray.ro, ray.rd, vertices[0].pos, vertices[1].pos, vertices[2].pos);
        if (res.x < dist && res.x > 0) {
            dist = res.x;
        }
    }

    return dist;
}

void main() {
    int m = PushConstants.numberOfIndex;
    uint z = PushConstants.indices.i[0];
    HitData b = PushConstants.vertexs.d[0];
    ObjectInfo zz = PushConstants.instances.objInfo[0];
    Ray ray = createRay(ivec2(gl_FragCoord.xy));
    float dist = intersectMesh(ray, zz);
    dist = 1.0 / dist;
    outColor = vec4(vec3(dist), 1.0);
}