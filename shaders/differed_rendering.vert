#version 460

const vec2 OFFSETS[3] = vec2[](
    vec2(-2, 2), vec2(0, -4.5), vec2(2, 2)

);

layout(location = 0) out vec2 fragOffset;

struct PointLight {
    vec4 position;  // ignore w
    vec4 color;     // w is intensity
};


layout(set = 0, binding = 0) uniform GlobalUbo {
    mat4 projection;
    mat4 view;
    mat4 invView;
}
ubo;

layout(set = 0, binding = 1) uniform sampler2D albedoAlphaTexture;
layout(set = 0, binding = 2) uniform sampler2D normalTexture;
layout(set = 0, binding = 3) uniform sampler2D worldPosTexture;
layout(set = 0, binding = 4) uniform sampler2D MRTexture;


layout(push_constant) uniform Push { float time; }
push;

void main() {
    float m = push.time;
    fragOffset = OFFSETS[gl_VertexIndex];
    gl_Position = vec4(fragOffset, 0, 1);
}