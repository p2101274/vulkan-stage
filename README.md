# Vulkan-Stage


## Description
A small project to learn Vulkan API and graphic programming.


## Installation

The engine should be cross-platform, but I only tested it on Linux. And the cmake file is not completed for windows.

### Download
```bash
git clone --recursive https://forge.univ-lyon1.fr/p2101274/vulkan-stage.git
```

### Requirements
- CMake
- Vulkan SDK
- GLFW
- GLM
- libuv (for shader hot reload)

### Build
// I'm not a professionnal with CMake, so I'm not sure about the following commands
```bash
mkdir build
cd build
cmake ..
make all
```
Personnally I juste use the cmaketools extension of Visual Studio Code.

I don't how to build the shader with cli cmake


## Usage

inside the build folder
```bash
./Nom_Du_Projet
```
A window should open with the sponza.

### Controls
- ZQSD/WASD to move
- arrow or Mouse to look around
- A/Q to move up
- E to move down

If there is validation error when you quit the program, it's normal, there is some ressources that are not freed. (There is no dynamic memory allocation, so it's not a big deal)

## Authors and acknowledgment
Jérémie Vernay
And thx to Jean-Claude Iehl for his help

## License
GPL-3.0 License
