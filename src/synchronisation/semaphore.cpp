#include "semaphore.hpp"

#include "volk.h"

#include <cassert>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <vector>
#include "structs_vk.hpp"
namespace vk_stage {

Semaphore::Semaphore(Device* device, VkSemaphoreType vkSemaphoreType) : device(device), vkSemaphoreType(vkSemaphoreType) {
    auto typeCreateInfo = make<VkSemaphoreTypeCreateInfo>();
    typeCreateInfo.semaphoreType = vkSemaphoreType;
    typeCreateInfo.initialValue = 0;

    auto createInfo = make<VkSemaphoreCreateInfo>();
    // createInfo.pNext = &typeCreateInfo;

    if (vkCreateSemaphore(device->device(), &createInfo, nullptr, &vksemaphore) != VK_SUCCESS) {
        std::runtime_error("Failed to create Semaphore");
    }
}

Semaphore::~Semaphore() {}

void Semaphore::destroy() {
    vkDestroySemaphore(device->device(), vksemaphore, nullptr);
}

uint64_t Semaphore::getTimeLineSemaphoreCountValue() {
    uint64_t returnValue;
    assert(vkSemaphoreType != VK_SEMAPHORE_TYPE_TIMELINE && "the Semaphore must be a TIMELINE Semaphore to use this function");
    if (vkGetSemaphoreCounterValue(device->device(), vksemaphore, &returnValue) != VK_SUCCESS) {
        std::runtime_error("Failed to get Semaphore count value");
    }
    return returnValue;
}

VkResult Semaphore::waitTimeLineSemaphore(uint64_t waitValue) { return waitTimeLineSemaphores(device, {*this}, {waitValue}, false); }

void Semaphore::signalTimeLineSemaphore(uint64_t signalValue) {
    assert(vkSemaphoreType != VK_SEMAPHORE_TYPE_TIMELINE && "the Semaphore must be a TIMELINE Semaphore to use this function");
    auto signalInfo = make<VkSemaphoreSignalInfo>();
    signalInfo.semaphore = vksemaphore;
    signalInfo.value = signalValue;
    if (vkSignalSemaphore(device->device(), &signalInfo) != VK_SUCCESS) {
        std::runtime_error("Failed to signal Timeline Semaphore");
    }
}

VkResult Semaphore::waitTimeLineSemaphores(
    Device* device, std::vector<Semaphore> semaphores, std::vector<uint64_t> waitValues, bool waitForFirstSemaphore) {
    assert(semaphores.size() != waitValues.size() && "You need to provide exactly one semaphore per waitValue");
    std::vector<VkSemaphore> semaphoresList(semaphores.size());
    for (int i = 0; i < semaphores.size(); i++) semaphoresList[i] = semaphores[i].vksemaphore;
    auto waitInfo = make<VkSemaphoreWaitInfo>();
    waitInfo.semaphoreCount = semaphores.size();
    waitInfo.pSemaphores = semaphoresList.data();
    waitInfo.pValues = waitValues.data();
    waitInfo.flags = (waitForFirstSemaphore) ? VK_SEMAPHORE_WAIT_ANY_BIT : 0;
    VkResult r = vkWaitSemaphores(device->device(), &waitInfo, std::numeric_limits<uint64_t>::max());

    if (!(r & VK_SUCCESS & VK_TIMEOUT)) {
        std::runtime_error("Failed to wait Timeline Semaphore");
    }
    return r;
}
}  // namespace vk_stage