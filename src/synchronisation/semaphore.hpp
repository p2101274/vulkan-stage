#pragma once

#include "volk.h"

#include <cstdint>
#include <vector>

#include "device.hpp"

namespace vk_stage {

class Semaphore {
   public:
    Semaphore(Device *device, VkSemaphoreType vkSemaphoreType);
    ~Semaphore();

    void destroy();
    uint64_t getTimeLineSemaphoreCountValue();
    VkResult waitTimeLineSemaphore(uint64_t waitValue);
    void signalTimeLineSemaphore(uint64_t signalValue);
    VkSemaphore semaphore(){return vksemaphore;}

    static VkResult waitTimeLineSemaphores(
        Device* device, std::vector<Semaphore> semaphores, std::vector<uint64_t> waitValues, bool waitForFirstSemaphore);

   private:
    VkSemaphore vksemaphore;
    VkSemaphoreType vkSemaphoreType;

    Device *device;
};

}  // namespace vk_stage