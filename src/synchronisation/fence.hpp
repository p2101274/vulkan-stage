#pragma once

#include "volk.h"
#include <vector>

#include "device.hpp"
namespace vk_stage {
class Fence {
   public:
    Fence(Device *device, bool signaled = false);
    ~Fence();

    void destroy();

    VkResult getFenceStatus();
    void resetFence(); 

    VkFence fence(){return vkFence;}

    VkResult waitForFence();
    static VkResult waitForFences(Device *vkDevice, std::vector<Fence> fences, bool waitAllFence, size_t *firstFenceSignaled);

   private:
    VkFence vkFence;

    Device *device;
};
}  // namespace vk_stage