#include "app.hpp"

#include <vulkan/vulkan_core.h>

#include <chrono>
#include <cstdint>
#include <glm/detail/qualifier.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/fwd.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "commandBuffer/commandBufferPoolHandler.hpp"
#include "dynamic_renderpass.hpp"
#include "hot_reload.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
#include "swapchain.hpp"
#include "systems/differed_system_ray_Traced.hpp"
#include "systems/differed_system_ray_Traced_CACA.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include "movement_controller.hpp"
#include "renderer.hpp"
#include "scene/loaders/gltf.hpp"
#include "scene/object.hpp"
#include "scene/scene.hpp"
#include "systems/cull_system.hpp"
#include "systems/draw_system.hpp"
#include "utils.hpp"
#include "volk.h"

namespace vk_stage {


glm::vec3 HSLtoRGB(float h, float s, float l) {
    float c = (1.0f - std::abs(2.0f * l - 1.0f)) * s;
    float x = c * (1.0f - std::abs(fmod(h / 60.0f, 2) - 1.0f));
    float m = l - c / 2.0f;

    float r, g, b;

    if (h >= 0 && h < 60) {
        r = c;
        g = x;
        b = 0;
    } else if (h >= 60 && h < 120) {
        r = x;
        g = c;
        b = 0;
    } else if (h >= 120 && h < 180) {
        r = 0;
        g = c;
        b = x;
    } else if (h >= 180 && h < 240) {
        r = 0;
        g = x;
        b = c;
    } else if (h >= 240 && h < 300) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }

    r += m;
    g += m;
    b += m;

    return glm::vec3(r, g, b);
}

App::App() {}

App::~App() {}

/*
I know that this function should be split into multiple functions but I'm too lazy to do it right now
*/
void App::init() {
    std::srand(std::time(0));
    // Create scene and load it into the GPU (gpu instance data, mesh data, etc)
    Scene s = GLTFLoader::read_gltf_scene("Sponza.gltf", &device);

    GraphicPipelineCreateInfo c;
    c.fragmentShaderFile = "hello_scene.frag";
    c.vexterShaderFile = "hello_scene.vert";
    auto pip = GraphicPipeline(&device, c);
    // Object sphere1;
    
    // sphere1.objType = objectType::sphere;
    // for (int i = 0; i < 7; i++) {
    //     for (int j = 0; j < 7; j++) {
    //         Material m;
    //         float r3 = 0.01 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(0.2-0.01)));
    //         if(i != 3 || j !=3)
    //             m.color = glm::vec4(HSLtoRGB(static_cast<float>(std::rand() % 360), 0.9, 0.5), 1);
    //         else 
    //             m.color = glm::vec4(1);
    //         if(i != 3 || j !=3)
    //             m.roughness = r3; //i * 0.14;
    //         else
    //              m.roughness = 0.01;
    //         m.metallic = 0.14;
    //         m.color_texture_id = -1;
    //         m.metallic_roughness_texture_id = -1;

    //         s.materials.push_back(m);
    //         sphere1.customObjMaterial = s.materials.size() - 1;
    //         sphere1.transform.translation = glm::vec3(i * 2.5, 1.5, j * 2.5);
    //         s.objects.push_back(sphere1);
    //     }
    // }

    

    s.genBufferAndDescriptorData(pip.getDescriptorSetLayout(0), pip.getDescriptorSetLayout(1));
    // Create renderer and dynamic render passes
    Renderer renderer{&device, window};

    DynamicRenderPass firstPass(
        &device, window.getExtent(),
        {VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R16G16B16A16_SFLOAT, VK_FORMAT_R32G32B32A32_SFLOAT, VK_FORMAT_B8G8R8A8_UNORM},
        MAX_FRAMES_IN_FLIGHT, DEPTH);

    DynamicRenderPass finalRenderPass{&device, window.getExtent(),     {}, uint32_t(renderer.getSwapChain()->getswapChainImages().size()),
                                      NONE,    renderer.getSwapChain()};

    renderer.addRenderPassResize(&finalRenderPass);
    renderer.addRenderPassResize(&firstPass);
    s.createSphereAccelerationStructure();
    // Create the ray tracing data
    if (device.RTenabled) {
        for (auto& mesh : s.meshes) {
            mesh.createRaytracingData();
            mesh.buildAccelerationStructure(VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR);
        }

        s.buildAccelerationStructure(VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR);
    }

    CullSystem cSys(&device, &s, &firstPass, window.getExtent());
    DrawSystem dSys(&device, &s, cSys.drawCommandBuffer);
    // DifferedSystem difSys(&device, &s, &firstPass);
    DifferedRayTracingSystem difRTSys(&device, &s, &firstPass, renderer.getSwapChain()->getswapChainImages());
    // DifferedRayTracingSystemCACA t(&device, &s);

    auto currentTime = std::chrono::high_resolution_clock::now();

    MovementController cameraController{window};
    float totalTime;
    HotReload::init(&device);
    s.cameras[0].viewerObject.transform.translation = glm::vec3(3.601894, 4.739911, 2.943703);
    s.cameras[0].viewerObject.transform.rotation = glm::vec3(-0.008176, 1.297441, 0.000000);
    while (!window.shouldClose()) {
        glfwPollEvents();
        auto newTime = std::chrono::high_resolution_clock::now();
        float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
        totalTime += frameTime;
        currentTime = newTime;
        s.cameras[0].setPerspectiveProjection(glm::radians(70.f), renderer.getAspectRatio(), 0.01f, 500.f);
        auto oldCam = s.cameras[0].getView();
        cameraController.moveInPlaneXZ(window.getGLFWwindow(), frameTime, s.cameras[0]);
        s.cameras[0].setViewYXZ(s.cameras[0].viewerObject.transform.translation, s.cameras[0].viewerObject.transform.rotation);

        bool camMoov = oldCam != s.cameras[0].getView();

        cameraController.addpointLight(window.getGLFWwindow(), frameTime, s);
        if (renderer.startFrame()) {
            
            // Update the camera data
            GlobalUbo camData{};
            camData.view = s.cameras[0].getView();
            camData.projection = s.cameras[0].getProjection();
            camData.invView = s.cameras[0].getInverseView();
            s.uboBuffers[renderer.getCurrentFrameIndex()][0].writeToBuffer(&camData);
            s.uboBuffers[renderer.getCurrentFrameIndex()][0].flush();
            // std::cout << glm::to_string(s.cameras[0].viewerObject.transform.translation) << std::endl;

            // first pass
            // build draw command with compute shader and draw the scene to the gbuffer
            // VkCommandBuffer c = renderer.startPrecompute();
            // // cSys.frustrumCull(c, renderer.getCurrentFrameIndex(), renderer.getCurrentSwapChainImageIndex());
            // // firstPass.beginRenderPass(c, renderer.getCurrentFrameIndex());  //, VK_RENDERING_SUSPENDING_BIT);
            // // pip.bindPipeline(c);
            // // dSys.drawScene(c, renderer.getCurrentFrameIndex(), pip.getPipelineLayout());

            // s.objects[1].transform.rotation.y =glm::sin(totalTime);
            // s.objects[1].transform.scale.y =glm::sin(totalTime)*0.4 + 0.7;
            // s.objects[1].transform.translation.z =glm::sin(totalTime) * 2 ;
            // s.objects[1].needGPUupdate = true;
            // s.updateInstanceData(c);
            // // firstPass.endRenderPass(c);
            // renderer.endPrecompute();

            // second pass
            // render the light to the final image
            VkCommandBuffer c = renderer.startRendering();

            // firstPass.transitionColorAttachment(renderer.getCurrentFrameIndex(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, c);

            // finalRenderPass.beginRenderPass(c, renderer.getCurrentSwapChainImageIndex(), ADDIDITIVE);
            // difSys.renderLight(c, renderer.getCurrentFrameIndex(), totalTime);
            difRTSys.renderLight(c, renderer.getCurrentFrameIndex(), renderer.getCurrentSwapChainImageIndex(), totalTime, camMoov);
            // t.renderLight(c, renderer.getCurrentFrameIndex());
            // finalRenderPass.endRenderPass(c);
            // firstPass.transitionColorAttachment(renderer.getCurrentFrameIndex(), VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, c);

            renderer.endRendering(true);
            renderer.endAndPresentFrame();
        }
    }
    HotReload::stop();
    vkDeviceWaitIdle(device.device());
    cSys.destroy();
    // difSys.destroy();

    s.destroy();

    pip.destroy();
    CommandBufferPoolHandler::defaultPool(&device)->destroy();
    delete CommandBufferPoolHandler::defaultPool(&device);
}

// I know that the render loop should be here
void App::run() {}

}  // namespace vk_stage