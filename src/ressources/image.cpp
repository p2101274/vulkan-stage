#include "image.hpp"

#include <sys/types.h>
#include <vulkan/vulkan_core.h>

#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>

#include "buffer.hpp"
#include "commandBuffer/commandBufferPoolHandler.hpp"
#include "device.hpp"
#include "structs_vk.hpp"
#include "utils.hpp"
#include "volk.h"

#define STB_IMAGE_IMPLEMENTATION
#include "external/stb_image.h"

namespace vk_stage {

Image::Image(Device* device, ImageCreateInfo& imageCreateInfo, VkCommandBuffer commandBuffer)
    : device{device},
      imageFormat(imageCreateInfo.format),
      width(imageCreateInfo.width),
      height(imageCreateInfo.height),
      layer(imageCreateInfo.layers),
      imageLayout(imageCreateInfo.imageLayout) {
    if (!imageCreateInfo.filename.empty()) {
        loadImageFromFile(imageCreateInfo);
    }
    actualImageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createImage(imageCreateInfo);
    if (imageCreateInfo.data != nullptr) {
        loadImageToGPU(imageCreateInfo, commandBuffer);
    }

    createImageView(imageCreateInfo);
    createSampler(imageCreateInfo);

    transitionImageLayout(actualImageLayout, imageLayout, commandBuffer);
}

Image::Image(Device* device, VkImage image, VkImageView imageView, VkFormat format, VkExtent2D swapChainExtent) : device(device) {
    this->imageFormat = format;
    this->width = swapChainExtent.width;
    this->height = swapChainExtent.height;
    this->layer = 1;
    this->imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    this->actualImageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    this->imageView = imageView;
    this->image = image;
    this->isSwapchainImage = true;
}

Image::~Image() {}

void Image::destroy() {
    if (!isSwapchainImage) {
        if (sampler != VK_NULL_HANDLE) vkDestroySampler(device->device(), sampler, nullptr);

        if (imageView != VK_NULL_HANDLE) vkDestroyImageView(device->device(), imageView, nullptr);

        if (image != VK_NULL_HANDLE) vkDestroyImage(device->device(), image, nullptr);
        vkFreeMemory(device->device(), imageMemory, nullptr);
    }
}

void Image::createImage(ImageCreateInfo& imageCreateInfo) {
    if (imageCreateInfo.enableMipMap) {
        imageCreateInfo.usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
        mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(imageCreateInfo.width, imageCreateInfo.width)))) + 1;
    }
    auto imageInfo = make<VkImageCreateInfo>();
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.format = imageCreateInfo.format;
    imageInfo.mipLevels = mipLevels;
    imageInfo.arrayLayers = imageCreateInfo.layers;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = imageCreateInfo.usageFlags;
    imageInfo.extent = {imageCreateInfo.width, imageCreateInfo.height, 1};
    // imageInfo.flags = VK_IMAGE_CREATE_2D_ARRAY_COMPATIBLE_BIT;
    if (imageCreateInfo.isCubeTexture) {
        imageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }

    createImageWithInfo(imageInfo, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, image, imageMemory);
    actualImageLayout = VK_IMAGE_LAYOUT_UNDEFINED;
}

void Image::createImageWithInfo(
    const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory) {
    if (vkCreateImage(device->device(), &imageInfo, nullptr, &image) != VK_SUCCESS) {
        throw std::runtime_error("failed to create image!");
    }

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(device->device(), image, &memRequirements);

    auto allocInfo = make<VkMemoryAllocateInfo>();
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = device->findMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(device->device(), &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate image memory!");
    }

    if (vkBindImageMemory(device->device(), image, imageMemory, 0) != VK_SUCCESS) {
        throw std::runtime_error("failed to bind image memory!");
    }
}

void Image::createImageView(ImageCreateInfo& imageCreateInfo) {
    auto imageViewInfo = make<VkImageViewCreateInfo>();
    if (imageCreateInfo.isCubeTexture) {
        if ((imageCreateInfo.layers / 6) > 1) {
            imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
        } else {
            imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
        }
    } else {
        if (imageCreateInfo.layers > 1 && !imageCreateInfo.isCubeTexture) {
            imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        } else {
            imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;  // VK_IMAGE_VIEW_TYPE_2D
        }
    }

    imageViewInfo.format = imageFormat;
    // imageViewInfo.components = {
    //     VK_COMPONENT_SWIZZLE_R,
    //     VK_COMPONENT_SWIZZLE_G,
    //     VK_COMPONENT_SWIZZLE_B,
    //     VK_COMPONENT_SWIZZLE_A};  // VK_COMPONENT_SWIZZLE_IDENTITY
    imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;  // VK_IMAGE_ASPECT_COLOR_BIT
    if (imageCreateInfo.usageFlags & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) {
        imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    }
    imageViewInfo.subresourceRange.baseMipLevel = 0;
    imageViewInfo.subresourceRange.baseArrayLayer = 0;
    imageViewInfo.subresourceRange.layerCount = imageCreateInfo.layers;
    imageViewInfo.subresourceRange.levelCount = mipLevels;
    imageViewInfo.image = image;
    vkCreateImageView(device->device(), &imageViewInfo, nullptr, &imageView);
}

void Image::createSampler(ImageCreateInfo& imageCreateInfo) { sampler = createSampler(device, static_cast<float>(mipLevels)); }

VkSampler Image::createSampler(Device* device, float maxLod) {
    VkSampler returnValue;
    auto samplerInfo = make<VkSamplerCreateInfo>();
    samplerInfo.magFilter = VK_FILTER_LINEAR;  // VK_FILTER_LINEAR
    samplerInfo.minFilter = VK_FILTER_LINEAR;  // VK_FILTER_LINEAR
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.compareOp = VK_COMPARE_OP_NEVER;  // VK_COMPARE_OP_NEVER
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = maxLod;
    samplerInfo.maxAnisotropy = 4.0f;
    samplerInfo.anisotropyEnable = VK_TRUE;                      // VK_FALSE
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;  // VK_BORDER_COLOR_INT_OPAQUE_BLACK
    vkCreateSampler(device->device(), &samplerInfo, nullptr, &returnValue);
    return returnValue;
}

void Image::loadImageFromFile(ImageCreateInfo& imageCreateInfo) {
    // stbi_set_flip_vertically_on_load(true);
    int nbOfchannel;
    int width, height;

    imageCreateInfo.data = stbi_load((imageCreateInfo.filename[0]).c_str(), &width, &height, &nbOfchannel, 4);
    this->width = width;
    this->height = height;
    imageCreateInfo.width = width;
    imageCreateInfo.height = height;
    if (imageCreateInfo.format == VK_FORMAT_UNDEFINED) {
        this->imageFormat = VK_FORMAT_R8G8B8A8_SRGB;
        imageCreateInfo.format = VK_FORMAT_R8G8B8A8_SRGB;
    }
}

void Image::loadImageToGPU(ImageCreateInfo& imageCreateInfo, VkCommandBuffer extCmdBuffer) {
    VkDeviceSize imageSize = width * height * getPixelSizeFromFormat(imageCreateInfo.format);
    
    Buffer *stagingBuffer = new Buffer{
        device, getPixelSizeFromFormat(imageCreateInfo.format), static_cast<u_int32_t>(width * height), VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT};

    stagingBuffer->map();
    stagingBuffer->writeToBuffer(imageCreateInfo.data);

    transitionImageLayout(actualImageLayout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, extCmdBuffer);

    Buffer::copyBufferToImage(
        device, stagingBuffer->getBuffer(), image, static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1, extCmdBuffer);

    if(extCmdBuffer != VK_NULL_HANDLE){
        CommandBufferPoolHandler::defaultPool(device)->addBufferToDestroy(extCmdBuffer, stagingBuffer);
    }
    else{
        stagingBuffer->destroy();
        delete stagingBuffer;
    }
    // transitionImageLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, imageLayout, nullptr);
    if (!imageCreateInfo.filename.empty()) stbi_image_free(imageCreateInfo.data);
}

void Image::generateMipmaps() {
    VkCommandBuffer commandBuffer = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();

    transitionImageLayout(actualImageLayout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, 0, 1, commandBuffer);

    for (uint32_t i = 1; i < mipLevels; i++) {
        VkImageBlit imageBlit{};

        // Source
        imageBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.srcSubresource.layerCount = layer;
        imageBlit.srcSubresource.mipLevel = i - 1;
        imageBlit.srcOffsets[1].x = int32_t(this->width >> (i - 1));
        imageBlit.srcOffsets[1].y = int32_t(this->height >> (i - 1));
        imageBlit.srcOffsets[1].z = 1;

        // Destination
        imageBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.dstSubresource.layerCount = layer;
        imageBlit.dstSubresource.mipLevel = i;
        imageBlit.dstOffsets[1].x = int32_t(this->width >> i);
        imageBlit.dstOffsets[1].y = int32_t(this->height >> i);
        imageBlit.dstOffsets[1].z = 1;

        VkImageSubresourceRange mipSubRange = {};
        mipSubRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        mipSubRange.baseMipLevel = i;
        mipSubRange.levelCount = 1;
        mipSubRange.layerCount = layer;

        // Prepare current mip level as image blit destination
        transitionImageLayout(VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, i, 1, commandBuffer);

        // Blit from previous level
        vkCmdBlitImage(
            commandBuffer, this->image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
            &imageBlit, VK_FILTER_LINEAR);

        // // Prepare current mip level as image blit source for next level
        transitionImageLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, i, 1, commandBuffer);
    }
    CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(commandBuffer);
}

void Image::transitionImageLayout(VkImageLayout oldLayout, VkImageLayout newLayout, VkCommandBuffer externalCommandBuffer) {
    transitionImageLayout(oldLayout, newLayout, 0, this->mipLevels, externalCommandBuffer);
    actualImageLayout = newLayout;
}

void Image::transitionImageLayout(
    VkImageLayout oldLayout, VkImageLayout newLayout, u_int32_t mipLevel, u_int32_t mipCount, VkCommandBuffer externalCommandBuffer) {
    auto barrier = make<VkImageMemoryBarrier>();
    barrier.oldLayout = oldLayout;                          // VK_IMAGE_LAYOUT_UNDEFINED
    barrier.newLayout = newLayout;                          // VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;  // VK_QUEUE_FAMILY_IGNORED
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;  // VK_QUEUE_FAMILY_IGNORED
    barrier.image = image;
    if (imageFormat != VK_FORMAT_D32_SFLOAT) {  // https://discord.com/channels/231931740661350410/1140282527760781323
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;  // VK_IMAGE_ASPECT_COLOR_BIT
    } else {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
    }
    barrier.subresourceRange.baseMipLevel = mipLevel;
    barrier.subresourceRange.levelCount = mipCount;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = layer;

    barrier.srcAccessMask = getAccessFlagsFromLayout(oldLayout);
    barrier.dstAccessMask = getAccessFlagsFromLayout(newLayout);

    // TODO : GET CORRECT STATE FLAGS
    VkPipelineStageFlags sourceStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

    if (externalCommandBuffer == nullptr) {
        VkCommandBuffer commandBuffer = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();
        vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
        CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(commandBuffer);
    } else {
        vkCmdPipelineBarrier(externalCommandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
    }
}

VkDescriptorImageInfo Image::descriptorInfo() {
    VkDescriptorImageInfo returnValue;
    returnValue.imageLayout = imageLayout;
    returnValue.imageView = imageView;
    return returnValue;
}

void Image::copyImage(Device* device, Image& srcImage, Image& dstImage, VkCommandBuffer externalCommandBuffer) {
    VkCommandBuffer commandBuffer = externalCommandBuffer;
    if (externalCommandBuffer == VK_NULL_HANDLE) commandBuffer = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();

    VkImageLayout srcImageLayout = srcImage.getActualImageLayout();
    VkImageLayout dstImageLayout = dstImage.getActualImageLayout();

    srcImage.transitionImageLayout(srcImageLayout, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, commandBuffer);
    dstImage.transitionImageLayout(dstImageLayout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, commandBuffer);

    VkImageBlit blit{};
    blit.srcOffsets[0] = {0, 0, 0};
    blit.srcOffsets[1] = {(int32_t)srcImage.getWidth(), (int32_t)srcImage.getHeight(), 1};
    blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.srcSubresource.mipLevel = 0;
    blit.srcSubresource.baseArrayLayer = 0;
    blit.srcSubresource.layerCount = 1;
    blit.dstOffsets[0] = {0, 0, 0};
    blit.dstOffsets[1] = {(int32_t)srcImage.getWidth(), (int32_t)srcImage.getHeight(), 1};
    blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit.dstSubresource.mipLevel = 0;
    blit.dstSubresource.baseArrayLayer = 0;
    blit.dstSubresource.layerCount = 1;

    vkCmdBlitImage(
        commandBuffer, srcImage.getImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dstImage.getImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1, &blit, VK_FILTER_LINEAR);

    srcImage.transitionImageLayout(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, srcImageLayout, commandBuffer);

    dstImage.transitionImageLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, dstImageLayout, commandBuffer);

    if (externalCommandBuffer == VK_NULL_HANDLE) CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(commandBuffer);
}
}  // namespace vk_stage