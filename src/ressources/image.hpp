#pragma once



#include "volk.h"

#include <cstdint>
#include <string>

#include "device.hpp"

namespace vk_stage {

struct ImageCreateInfo {
    uint32_t width;
    uint32_t height;
    uint32_t layers = 1;
    VkFormat format = VK_FORMAT_UNDEFINED;
    VkImageUsageFlags usageFlags;
    VkImageLayout imageLayout;
    bool isCubeTexture = false;
    bool enableMipMap = false;
    std::vector<std::string> filename;
    void* data = nullptr;
};

class Image {
   public:
   Image(){};
    Image(Device *device, ImageCreateInfo& imageCreateInfo, VkCommandBuffer commandBuffer = VK_NULL_HANDLE);
    Image(Device *device, VkImage image, VkImageView imageView, VkFormat format, VkExtent2D swapChainExtent);

    ~Image();
    void destroy();
    void transitionImageLayout(VkImageLayout oldLayout, VkImageLayout newLayout, VkCommandBuffer commandBuffer = VK_NULL_HANDLE);
    void transitionImageLayout(
        VkImageLayout oldLayout, VkImageLayout newLayout, u_int32_t mipLevel, u_int32_t mipCount, VkCommandBuffer commandBuffer);

    // Getters
    VkImage getImage() const { return image; }
    VkImageView getImageView() const { return imageView; }
    VkSampler getSampler() const { return sampler; }
    VkImageLayout getImageLayout() const { return imageLayout; }
    VkImageLayout getActualImageLayout() const { return actualImageLayout; }
    uint32_t getWidth() const { return width; }
    uint32_t getHeight() const { return height; }
    VkFormat getFormat() const { return imageFormat; }
    uint32_t getMipmapLvl() const{return  mipLevels;}
    VkDescriptorImageInfo descriptorInfo();
    bool isSwapchainImg() const {return isSwapchainImage;}

    // Static Functions
    static void copyImage(Device *device, Image& srcImage, Image& dstImage, VkCommandBuffer commandBuffer = VK_NULL_HANDLE);

    static VkSampler createSampler(Device *device, float maxLod);


   private:
    uint32_t width = 0;
    uint32_t height = 0;
    uint32_t layer = 0;
    uint32_t mipLevels = 1;
    Device *device;
    VkImage image = VK_NULL_HANDLE;
    VkDeviceMemory imageMemory = VK_NULL_HANDLE;
    VkImageView imageView = VK_NULL_HANDLE;
    VkSampler sampler = VK_NULL_HANDLE;
    VkFormat imageFormat;
    VkImageLayout imageLayout;
    VkImageLayout actualImageLayout;

    bool isSwapchainImage =false;

    void createImage(ImageCreateInfo& imageCreateInfo);
    void createImageView(ImageCreateInfo& imageCreateInfo);
    void createSampler(ImageCreateInfo& imageCreateInfo);

    void generateMipmaps();

    void loadImageFromFile(ImageCreateInfo& imageCreateInfo);
    void loadImageToGPU(ImageCreateInfo& imageCreateInfo, VkCommandBuffer extCmdBuffer= VK_NULL_HANDLE);
    void createImageWithInfo(
        const VkImageCreateInfo& imageInfo, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);
    

    
};
}  // namespace vk_stage