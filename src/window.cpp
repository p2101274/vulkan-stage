#include "window.hpp"

#include "volk.h"

#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"


namespace vk_stage {
Window::Window(unsigned int width, unsigned int height, std::string name) : width(width), height(height) {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);
    glfwSetWindowUserPointer(window, this);
    if (glfwRawMouseMotionSupported())
        glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}


VkSurfaceKHR Window::getSurface(vkb::Instance &vkInstance) {
    VkSurfaceKHR vkSurface;
    if (glfwCreateWindowSurface(vkInstance.instance, window, nullptr, &vkSurface) != VK_SUCCESS) {
        throw std::runtime_error("failed to craete window surface");
    }
    return vkSurface;
}

Window::~Window() {
    glfwDestroyWindow(window);
    glfwTerminate();
}

void Window::framebufferResizeCallback(GLFWwindow *window, int width, int height) {
    auto windowObj = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
    windowObj->framebufferResized = true;
    windowObj->width = width;
    windowObj->height = height;
}
}  // namespace vk_stage