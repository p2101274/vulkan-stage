#pragma once

#include <uv.h>

#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "device.hpp"
#include "shader/pipeline.hpp"
namespace vk_stage {
class HotReload {
   public:
    static void init(Device* device);
    static void stop();
    static void addShaderToWatch(std::string shaderName, Pipeline* pipelineToNotify);
    static void reloadShaders(std::string name);
    static void on_file_change(uv_fs_event_t* handle, const char* filename, int events, int status);
    static void on_file_changeSPV(uv_fs_event_t* handle, const char* filename, int events, int status);

    static std::map<std::string, std::vector<Pipeline*>> shaderPipelineMap;

   private:
   static Device* device;
   static std::mutex m;
    static std::thread uv_thread;
    static std::thread uv_threadSPV;
    static uv_fs_event_t fs_event;
    static uv_fs_event_t fs_eventSPV;
    static uv_loop_t loop;
    static uv_loop_t loopSPV;
};
}  // namespace vk_stage