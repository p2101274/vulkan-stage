#pragma once

#include <map>
#include <vector>

#include "device.hpp"
#include "synchronisation/fence.hpp"
#include "volk.h"
#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"
#include "commandBuffer.hpp"

namespace vk_stage {

class CommandBufferPoolHandler {
   public:
    CommandBufferPoolHandler(Device* device, vkb::QueueType queueType);
    ~CommandBufferPoolHandler();

    void destroy();

    std::vector<CommandBuffer> createCommandBuffer(unsigned int commandBufferCount);
    void resetPool();

    VkCommandBuffer beginSingleTimeCommand();
    void endSingleTimeCommand(VkCommandBuffer cmdBuffer, bool waitForSingleCommand = true);
    void endSingleTimeCommand2(VkCommandBuffer cmdBuffer, bool waitForSingleCommand = true);
    void addBufferToDestroy(VkCommandBuffer cmdBuffer, Buffer* buffer){bufferTodestroyAtTheEnd[cmdBuffer].push_back(buffer);}

    VkCommandPool getPool() { return commandPool; }
    VkQueue getQueue() { return queue; }
    static CommandBufferPoolHandler* defaultPool(Device* device);


   private:
    static void delayDestroyCmdBuffer(Device *device, VkCommandBuffer cmdBuffer, VkCommandPool commandPool, Fence fence, std::vector<Buffer*> bufferToDestroy);

    VkCommandPool commandPool;
    VkQueue queue;
    Device* device;


    std::map<VkCommandBuffer, std::vector<Buffer*>> bufferTodestroyAtTheEnd;
    static CommandBufferPoolHandler* defaultPoolHandler;
};
}  // namespace vk_stage