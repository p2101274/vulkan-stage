#include "commandBufferPoolHandler.hpp"

#include <vulkan/vulkan_core.h>

#include <iostream>
#include <stdexcept>
#include <thread>
#include <vector>

#include "VkBootstrap.h"
#include "commandBuffer.hpp"
#include "device.hpp"
#include "structs_vk.hpp"
#include "synchronisation/fence.hpp"
#include "volk.h"

namespace vk_stage {

CommandBufferPoolHandler* CommandBufferPoolHandler::defaultPoolHandler = nullptr;

CommandBufferPoolHandler* CommandBufferPoolHandler::defaultPool(Device* device) {
    if (defaultPoolHandler == nullptr) {
        defaultPoolHandler = new CommandBufferPoolHandler(device, vkb::QueueType::graphics);
    }
    return defaultPoolHandler;
}

CommandBufferPoolHandler::CommandBufferPoolHandler(Device* device, vkb::QueueType queueType) : device(device) {
    queue = device->get_queue(queueType).value();
    auto createInfo = make<VkCommandPoolCreateInfo>();
    createInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    createInfo.queueFamilyIndex = device->get_queue_index(queueType).value();

    if (vkCreateCommandPool(device->device(), &createInfo, nullptr, &commandPool) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate CommandBufferPoolHandler");
    }
}

CommandBufferPoolHandler::~CommandBufferPoolHandler() {}

void CommandBufferPoolHandler::destroy() { vkDestroyCommandPool(device->device(), commandPool, nullptr); }

std::vector<CommandBuffer> CommandBufferPoolHandler::createCommandBuffer(unsigned int commandBufferCount) {
    std::vector<VkCommandBuffer> commandBuffers(commandBufferCount);
    std::vector<CommandBuffer> commandBuffersReturn;
    commandBuffersReturn.reserve(commandBufferCount);

    auto allocInfo = make<VkCommandBufferAllocateInfo>();
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = commandBufferCount;

    if (vkAllocateCommandBuffers(device->device(), &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate commandBuffer");
    }

    for (int i = 0; i < commandBufferCount; i++) {
        commandBuffersReturn.emplace_back(commandPool, queue, device, commandBuffers[i]);
    }
    return commandBuffersReturn;
}

void CommandBufferPoolHandler::resetPool() {
    auto res = vkResetCommandPool(device->device(), commandPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
    if (res != VK_SUCCESS) {
        throw std::runtime_error("Failed to reset command pool");
    }
}

VkCommandBuffer CommandBufferPoolHandler::beginSingleTimeCommand() {
    auto allocInfo = make<VkCommandBufferAllocateInfo>();
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer cmdBuffer;
    if (vkAllocateCommandBuffers(device->device(), &allocInfo, &cmdBuffer) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate commandBuffer");
    }
    auto beginInfo = make<VkCommandBufferBeginInfo>();
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(cmdBuffer, &beginInfo) != VK_SUCCESS) {
        throw std::runtime_error("Failed to begin single time commandBuffer");
    }
    return cmdBuffer;
}

void CommandBufferPoolHandler::endSingleTimeCommand(VkCommandBuffer cmdBuffer, bool waitForSingleCommand) {
    vkEndCommandBuffer(cmdBuffer);

    auto submitInfo = make<VkSubmitInfo>();
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &cmdBuffer;

    if (waitForSingleCommand) {
        vkQueueSubmit(queue, 1, &submitInfo, nullptr);
        vkQueueWaitIdle(queue);
        vkFreeCommandBuffers(device->device(), commandPool, 1, &cmdBuffer);
        for (auto& buffer : bufferTodestroyAtTheEnd[cmdBuffer]) {
            buffer->destroy();
            delete buffer;
        }
        bufferTodestroyAtTheEnd[cmdBuffer].clear();
    } else {
        Fence f(device, false);
        vkQueueSubmit(queue, 1, &submitInfo, f.fence());
        std::thread t(delayDestroyCmdBuffer,device, cmdBuffer, commandPool, f, bufferTodestroyAtTheEnd[cmdBuffer]);
        t.detach();
    }
}

void CommandBufferPoolHandler::endSingleTimeCommand2(VkCommandBuffer cmdBuffer, bool waitForSingleCommand) {
    vkEndCommandBuffer(cmdBuffer);

    auto cmdSubmitInfo = make<VkCommandBufferSubmitInfo>();
    cmdSubmitInfo.commandBuffer = cmdBuffer;

    auto submitInfo = make<VkSubmitInfo2>();
    submitInfo.commandBufferInfoCount = 1;
    submitInfo.pCommandBufferInfos = &cmdSubmitInfo;

    if (vkQueueSubmit2(queue, 1, &submitInfo, nullptr) != VK_SUCCESS) {
        throw std::runtime_error("Failed to submit single time commandBuffer");
    }

    vkQueueWaitIdle(queue);
    vkFreeCommandBuffers(device->device(), commandPool, 1, &cmdBuffer);
}

void CommandBufferPoolHandler::delayDestroyCmdBuffer(
    Device* device, VkCommandBuffer cmdBuffer, VkCommandPool commandPool, Fence fence, std::vector<Buffer*> bufferToDestroy) {
    fence.waitForFence();
    for (auto& buffer : bufferToDestroy) {
        buffer->destroy();
        delete buffer;
    }
    vkFreeCommandBuffers(device->device(), commandPool, 1, &cmdBuffer);
    fence.destroy();
}
}  // namespace vk_stage