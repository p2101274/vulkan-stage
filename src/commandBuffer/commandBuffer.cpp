#include "commandBuffer.hpp"

#include "device.hpp"
#include "volk.h"

#include <iostream>
#include <vector>

#include "structs_vk.hpp"

namespace vk_stage {

CommandBuffer::CommandBuffer(VkCommandPool vkcommandPool, VkQueue vkQueue, Device *device, VkCommandBuffer commandBuffer)
    : vkcommandPool(vkcommandPool), vkQueue(vkQueue), device(device), vkcommandBuffer(commandBuffer) {}

CommandBuffer::~CommandBuffer() {}

void CommandBuffer::destroy() {

    vkFreeCommandBuffers(device->device(), vkcommandPool, 1, &vkcommandBuffer);
}

void CommandBuffer::beginCommandBuffer() {
    auto beginInfo = make<VkCommandBufferBeginInfo>();
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(vkcommandBuffer, &beginInfo) != VK_SUCCESS) {
        throw std::runtime_error("Failed to begin commandBuffer");
    }
}

void CommandBuffer::endCommandBuffer() { vkEndCommandBuffer(vkcommandBuffer); }

void CommandBuffer::submitCommandBuffer2(
    std::vector<SemaphoreSubmitInfo> waitSemaphores, std::vector<SemaphoreSubmitInfo> signalSemaphores, VkFence fence) {
    submitCommandBuffers2({*this}, waitSemaphores, signalSemaphores, fence);
}

void CommandBuffer::submitCommandBuffer(
    std::vector<SemaphoreSubmitInfo> waitSemaphores, std::vector<SemaphoreSubmitInfo> signalSemaphores, VkFence fence) {
    submitCommandBuffers({*this}, waitSemaphores, signalSemaphores, fence);
}

void CommandBuffer::submitCommandBuffers(
    std::vector<CommandBuffer> submitCommandBuffers,
    std::vector<SemaphoreSubmitInfo> waitSemaphores,
    std::vector<SemaphoreSubmitInfo> signalSemaphores,
    VkFence fence) {

    std::vector<VkSemaphore> waitSemaphoresSubmitInfo(waitSemaphores.size());
    std::vector<VkSemaphore> signalSemaphoresSubmitInfo(signalSemaphores.size());
    std::vector<VkCommandBuffer> cmdsSubmitInfo(submitCommandBuffers.size());

    for (int i = 0; i < waitSemaphoresSubmitInfo.size(); i++) {
        waitSemaphoresSubmitInfo[i] = waitSemaphores[i].vkSemaphore;

    }

    for (int i = 0; i < signalSemaphoresSubmitInfo.size(); i++) {
        signalSemaphoresSubmitInfo[i] = signalSemaphores[i].vkSemaphore;

    }

    for (int i = 0; i < cmdsSubmitInfo.size(); i++) {
        cmdsSubmitInfo[i] = submitCommandBuffers[i].vkcommandBuffer;
    }
    VkPipelineStageFlags waitFlag = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    auto submitInfo = make<VkSubmitInfo>();
    submitInfo.waitSemaphoreCount = waitSemaphoresSubmitInfo.size();
    submitInfo.pWaitSemaphores = waitSemaphoresSubmitInfo.data();
    submitInfo.pWaitDstStageMask = &waitFlag;
    submitInfo.signalSemaphoreCount = signalSemaphoresSubmitInfo.size();
    submitInfo.pSignalSemaphores = signalSemaphoresSubmitInfo.data();
    
    submitInfo.commandBufferCount = cmdsSubmitInfo.size();
    submitInfo.pCommandBuffers = cmdsSubmitInfo.data();
    if (vkQueueSubmit(submitCommandBuffers[0].vkQueue, 1, &submitInfo, fence) != VK_SUCCESS) {
        throw std::runtime_error("Failed to submit commandBuffer");
    }
}


void CommandBuffer::submitCommandBuffers2(
    std::vector<CommandBuffer> submitCommandBuffers,
    std::vector<SemaphoreSubmitInfo> waitSemaphores,
    std::vector<SemaphoreSubmitInfo> signalSemaphores,
    VkFence fence) {
    std::vector<VkSemaphoreSubmitInfo> waitSemaphoresSubmitInfo(waitSemaphores.size(), make<VkSemaphoreSubmitInfo>());
    std::vector<VkSemaphoreSubmitInfo> signalSemaphoresSubmitInfo(signalSemaphores.size(), make<VkSemaphoreSubmitInfo>());
    std::vector<VkCommandBufferSubmitInfo> cmdsSubmitInfo(submitCommandBuffers.size(), make<VkCommandBufferSubmitInfo>());

    for (int i = 0; i < waitSemaphoresSubmitInfo.size(); i++) {
        waitSemaphoresSubmitInfo[i].semaphore = waitSemaphores[i].vkSemaphore;
        waitSemaphoresSubmitInfo[i].stageMask = waitSemaphores[i].vkPipelineStage;
        waitSemaphoresSubmitInfo[i].value = waitSemaphores[i].value;
        waitSemaphoresSubmitInfo[i].deviceIndex = 0;
    }

    for (int i = 0; i < signalSemaphoresSubmitInfo.size(); i++) {
        signalSemaphoresSubmitInfo[i].semaphore = signalSemaphores[i].vkSemaphore;
        signalSemaphoresSubmitInfo[i].stageMask = signalSemaphores[i].vkPipelineStage;
        signalSemaphoresSubmitInfo[i].value = signalSemaphores[i].value;
        signalSemaphoresSubmitInfo[i].deviceIndex = 0;
    }

    for (int i = 0; i < cmdsSubmitInfo.size(); i++) {
        cmdsSubmitInfo[i].commandBuffer = submitCommandBuffers[i].vkcommandBuffer;
    }

    auto submitInfo = make<VkSubmitInfo2>();
    submitInfo.waitSemaphoreInfoCount = waitSemaphoresSubmitInfo.size();
    submitInfo.pWaitSemaphoreInfos = waitSemaphoresSubmitInfo.data();
    submitInfo.signalSemaphoreInfoCount = signalSemaphoresSubmitInfo.size();
    submitInfo.pSignalSemaphoreInfos = signalSemaphoresSubmitInfo.data();
    submitInfo.commandBufferInfoCount = cmdsSubmitInfo.size();
    submitInfo.pCommandBufferInfos = cmdsSubmitInfo.data();
    if (vkQueueSubmit2(submitCommandBuffers[0].vkQueue, 1, &submitInfo, fence) != VK_SUCCESS) {
        throw std::runtime_error("Failed to submit commandBuffer");
    }
}

}  // namespace vk_stage