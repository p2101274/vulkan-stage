#pragma once

#include <vector>
#include "device.hpp"
#include "ressources/buffer.hpp"
#include "volk.h"
#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"
#include "utils.hpp"
namespace vk_stage {


class CommandBuffer {
   public:
    CommandBuffer(VkCommandPool vkcommandPool, VkQueue vkQueue, Device *device, VkCommandBuffer commandBuffer);
    ~CommandBuffer();

    void destroy();
    void beginCommandBuffer();
    void endCommandBuffer();

    void submitCommandBuffer2(
        std::vector<SemaphoreSubmitInfo> waitSemaphores, std::vector<SemaphoreSubmitInfo> signalSemaphores, VkFence fence);
    
    void submitCommandBuffer(
        std::vector<SemaphoreSubmitInfo> waitSemaphores, std::vector<SemaphoreSubmitInfo> signalSemaphores, VkFence fence);

     static void submitCommandBuffers(
        std::vector<CommandBuffer> submitCommandBuffers,
        std::vector<SemaphoreSubmitInfo> waitSemaphores,
        std::vector<SemaphoreSubmitInfo> signalSemaphores,
        VkFence fence);

    static void submitCommandBuffers2(
        std::vector<CommandBuffer> submitCommandBuffers,
        std::vector<SemaphoreSubmitInfo> waitSemaphores,
        std::vector<SemaphoreSubmitInfo> signalSemaphores,
        VkFence fence);

    VkCommandBuffer getCommandBuffer() { return vkcommandBuffer; }

   private:
    VkCommandPool vkcommandPool;
    VkQueue vkQueue;
    Device *device;
    VkCommandBuffer vkcommandBuffer;
};
}  // namespace vk_stage