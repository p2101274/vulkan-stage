#pragma once

#include "volk.h"
#include <string>


#include "device.hpp"
#include "window.hpp"

namespace vk_stage {
    
class App {
   public:
    App();
    ~App();
    void init();
    void run();

   private:


    Window window{1280, 720, "Hello Vulkan"};
    Device device{window};
    
    VkSurfaceKHR vkSurface;
};
}  // namespace vk_stage