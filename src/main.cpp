#include <cstdlib>
#include <iostream>

#include "app.hpp"

int main() {
    
    vk_stage::App app{};

    try {
        app.init();
        app.run();
    } catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}