#pragma once

#include "resizable.hpp"
#include "volk.h"


#include <cstdint>
#include <vector>


#include "device.hpp"
#include "ressources/image.hpp"
#include "swapchain.hpp"
namespace vk_stage {
enum depthAndStencil {
    NONE,
    DEPTH,
    DEPTH_AND_STENCIL,
};


enum renderPassModeEnum{
    DEFAULT = 0,
    ADDIDITIVE = 1,
};
class DynamicRenderPass : public Resizable {
    struct AttachementStruct{
        std::vector<VkRenderingAttachmentInfo> colorAttachments;
        VkRenderingAttachmentInfo depthAttachment;
    };


   public:
    DynamicRenderPass(
        Device *device,
        VkExtent2D frameSize,
        std::vector<VkFormat> imageFormats,
        unsigned int numberOfFrame,
        depthAndStencil enableDepthAndStencil,
        SwapChain *swapChain = nullptr,
        std::vector<Image> *externalDepthImages = {}
        );
    ~DynamicRenderPass();

    void beginRenderPass(VkCommandBuffer commandBuffer, unsigned imageIndex, renderPassModeEnum renderPassMode = DEFAULT, VkRenderingFlags optionalRenderingFlag = 0);
    void endRenderPass(VkCommandBuffer commandBuffer);

    void resize(VkExtent2D frameSize);
    void setClearColor(RGB rgb);
    void transitionColorAttachment(uint32_t frameIndex, VkImageLayout newLayout, VkCommandBuffer commandBuffer);


    std::vector<Image>& getDepthAndStencilAttachement(){return depthAndStencilAttachement;};
    std::vector<Image>* getDepthAndStencilPtr(){return &depthAndStencilAttachement;}
    std::vector<std::vector<Image>>& getimageAttachement(){return imageAttachement;};
    VkExtent2D getFrameSize(){return frameSize;};

   private:
    void createRessources();
    void createAttachmentInfo();
    void createRenderpassInfo();

    unsigned int numberOfFrame;
    VkExtent2D frameSize;
    depthAndStencil enableDepthAndStencil;
    SwapChain *swapChain;

    std::vector<AttachementStruct> attachments;
    std::vector<std::vector<Image>> imageAttachement;
    std::vector<Image> depthAndStencilAttachement;
    std::vector<VkFormat> imageFormats;
    std::vector<Image> *externalDepthImages;
    std::vector<VkRenderingInfo> renderingInfos;

    Device *device;
};
}  // namespace vk_stage