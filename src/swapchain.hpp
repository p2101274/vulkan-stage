#pragma once

#include <vector>
#include <chrono>

#include "volk.h"

#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"
#include "device.hpp"
#include "ressources/image.hpp"
#include "synchronisation/fence.hpp"
#include "synchronisation/semaphore.hpp"
#include "utils.hpp"
namespace vk_stage {

class SwapChain {
   public:
    SwapChain(Device *device, VkExtent2D windowExtent, vkb::SwapchainBuilder::BufferMode bufferingMode);
    ~SwapChain();

    void recreateSwapchain(VkExtent2D windowExtent);
    VkResult acquireNextImage(uint32_t* currentSwapchainImage, size_t* frameIndex, std::vector<SyncObject>& sync, std::chrono::system_clock::time_point frameChrono);
    VkResult presentFrame(uint32_t* currentSwapchainImage, size_t* frameIndex, SyncObject& sync);
    float extentAspectRatio() { 
        return static_cast<float>(vkbSwapchain.extent.width) / static_cast<float>(vkbSwapchain.extent.height); 
        }
    Image getSwapChainImage(unsigned int index) { return swapChainImage[index]; }
    std::vector<Image> &getswapChainImages(){return swapChainImage;};
   private:
    void generateSwapchainImages();
    void createSyncObjects();

    unsigned int numberOfFrame = 0;
    std::vector<Image> swapChainImage;
    std::vector<Fence> imageAvailableFences;
    std::vector<Semaphore> imageAvailableSemaphores;
    std::vector<VkImageView> swapChainImageView;
    vkb::SwapchainBuilder::BufferMode bufferingMode;
    VkQueue presentQueue;
    Device *device;
    vkb::Swapchain vkbSwapchain;
};

}  // namespace vk_stage