
#include "renderer.hpp"
#include <vulkan/vulkan_core.h>

#include "commandBuffer/commandBufferPoolHandler.hpp"
#include "volk.h"

#include <iostream>
#include <mutex>
#include <vector>

#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"

#include "synchronisation/semaphore.hpp"
#include "swapchain.hpp"


namespace vk_stage {

    bool Renderer::stopRendering = false;
    std::condition_variable Renderer::waitToStartRendering;

Renderer::Renderer(Device *device, Window &window)
    : device(device), window(window), swapChain(device, window.getExtent(), (vkb::SwapchainBuilder::BufferMode)MAX_FRAMES_IN_FLIGHT) {
    createCommandBuffers();
    createSyncObject();
}

Renderer::~Renderer() {
    for (auto &pcs : preComputeSemaphores) {
        pcs.destroy();
    }
    for (auto &rs : renderingSemaphores) {
        rs.destroy();
    }
    for (auto &pps : postProcessingSemaphores) {
        pps.destroy();
    }
    
}

bool Renderer::startFrame() {
    if(stopRendering){
        std::unique_lock<std::mutex> ul (stopRenderingMutex);
        while (stopRendering) {
            waitToStartRendering.wait(ul);
        }
    }


    VkResult result = swapChain.acquireNextImage(&swapChainImageIndex, &currentFrameIndex, sync, frameChrono);
    

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        resize();
        return false;
    }
    if(result == VK_TIMEOUT){
        return false;
    }
    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        throw std::runtime_error("failed to acquire swap chain image!");
    }

    return true;


}

VkCommandBuffer Renderer::startPrecompute() {
    commandBuffers[currentFrameIndex].preComputeCmdBuffer.beginCommandBuffer();
    return commandBuffers[currentFrameIndex].preComputeCmdBuffer.getCommandBuffer();
}

void Renderer::endPrecompute() {
    commandBuffers[currentFrameIndex].preComputeCmdBuffer.endCommandBuffer();
    SemaphoreSubmitInfo signalSemaphore{preComputeSemaphores[currentFrameIndex].semaphore(), VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT, 0};

    commandBuffers[currentFrameIndex].preComputeCmdBuffer.submitCommandBuffer(
        sync[currentFrameIndex].waitToStartPreComputeSemaphores, {signalSemaphore}, VK_NULL_HANDLE);

    sync[currentFrameIndex].waitToStartRenderingSemaphores.push_back(signalSemaphore);
}

VkCommandBuffer Renderer::startRendering() {
    commandBuffers[currentFrameIndex].renderingCmdBuffer.beginCommandBuffer();
    swapChain.getSwapChainImage(swapChainImageIndex)
        .transitionImageLayout(
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            commandBuffers[currentFrameIndex].renderingCmdBuffer.getCommandBuffer());
    return commandBuffers[currentFrameIndex].renderingCmdBuffer.getCommandBuffer();
}

void Renderer::endRendering(bool signalFence) {
    if (signalFence) {
        swapChain.getSwapChainImage(swapChainImageIndex)
            .transitionImageLayout(
                VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                commandBuffers[currentFrameIndex].renderingCmdBuffer.getCommandBuffer());
    }
    commandBuffers[currentFrameIndex].renderingCmdBuffer.endCommandBuffer();
    SemaphoreSubmitInfo signalSemaphore{
        renderingSemaphores[currentFrameIndex].semaphore(), VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT, 0};

    frameChrono = std::chrono::high_resolution_clock::now();
    commandBuffers[currentFrameIndex].renderingCmdBuffer.submitCommandBuffer(
        sync[currentFrameIndex].waitToStartRenderingSemaphores, {signalSemaphore},
        (signalFence) ? sync[currentFrameIndex].imageAvailableFence : VK_NULL_HANDLE);

    if (signalFence) {
        sync[currentFrameIndex].waitToPrensentSemaphores.push_back(signalSemaphore);
    } else {
        sync[currentFrameIndex].waitToStartPostProcessingSemaphores.push_back(signalSemaphore);
    }
}

void Renderer::endAndPresentFrame() {
    auto result = swapChain.presentFrame(&swapChainImageIndex, &currentFrameIndex, sync[currentFrameIndex]);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || window.wasWindowResized()) {
        window.resetWindowResizedFlag();
        resize();
    } else if (result != VK_SUCCESS) {
        throw std::runtime_error("failed to present swap chain image!");
    }
}

void Renderer::createCommandBuffers() {
    commandBuffers.reserve(MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        std::vector<CommandBuffer> cmdBuffers = CommandBufferPoolHandler::defaultPool(device)->createCommandBuffer(3);
        // RendererCommandBuffer frameCommandBuffer{cmdBuffers[0],cmdBuffers[1],cmdBuffers[2]};
        commandBuffers.emplace_back(std::move(cmdBuffers[0]), std::move(cmdBuffers[1]), std::move(cmdBuffers[2]));
        int x = 1;
    }
}

void Renderer::createSyncObject() {
    sync.resize(MAX_FRAMES_IN_FLIGHT);
    preComputeSemaphores.reserve(MAX_FRAMES_IN_FLIGHT);
    renderingSemaphores.reserve(MAX_FRAMES_IN_FLIGHT);
    postProcessingSemaphores.reserve(MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        preComputeSemaphores.emplace_back(Semaphore(device, VK_SEMAPHORE_TYPE_BINARY));
        renderingSemaphores.emplace_back(Semaphore(device, VK_SEMAPHORE_TYPE_BINARY));
        postProcessingSemaphores.emplace_back(Semaphore(device, VK_SEMAPHORE_TYPE_BINARY));
    }
}

void Renderer::resize() {
    auto extent = window.getExtent();
    while (extent.width == 0 || extent.height == 0) {
        extent = window.getExtent();
        glfwWaitEvents();
    }
    vkDeviceWaitIdle(device->device());
    swapChain.recreateSwapchain(extent);
    for (auto &obj : resizableObjects) {
        obj->resize(extent);
    }
}
}  // namespace vk_stage