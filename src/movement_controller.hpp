#pragma once

#include <GLFW/glfw3.h>
#include "scene/camera.hpp"
#include "scene/scene.hpp"
#include "window.hpp"

namespace vk_stage {
class MovementController {
   public:
   MovementController(Window &windowObj);
    struct KeyMappings {
        int moveLeft = GLFW_KEY_A;
        int moveRight = GLFW_KEY_D;
        int moveForward = GLFW_KEY_W;
        int moveBackward = GLFW_KEY_S;
        int moveUp = GLFW_KEY_E;
        int moveDown = GLFW_KEY_Q;
        int lookLeft = GLFW_KEY_LEFT;
        int lookRight = GLFW_KEY_RIGHT;
        int lookUp = GLFW_KEY_UP;
        int lookDown = GLFW_KEY_DOWN;
        int space = GLFW_KEY_SPACE;
    };

    void moveInPlaneXZ(GLFWwindow* window, float dt, Camera& cam);
    static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
    void addpointLight(GLFWwindow* window, float dt, Scene& s);
    Window &windowObj;
    KeyMappings keys{};
    bool waspressed = false;
    float moveSpeed{3.f};
    float lookSpeed{1.5f};
};
}  // namespace vk_stage