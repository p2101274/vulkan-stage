#pragma once
#include "volk.h"
namespace vk_stage {

class Resizable {
    public:
    virtual void resize(VkExtent2D frameSize) = 0;
};
}  // namespace vk_stage