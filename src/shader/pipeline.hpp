#pragma once


#include "descriptor/descriptorSetLayout.hpp"

#include <cstdint>
#include <memory>

#include "utils.hpp"
#include "device.hpp"

#include "volk.h"

namespace vk_stage {


class Pipeline {
   public:
    virtual void bindPipeline(VkCommandBuffer cmdBuffer) = 0;
    virtual void reloadShader(VkShaderStageFlagBits shaderStageToReload) = 0;
    std::shared_ptr<DescriptorSetLayout> getDescriptorSetLayout(uint32_t id){return pipelineDescriptorsSetsLayoutList[id];}
    VkPipelineLayout getPipelineLayout(){return pipelineLayout;};
    
   private:

    VkShaderStageFlags pipelineStageFlags = 0;
    std::map<uint32_t ,std::shared_ptr<DescriptorSetLayout>> pipelineDescriptorsSetsLayoutList;
    VkPipelineLayout pipelineLayout;
    Device *device;
};

}  // namespace vk_stage