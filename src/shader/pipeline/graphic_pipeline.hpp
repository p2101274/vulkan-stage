#pragma once

#include "descriptor/descriptorSetLayout.hpp"
#include "shader/pipeline.hpp"
#include "volk.h"
#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
#include "utils.hpp"
#include "device.hpp"
#include "../shader.hpp"
#include "volk.h"

namespace vk_stage {
struct GraphicPipelineCreateInfo{
    std::string vexterShaderFile;
    std::string fragmentShaderFile;
    std::string tesselationControlShaderFile;
    std::string tesselationEvaluationShaderFile;
    std::string geometryShaderFile;
};

class GraphicPipeline : Pipeline {
   public:
    GraphicPipeline(Device *device, GraphicPipelineCreateInfo& pipelineCreateInfo);
    void destroy();


    void bindPipeline(VkCommandBuffer cmdBuffer);
    std::shared_ptr<DescriptorSetLayout> getDescriptorSetLayout(uint32_t id){return pipelineDescriptorsSetsLayoutList[id];}
    VkPipelineLayout getPipelineLayout(){return pipelineLayout;};
    void reloadShader(VkShaderStageFlagBits shaderStageToReload);

   private:

    void createShaders(GraphicPipelineCreateInfo& pipelineCreateInfo);

    Shader * createFragmentShader(GraphicPipelineCreateInfo& pipelineCreateInfo);
    Shader * createVertexShader(GraphicPipelineCreateInfo& pipelineCreateInfo, VkShaderStageFlagBits nextStageFlag);
    Shader * createTesselationControlShader(GraphicPipelineCreateInfo& pipelineCreateInfo, VkShaderStageFlagBits nextStageFlag);
    Shader * createTesselationEvaluationShader(GraphicPipelineCreateInfo& pipelineCreateInfo, VkShaderStageFlagBits nextStageFlag);
    Shader * createGeometryShader(GraphicPipelineCreateInfo& pipelineCreateInfo, VkShaderStageFlagBits nextStageFlag);

    void setPipelineStage(GraphicPipelineCreateInfo& pipelineCreateInfo);
    void createPipelineLayout(GraphicPipelineCreateInfo& pipelineCreateInfo);
    void createVertexShaderInfo();

    void setVextexInfo(VkCommandBuffer cmdBuffer);

    void setRasterizerInfo(VkCommandBuffer cmdBuffer);

    void setFragmentInfo(VkCommandBuffer cmdBuffer);

    VkVertexInputBindingDescription2EXT vertexInputBinding;
    std::vector<VkVertexInputAttributeDescription2EXT> vertexAttributes;

    std::map<VkShaderStageFlagBits, Shader*> shadersMap;
    
    VkShaderStageFlags pipelineStageFlags = 0;
    std::unordered_map<std::vector<uint32_t>, std::shared_ptr<DescriptorSetLayout>> pipelineDescriptorsSetsLayout;
    std::map<uint32_t ,std::shared_ptr<DescriptorSetLayout>> pipelineDescriptorsSetsLayoutList;
    VkPipelineLayout pipelineLayout;


    Device *device;
};

}  // namespace vk_stage