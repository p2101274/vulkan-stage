#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "../shader.hpp"
#include "descriptor/descriptorSetLayout.hpp"
#include "device.hpp"
#include "shader/pipeline.hpp"
#include "utils.hpp"
#include "volk.h"

namespace vk_stage {

class ComputePipeline : Pipeline {
   public:
    ComputePipeline(Device* device, std::string computeShaderName);
    
    void destroy();

    void bindPipeline(VkCommandBuffer cmdBuffer);
    VkPipelineLayout getPipelineLayout() { return pipelineLayout; };
    void reloadShader(VkShaderStageFlagBits shaderStageToReload);
    std::vector<std::shared_ptr<DescriptorSetLayout>>& getDescriptorsSetLayout(){return  computeShader->getDescriptorsSetLayout();}

   private:
    void createShaders(std::string& computeShaderName);
    void createPipelineLayout();

    Shader* computeShader;


    VkPipelineLayout pipelineLayout;

    Device* device;
};

}  // namespace vk_stage