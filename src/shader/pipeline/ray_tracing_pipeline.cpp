
#include "ray_tracing_pipeline.hpp"

#include <vulkan/vulkan_core.h>

#include <cassert>
#include <cstdint>
#include <stdexcept>
#include <vector>

#include "shader/shader.hpp"
#include "structs_vk.hpp"

namespace vk_stage {

RayTracingPipeline::RayTracingPipeline(Device *device, std::vector<std::string> &shadersNamelist, std::vector<SBTLine> &SBTLayout)
    : device(device) {
    loadShader(shadersNamelist);
    createShaderGroup(SBTLayout);
    createPipelineLayout();
    buildPipeline();
    createShaderBindingTable();
}

void RayTracingPipeline::bindPipeline(VkCommandBuffer cmdBuffer) {
    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipeline);
}

void RayTracingPipeline::reloadShader(VkShaderStageFlagBits shaderStageToReload) {}

void RayTracingPipeline::createPipelineLayout() {
    std::vector<VkDescriptorSetLayout> descriptorSetLayoutVector;

    for (auto &descriptorSetLayout : pipelineDescriptorsSetsLayoutMap) {
        pipelineDescriptorsSetsLayoutList[descriptorSetLayout.first[0]] = descriptorSetLayout.second;
    }

    for (auto &descriptorSetLayout : pipelineDescriptorsSetsLayoutList) {
        descriptorSetLayoutVector.push_back(descriptorSetLayout.second->getDescriptorSetLayout());
    }
    VkPushConstantRange pushConstantInfo = shadersList.begin()->getPushConstants();
    pushConstantInfo.stageFlags = 
                                  VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR | VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
                                  pushConstantInfo.size = 32;


    auto pipelineLayoutInfo = make<VkPipelineLayoutCreateInfo>();
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = static_cast<uint32_t>(descriptorSetLayoutVector.size());
    pipelineLayoutInfo.pSetLayouts = descriptorSetLayoutVector.data();
    if (pushConstantInfo.size > 0) {
        pipelineLayoutInfo.pushConstantRangeCount = 1;
        pipelineLayoutInfo.pPushConstantRanges = &pushConstantInfo;
    }

    if (vkCreatePipelineLayout(device->device(), &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create pipeline layout!");
    }
}

void RayTracingPipeline::loadShader(std::vector<std::string> &shadersNamelist) {
    VkShaderStageFlags flags = VK_SHADER_STAGE_ANY_HIT_BIT_KHR | VK_SHADER_STAGE_INTERSECTION_BIT_KHR |
                               VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_RAYGEN_BIT_KHR | VK_SHADER_STAGE_MISS_BIT_KHR |
                               VK_SHADER_STAGE_CALLABLE_BIT_KHR;

    for (auto &shadersName : shadersNamelist) {
        shadersList.push_back(Shader(device, shadersName, flags));
        shadersList.back().createShaderModule();
        for (auto &descriptorSetlayout : shadersList.back().getDescriptorsSetLayout()) {
            pipelineDescriptorsSetsLayoutMap[descriptorSetlayout->getId()] = descriptorSetlayout;
        }
    }
}

/*
SBT Layout used in this sample:
        /-----------\
        | raygen    |
        |-----------|
        | callable  |
        | ...       |
        |-----------|
        | miss      |
        | ...       |
        |-----------|
        | hit       |
        | ...       |
        \-----------/

*/
void RayTracingPipeline::createShaderGroup(std::vector<SBTLine> &SBTLayout) {
    int i = 0;

    enum SBTstate { genRay, call, miss, hit };

    SBTstate state = genRay;

    for (auto &line : SBTLayout) {
        uint32_t shaderId;
        if (line.generalShader != VK_SHADER_UNUSED_KHR) {
            shaderId = line.generalShader;

        } else if (line.closestHitShader != VK_SHADER_UNUSED_KHR) {
            shaderId = line.closestHitShader;
        }

        assert((shaderId != VK_SHADER_UNUSED_KHR) && "Au moins un shader doit être présent dans la ligne de la Shader Binding Table");

        VkShaderStageFlagBits stage = shadersList[shaderId].getShaderStage();

        if (state == genRay && stage != VK_SHADER_STAGE_RAYGEN_BIT_KHR) {
            throw std::runtime_error("Le premier groupe dois être la génération de rayon");
        } else if (
            state == call && (stage != VK_SHADER_STAGE_CALLABLE_BIT_KHR && stage != VK_SHADER_STAGE_MISS_BIT_KHR &&
                              stage != VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR)) {
            throw std::runtime_error("Le shader après celui de raygen doit être un callable ou un miss ou un hit shader");

        } else if (state == miss && (stage != VK_SHADER_STAGE_MISS_BIT_KHR && stage != VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR)) {
            throw std::runtime_error("Le shader après celui de raygen doit être un miss ou un hit shader");
        } else if (state == hit && (stage != VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR)) {
            throw std::runtime_error("Le shader après celui de raygen doit être un hit shader");
        }

        auto shaderGroupLine = make<VkRayTracingShaderGroupCreateInfoKHR>();
        if (line.generalShader != VK_SHADER_UNUSED_KHR) {
            shaderGroupLine.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        } else if (line.intersectionShader == VK_SHADER_UNUSED_KHR) {
            shaderGroupLine.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
        } else {
            shaderGroupLine.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_PROCEDURAL_HIT_GROUP_KHR;
        }
        shaderGroupLine.generalShader = line.generalShader;
        shaderGroupLine.closestHitShader = line.closestHitShader;
        shaderGroupLine.anyHitShader = line.anyHitShader;
        shaderGroupLine.intersectionShader = line.intersectionShader;
        shaderGroup.push_back(shaderGroupLine);

        if (i == 0) {
            state = call;
        } else if (stage == VK_SHADER_STAGE_CALLABLE_BIT_KHR) {
            state = call;
            if (callableShadersIndex == 0) {
                callableShadersIndex = i;
            }
            callableShadersSize++;
        } else if (stage == VK_SHADER_STAGE_MISS_BIT_KHR) {
            state = miss;
            if (missShadersIndex == 0) {
                missShadersIndex = i;
            }
            missShadersSize++;
        } else if (stage == VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR) {
            state = hit;
            if (hitShadersIndex == 0) {
                hitShadersIndex = i;
            }
            hitShadersSize++;
        }

        i++;
    }
}

uint32_t alignedSize(uint32_t value, uint32_t alignment) { return (value + alignment - 1) & ~(alignment - 1); }

void RayTracingPipeline::createShaderBindingTable() {
    handleSize = device->rayTracingPipelineProperties.shaderGroupHandleSize;
    handleSizeAligned = alignedSize(
        device->rayTracingPipelineProperties.shaderGroupHandleSize, device->rayTracingPipelineProperties.shaderGroupHandleAlignment);
    const uint32_t groupCount = static_cast<uint32_t>(shaderGroup.size());
    const uint32_t sbtSize = groupCount * handleSizeAligned;

    std::vector<uint8_t> shaderHandleStorage(sbtSize);
    auto res = vkGetRayTracingShaderGroupHandlesKHR(device->device(), pipeline, 0, groupCount, sbtSize, shaderHandleStorage.data());

    if (res != VK_SUCCESS) {
        throw std::runtime_error("failed to get shader group Handle");
    }

    const VkBufferUsageFlags bufferUsageFlags = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    const VkMemoryPropertyFlags memoryUsageFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    rayGenBuffer = Buffer(device, handleSize, 1, bufferUsageFlags, memoryUsageFlags, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);
    callBuffer = Buffer(device, handleSize, 1, bufferUsageFlags, memoryUsageFlags, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);
    missBuffer = Buffer(device, handleSize, 1, bufferUsageFlags, memoryUsageFlags, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);
    hitBuffer = Buffer(device, handleSize, hitShadersSize, bufferUsageFlags, memoryUsageFlags, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    rayGenBuffer.map();
    callBuffer.map();
    missBuffer.map();
    hitBuffer.map();

    rayGenBuffer.writeToBuffer(shaderHandleStorage.data(), handleSize);
    callBuffer.writeToBuffer(shaderHandleStorage.data() + handleSize, handleSize);
    missBuffer.writeToBuffer(shaderHandleStorage.data() + handleSize * 1, handleSize);
    hitBuffer.writeToBuffer(shaderHandleStorage.data() + handleSize * 2, handleSize*hitShadersSize);
}

void RayTracingPipeline::buildPipeline() {
    std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
    for (auto &shader : shadersList) {
        auto shaderStageCreateInfo = make<VkPipelineShaderStageCreateInfo>();
        shaderStageCreateInfo.stage = shader.getShaderStage();
        shaderStageCreateInfo.module = shader.getShaderModule();
        shaderStageCreateInfo.pName = "main";
        shaderStages.push_back(shaderStageCreateInfo);
    }

    VkRayTracingPipelineCreateInfoKHR rayTracingPipelineCI{};
    rayTracingPipelineCI.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    rayTracingPipelineCI.stageCount = static_cast<uint32_t>(shaderStages.size());
    rayTracingPipelineCI.pStages = shaderStages.data();
    rayTracingPipelineCI.groupCount = static_cast<uint32_t>(shaderGroup.size());
    rayTracingPipelineCI.pGroups = shaderGroup.data();
    rayTracingPipelineCI.maxPipelineRayRecursionDepth = 1;
    rayTracingPipelineCI.layout = pipelineLayout;

    auto res =
        vkCreateRayTracingPipelinesKHR(device->device(), VK_NULL_HANDLE, VK_NULL_HANDLE, 1, &rayTracingPipelineCI, nullptr, &pipeline);

    if (res != VK_SUCCESS) {
        throw std::runtime_error("failed to create ray tracing pipeline!");
    }
}

}  // namespace vk_stage