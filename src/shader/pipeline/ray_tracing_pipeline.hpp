#pragma once


#include <cstdint>
#include <string>
#include <vector>

#include "device.hpp"
#include "ressources/buffer.hpp"
#include "shader/pipeline.hpp"
#include "shader/shader.hpp"
namespace vk_stage {
struct SBTLine {
    uint32_t generalShader = VK_SHADER_UNUSED_KHR;
    uint32_t closestHitShader = VK_SHADER_UNUSED_KHR;
    uint32_t anyHitShader = VK_SHADER_UNUSED_KHR;
    uint32_t intersectionShader = VK_SHADER_UNUSED_KHR;
};

class RayTracingPipeline : Pipeline {
   public:
    RayTracingPipeline(Device *device, std::vector<std::string> &shadersNamelist, std::vector<SBTLine> &SBTLayout);

    void bindPipeline(VkCommandBuffer cmdBuffer);
    void reloadShader(VkShaderStageFlagBits shaderStageToReload);
    std::shared_ptr<DescriptorSetLayout> getDescriptorSetLayout(uint32_t id){return pipelineDescriptorsSetsLayoutList[id];}
    VkPipelineLayout getPipelineLayout(){return pipelineLayout;}
    Buffer rayGenBuffer;
    Buffer callBuffer;
    Buffer missBuffer;
    Buffer hitBuffer;

     uint32_t handleSize;
     uint32_t handleSizeAligned;

   private:
    void createPipelineLayout();
    void loadShader(std::vector<std::string> &shadersNamelist);
    void createShaderGroup(std::vector<SBTLine> &SBTLayout);
    void createShaderBindingTable();
    void buildPipeline();

    std::vector<Shader> shadersList;
    std::vector<VkRayTracingShaderGroupCreateInfoKHR> shaderGroup;

    uint32_t callableShadersIndex = 0;
    uint32_t callableShadersSize = 0;

    uint32_t missShadersIndex = 0;
    uint32_t missShadersSize = 0;

    uint32_t hitShadersIndex = 0;
    uint32_t hitShadersSize = 0;



    std::map<uint32_t, std::shared_ptr<DescriptorSetLayout>> pipelineDescriptorsSetsLayoutList;
    std::unordered_map<std::vector<uint32_t>, std::shared_ptr<DescriptorSetLayout>> pipelineDescriptorsSetsLayoutMap;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    Device *device;
};

}  // namespace vk_stage