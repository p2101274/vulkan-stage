
#include "swapchain.hpp"

#include <vulkan/vulkan_core.h>

#include <iostream>
#include <ostream>
#include <vector>

#include "volk.h"

#define VK_NO_PROTOTYPES
#include <chrono>

#include "VkBootstrap.h"
#include "structs_vk.hpp"
#include "utils.hpp"
namespace vk_stage {

SwapChain::SwapChain(Device *device, VkExtent2D windowExtent, vkb::SwapchainBuilder::BufferMode bufferingMode)
    : device(device), bufferingMode(bufferingMode) {

    vkb::SwapchainBuilder swapchain_builder{device->getvkbDevice()};
    swapchain_builder.set_desired_min_image_count(bufferingMode + 1);
    swapchain_builder.set_desired_extent(windowExtent.width, windowExtent.height);
    swapchain_builder.set_image_usage_flags(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
    
    //swapchain_builder.set_desired_present_mode(VK_PRESENT_MODE_IMMEDIATE_KHR);
    auto swap_ret = swapchain_builder.build();

    if (!swap_ret) {
        throw std::runtime_error(swap_ret.error().message());
    }
    
    vkbSwapchain = swap_ret.value();
    numberOfFrame = bufferingMode + 1;
    presentQueue = device->get_queue(vkb::QueueType::present).value();
    generateSwapchainImages();
    createSyncObjects();
}

SwapChain::~SwapChain() {
    for (auto &f : imageAvailableFences) {
        f.destroy();
    }

    for (auto &s : imageAvailableSemaphores) {
        s.destroy();
    }

    vkbSwapchain.destroy_image_views(swapChainImageView);
    vkb::destroy_swapchain(vkbSwapchain);
}

void SwapChain::generateSwapchainImages() {
    auto vkImages = vkbSwapchain.get_images().value();
    swapChainImage.clear();
    swapChainImage.reserve(vkImages.size());

    swapChainImageView = vkbSwapchain.get_image_views().value();
    
    for (int i = 0; i < vkbSwapchain.image_count; i++) {
        swapChainImage.emplace_back(device, vkImages[i], swapChainImageView[i], vkbSwapchain.image_format, vkbSwapchain.extent);
    }
}

void SwapChain::recreateSwapchain(VkExtent2D windowExtent) {
    for (auto &f : imageAvailableFences) {
        f.destroy();
    }

    for (auto &s : imageAvailableSemaphores) {
        s.destroy();
    }

    vkbSwapchain.destroy_image_views(swapChainImageView);
    vkb::SwapchainBuilder swapchain_builder{device->getvkbDevice()};
    swapchain_builder.set_old_swapchain(vkbSwapchain);
    swapchain_builder.set_desired_extent(windowExtent.width, windowExtent.height);
    swapchain_builder.set_desired_min_image_count(bufferingMode + 1);
    swapchain_builder.set_image_usage_flags(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
    //swapchain_builder.set_desired_present_mode(VK_PRESENT_MODE_IMMEDIATE_KHR);
    auto swap_ret = swapchain_builder.build();
    if (!swap_ret) {
        throw std::runtime_error(swap_ret.error().message());
    }

    vkb::destroy_swapchain(vkbSwapchain);

    vkbSwapchain = swap_ret.value();
    generateSwapchainImages();
    createSyncObjects();
}

VkResult SwapChain::acquireNextImage(
    uint32_t *currentSwapchainImage, size_t *frameIndex, std::vector<SyncObject> &sync, std::chrono::system_clock::time_point frameChrono) {
    Fence::waitForFences(device, imageAvailableFences, false, frameIndex);
    if(*frameIndex > imageAvailableFences.size()){
        return VK_TIMEOUT;
    }
    auto newTime = std::chrono::high_resolution_clock::now();
    float frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - frameChrono).count();
    //std::cout << "ms : " << frameTime << std::endl << "fps : " << 1.0 / frameTime << std::endl;
    imageAvailableFences[*frameIndex].resetFence();

    VkResult result = vkAcquireNextImageKHR(
        device->device(), vkbSwapchain.swapchain, std::numeric_limits<uint64_t>::max(),
        imageAvailableSemaphores[*frameIndex].semaphore(),  // must be a not signaled semaphore
        VK_NULL_HANDLE, currentSwapchainImage);
    SemaphoreSubmitInfo s{imageAvailableSemaphores[*frameIndex].semaphore(), VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT, 0};

    sync[*frameIndex].waitToStartRenderingSemaphores.clear();
    sync[*frameIndex].waitToStartPreComputeSemaphores.clear();
    sync[*frameIndex].waitToStartRenderingSemaphores.clear();
    sync[*frameIndex].waitToPrensentSemaphores.clear();

    sync[*frameIndex].waitToStartRenderingSemaphores.push_back(s);
    sync[*frameIndex].imageAvailableFence = imageAvailableFences[*frameIndex].fence();

    return result;
}

VkResult SwapChain::presentFrame(uint32_t *currentSwapchainImage, size_t *frameIndex, SyncObject &sync) {
    auto presentInfo = make<VkPresentInfoKHR>();

    std::vector<VkSemaphore> waitSemaphores(sync.waitToPrensentSemaphores.size());
    for (int i = 0; i < sync.waitToPrensentSemaphores.size(); i++) {
        waitSemaphores[i] = sync.waitToPrensentSemaphores[i].vkSemaphore;
    }

    presentInfo.waitSemaphoreCount = waitSemaphores.size();
    presentInfo.pWaitSemaphores = waitSemaphores.data();

    VkSwapchainKHR swapChains[] = {vkbSwapchain.swapchain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;

    presentInfo.pImageIndices = currentSwapchainImage;

    auto result = vkQueuePresentKHR(presentQueue, &presentInfo);
    sync.waitToPrensentSemaphores.clear();
    int x = 1;
    return result;
}

void SwapChain::createSyncObjects() {
    imageAvailableFences.clear();
    imageAvailableSemaphores.clear();
    imageAvailableFences.reserve(numberOfFrame - 1);
    imageAvailableSemaphores.reserve(numberOfFrame - 1);
    for (int i = 0; i < numberOfFrame - 1; i++) {
        imageAvailableFences.emplace_back(device, true);
        imageAvailableSemaphores.emplace_back(device, VK_SEMAPHORE_TYPE_BINARY);
    }
}
}  // namespace vk_stage