#pragma once


#include "volk.h"
#include <cstdint>
#include <vector>
#include "descriptorSetLayout.hpp"
#include "device.hpp"


namespace vk_stage {
class DescriptorSet {
   public:
    DescriptorSet(Device *device, VkDescriptorSet set, std::shared_ptr<DescriptorSetLayout> layout, uint32_t poolId);
    DescriptorSet(){};
 
    
    //Set descriptor set data
    void writeBuffer(uint32_t binding, VkDescriptorBufferInfo * bufferInfo);
    void writeImage(uint32_t binding, VkDescriptorImageInfo* imageInfo);
    void writeAccelerationStructure(uint32_t binding, VkWriteDescriptorSetAccelerationStructureKHR* acclStructInfo);
    void writeTexelBuffer(uint32_t binding, VkBufferView* bufferView);
    
    void writeBuffers(uint32_t binding, std::vector<VkDescriptorBufferInfo> *buffersInfo);
    void writeImages(uint32_t binding, std::vector<VkDescriptorImageInfo> *imagesInfo);
    void writeTexelBuffers(uint32_t binding, std::vector<VkBufferView> *buffersView);
    
    void writeToDescriptorSet();

    static void bindDescriptorSet(VkCommandBuffer cmdBuffer, std::vector<VkDescriptorSet> descriptorsSet, VkPipelineLayout pipelineLayout, VkPipelineBindPoint bindpoint, uint32_t firstSet = 0);
    uint32_t getPoolId() { return poolId; };
    VkDescriptorSet *getDescriptorSetPtr() { return &descriptorSet; }
    VkDescriptorSet getDescriptorSet() { return descriptorSet; }
    

   private:
    std::vector<VkWriteDescriptorSet> writes;
    std::shared_ptr<DescriptorSetLayout> layout;
    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    uint32_t poolId;
    Device *device = nullptr;
};
}