#pragma once

#include <cstdint>
#include <map>
#include <memory>
#include <unordered_map>
#include <vector>

#include "device.hpp"
#include "volk.h"

namespace vk_stage {
class DescriptorSetLayout {
   public:
    DescriptorSetLayout(
        Device *device, std::map<uint32_t, VkDescriptorSetLayoutBinding> layoutBindings, std::vector<uint32_t> id);
    ~DescriptorSetLayout();
    static std::shared_ptr<DescriptorSetLayout> createDescriptorSetLayout(
        Device *device, std::map<uint32_t, VkDescriptorSetLayoutBinding> layoutBindings, uint32_t setId);

    VkDescriptorSetLayout getDescriptorSetLayout() { return descriptorSetLayout; }
    VkDescriptorSetLayout *getDescriptorSetLayoutPtr() { return &descriptorSetLayout; }
    std::map<uint32_t, VkDescriptorSetLayoutBinding> getLayoutBindings() { return layoutBindings; }
    std::vector<uint32_t> getId() { return id; }
    static std::unordered_map<std::vector<uint32_t>, std::weak_ptr<DescriptorSetLayout>> descriptorSetLayoutCache;
   private:
    Device *device;

    VkDescriptorSetLayout descriptorSetLayout;
   
    std::map<uint32_t, VkDescriptorSetLayoutBinding> layoutBindings;
    std::vector<uint32_t> id;

};
}  // namespace vk_stage