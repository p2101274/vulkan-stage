
#include "descriptorSetLayout.hpp"

#include <iostream>
#include <memory>
#include <vector>
#include "utils.hpp"
#include "structs_vk.hpp"

namespace vk_stage {
std::unordered_map<std::vector<uint32_t>, std::weak_ptr<DescriptorSetLayout>> DescriptorSetLayout::descriptorSetLayoutCache;

DescriptorSetLayout::DescriptorSetLayout(
    Device *device,
    std::map<uint32_t, VkDescriptorSetLayoutBinding> layoutBindings,
    std::vector<uint32_t> id)
    : device(device), id(id), layoutBindings(layoutBindings){
    std::vector<VkDescriptorSetLayoutBinding> bindingsVector;
    int i = 0;

    std::vector<VkDescriptorBindingFlags> binsFlag;

    for (auto &binding : layoutBindings) {
        bindingsVector.push_back(binding.second);
        binsFlag.push_back(VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT_EXT);
        i++;
    }


    auto extendedInfo = make<VkDescriptorSetLayoutBindingFlagsCreateInfo>();

    extendedInfo.bindingCount = binsFlag.size();
    extendedInfo.pBindingFlags = binsFlag.data();

    auto descriptorSetLayoutInfo = make<VkDescriptorSetLayoutCreateInfo>();
    descriptorSetLayoutInfo.bindingCount = layoutBindings.size();
    descriptorSetLayoutInfo.pBindings = bindingsVector.data();
    descriptorSetLayoutInfo.pNext = &extendedInfo;

    if (vkCreateDescriptorSetLayout(device->device(), &descriptorSetLayoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
        throw std::runtime_error("failed to create descriptorSetLayout");
    }
}

DescriptorSetLayout::~DescriptorSetLayout() {
    vkDestroyDescriptorSetLayout(device->device(), descriptorSetLayout, nullptr);
    descriptorSetLayoutCache.erase(id);
}

std::shared_ptr<DescriptorSetLayout> DescriptorSetLayout::createDescriptorSetLayout(
    Device *device, std::map<uint32_t, VkDescriptorSetLayoutBinding> layoutBindings, uint32_t setId) {
    std::shared_ptr<DescriptorSetLayout> returnValue;
    std::vector<uint32_t> id;
    id.push_back(setId);
    for (auto &binding : layoutBindings) {
        id.push_back(binding.second.binding);
        id.push_back(binding.second.descriptorCount);

        id.push_back(binding.second.stageFlags);
        id.push_back(binding.second.descriptorType);
    }
    if (descriptorSetLayoutCache.find(id) == descriptorSetLayoutCache.end()) {
        std::shared_ptr<DescriptorSetLayout> returnValue = std::make_shared<DescriptorSetLayout>(device, layoutBindings, id);
        descriptorSetLayoutCache[id] = returnValue;
        return returnValue;
    }

    return descriptorSetLayoutCache[id].lock();
}

}  // namespace vk_stage