
#include "descriptorSet.hpp"

#include <vulkan/vulkan_core.h>

#include "structs_vk.hpp"

namespace vk_stage {

DescriptorSet::DescriptorSet(Device *device, VkDescriptorSet set, std::shared_ptr<DescriptorSetLayout> layout, uint32_t poolId)
    : device(device), descriptorSet(set), layout(layout), poolId(poolId) {}

void DescriptorSet::writeBuffer(uint32_t binding, VkDescriptorBufferInfo* bufferInfo) {
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];

    auto write = make<VkWriteDescriptorSet>();
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = 1;
    write.pBufferInfo = bufferInfo;
    write.dstSet = descriptorSet;
    writes.push_back(write);
}

void DescriptorSet::writeImage(uint32_t binding, VkDescriptorImageInfo* imageInfo) { 
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];
    
    auto write = make<VkWriteDescriptorSet>();
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = 1;
    write.pImageInfo = imageInfo;
    write.dstSet = descriptorSet;
    writes.push_back(write);

 }

 void DescriptorSet::writeAccelerationStructure(uint32_t binding, VkWriteDescriptorSetAccelerationStructureKHR* acclStructInfo) {
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];
    
    auto write = make<VkWriteDescriptorSet>();
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = 1;
    write.pNext = acclStructInfo;
    write.dstSet = descriptorSet;
    writes.push_back(write);
 }

void DescriptorSet::writeTexelBuffer(uint32_t binding, VkBufferView *bufferView) { 
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];
    
    auto write = make<VkWriteDescriptorSet>();
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = 1;
    write.pTexelBufferView = bufferView;
    write.dstSet = descriptorSet;
    writes.push_back(write);
 }

void DescriptorSet::writeBuffers(uint32_t binding, std::vector<VkDescriptorBufferInfo>* buffersInfo) {
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];


    auto write = make<VkWriteDescriptorSet>();
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = buffersInfo->size();
    write.pBufferInfo = buffersInfo->data();
    write.dstSet = descriptorSet;
    

    writes.push_back(write);
}

void DescriptorSet::writeImages(uint32_t binding, std::vector<VkDescriptorImageInfo> *imagesInfo) {
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];


    VkWriteDescriptorSet write{};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.descriptorCount = imagesInfo->size();
    write.pImageInfo = imagesInfo->data();
    write.dstSet = descriptorSet;
    writes.push_back(write);
}

void DescriptorSet::writeTexelBuffers(uint32_t binding, std::vector<VkBufferView> *buffersView) {
    assert(layout->getLayoutBindings().count(binding) == 1 && "Layout does not contain specified binding");

    auto bindingDescription = layout->getLayoutBindings()[binding];


    VkWriteDescriptorSet write{};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.descriptorType = bindingDescription.descriptorType;
    write.dstBinding = binding;
    write.dstSet = descriptorSet;
    write.descriptorCount = buffersView->size();
    write.pTexelBufferView = buffersView->data();

    writes.push_back(write);
}

void DescriptorSet::writeToDescriptorSet() {
    vkUpdateDescriptorSets(device->device(), writes.size(), writes.data(), 0, nullptr);
    writes.clear();
}


void DescriptorSet::bindDescriptorSet(
    VkCommandBuffer cmdBuffer,
    std::vector<VkDescriptorSet> descriptorsSet,
    VkPipelineLayout pipelineLayout,
    VkPipelineBindPoint bindpoint,
    uint32_t firstSet) {
    vkCmdBindDescriptorSets(cmdBuffer, bindpoint, pipelineLayout, firstSet, descriptorsSet.size(), descriptorsSet.data(), 0, nullptr);
}

}  // namespace vk_stage