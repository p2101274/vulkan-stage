
#include "descriptorPool.hpp"

#include <vulkan/vulkan_core.h>

#include <cstdint>
#include <iostream>
#include <memory>
#include <stdexcept>

#include "structs_vk.hpp"

namespace vk_stage {

DescriptorPool::DescriptorPool(Device *device, uint32_t basePoolSize, std::vector<PoolSizeRatio> poolRatios)
    : device(device), poolRatios(poolRatios) {
    VkDescriptorPool newPool = create_pool(basePoolSize);
    setsPerPool = basePoolSize * 1.5;

    readyPools[0] = newPool;
    numberOfPool++;
}

DescriptorSet DescriptorPool::allocateDescriptorSet(std::shared_ptr<DescriptorSetLayout> descriptorSetLayout) {
    uint32_t poolId = get_poolId();
    VkDescriptorPool poolToUse = readyPools[poolId];

    auto allocInfo = make<VkDescriptorSetAllocateInfo>();
    ;
    allocInfo.descriptorPool = poolToUse;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = descriptorSetLayout->getDescriptorSetLayoutPtr();

    VkDescriptorSet ds;
    VkResult result = vkAllocateDescriptorSets(device->device(), &allocInfo, &ds);

    // allocation failed. Try again
    if (result == VK_ERROR_OUT_OF_POOL_MEMORY || result == VK_ERROR_FRAGMENTED_POOL) {
        fullPools[poolId] = poolToUse;
        readyPools.erase(poolId);
        poolId = get_poolId();
        poolToUse = readyPools[poolId];
        allocInfo.descriptorPool = poolToUse;
        readyPools[poolId] = poolToUse;
        if (vkAllocateDescriptorSets(device->device(), &allocInfo, &ds) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate descriptor set");
        }
    }

    return DescriptorSet(device, ds, descriptorSetLayout, poolId);
}

void DescriptorPool::freeDescriptorSet(uint32_t poolId, DescriptorSet descriptorSet) {
    VkDescriptorPool pool;
    if (readyPools.find(descriptorSet.getPoolId()) != readyPools.end()) {
        pool = readyPools[descriptorSet.getPoolId()];
    } else {
        pool = fullPools[descriptorSet.getPoolId()];
        readyPools[descriptorSet.getPoolId()] = pool;
        fullPools.erase(descriptorSet.getPoolId());
    }

    if (vkFreeDescriptorSets(device->device(), pool, 1, descriptorSet.getDescriptorSetPtr()) != VK_SUCCESS) {
        throw std::runtime_error("failed to free descriptorSet");
    }
}

void DescriptorPool::clearPools() {
    for (auto p : readyPools) {
        vkResetDescriptorPool(device->device(), p.second, 0);
    }
    for (auto p : fullPools) {
        vkResetDescriptorPool(device->device(), p.second, 0);
        readyPools[p.first] = p.second;
    }
    fullPools.clear();
}

void DescriptorPool::destroy() {
    for (auto p : readyPools) {
        vkDestroyDescriptorPool(device->device(), p.second, nullptr);
    }
    readyPools.clear();
    for (auto p : fullPools) {
        vkDestroyDescriptorPool(device->device(), p.second, nullptr);
    }
    fullPools.clear();
}

uint32_t DescriptorPool::get_poolId() {
    uint32_t pool_id;
    VkDescriptorPool newPool;
    if (readyPools.size() != 0) {
        pool_id = readyPools.begin()->first;
    } else {
        // need to create a new pool
        newPool = create_pool(setsPerPool);

        setsPerPool = setsPerPool * 1.5;
        if (setsPerPool > 4096) {
            setsPerPool = 4096;
        }
        readyPools[numberOfPool] = newPool;
        numberOfPool++;
    }
    return pool_id;
}

VkDescriptorPool DescriptorPool::create_pool(uint32_t setCount) {
    std::vector<VkDescriptorPoolSize> poolSizes;
    for (PoolSizeRatio ratio : poolRatios) {
        poolSizes.push_back(VkDescriptorPoolSize{.type = ratio.type, .descriptorCount = uint32_t(ratio.ratio * setCount)});
    }

    VkDescriptorPoolCreateInfo pool_info = {};
    pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    pool_info.maxSets = setCount;
    pool_info.poolSizeCount = (uint32_t)poolSizes.size();
    pool_info.pPoolSizes = poolSizes.data();

    VkDescriptorPool newPool;
    vkCreateDescriptorPool(device->device(), &pool_info, nullptr, &newPool);
    return newPool;
}

}  // namespace vk_stage