#pragma once
#include <cstdint>
#include <map>
#include <vector>

#include "descriptorSetLayout.hpp"
#include "descriptorSet.hpp"
#include "device.hpp"
#include "volk.h"
namespace vk_stage {
struct PoolSizeRatio {
        VkDescriptorType type;
        float ratio;
    };
class DescriptorPool {
   public:
    

    DescriptorPool(Device *device, uint32_t basePoolSize, std::vector<PoolSizeRatio> poolRatios);

    DescriptorSet allocateDescriptorSet(std::shared_ptr<DescriptorSetLayout> descriptorSetLayout);
    void freeDescriptorSet(uint32_t poolId, DescriptorSet descriptorSet);
    void clearPools();
    void destroy();

   private:
    uint32_t numberOfPool{0};
    uint32_t get_poolId();
    VkDescriptorPool create_pool(uint32_t setCount);

    uint32_t setsPerPool;
    std::vector<PoolSizeRatio> poolRatios;
    std::map<uint32_t, VkDescriptorPool> fullPools;
    std::map<uint32_t, VkDescriptorPool> readyPools;
    Device *device;
};

}  // namespace vk_stage