#include "movement_controller.hpp"

#include <GLFW/glfw3.h>

// std
#include <glm/gtc/constants.hpp>
#include <iostream>
#include <limits>

#include "scene/light.hpp"
#include "scene/scene.hpp"

namespace vk_stage {

MovementController::MovementController(Window& windowObj) : windowObj(windowObj) {
    glfwSetCursorPosCallback(windowObj.getGLFWwindow(), MovementController::mouse_callback);
}

void MovementController::moveInPlaneXZ(GLFWwindow* window, float dt, Camera& cam) {
    glm::vec3 rotate{0};

    if (glfwGetKey(window, keys.lookRight) == GLFW_PRESS) rotate.y -= 1.f;
    if (glfwGetKey(window, keys.lookLeft) == GLFW_PRESS) rotate.y += 1.f;
    if (glfwGetKey(window, keys.lookUp) == GLFW_PRESS) rotate.x += 1.f;
    if (glfwGetKey(window, keys.lookDown) == GLFW_PRESS) rotate.x -= 1.f;

    if (glm::dot(rotate, rotate) > std::numeric_limits<float>::epsilon()) {
        cam.viewerObject.transform.rotation += lookSpeed * dt * glm::normalize(rotate);
    }
    cam.viewerObject.transform.rotation += windowObj.mouseDirection * dt;
    windowObj.mouseDirection *= 0.1;
    
    // limit pitch values between about +/- 85ish degrees
    cam.viewerObject.transform.rotation.x = glm::clamp(cam.viewerObject.transform.rotation.x, -1.5f, 1.5f);
    cam.viewerObject.transform.rotation.y = glm::mod(cam.viewerObject.transform.rotation.y, glm::two_pi<float>());

    float yaw = cam.viewerObject.transform.rotation.y;
    const glm::vec3 forwardDir{sin(yaw), 0.f, cos(yaw)};
    const glm::vec3 rightDir{forwardDir.z, 0.f, -forwardDir.x};
    const glm::vec3 upDir{0.f, 1.f, 0.f};

    glm::vec3 moveDir{0.f};
    if (glfwGetKey(window, keys.moveForward) == GLFW_PRESS) moveDir -= forwardDir;
    if (glfwGetKey(window, keys.moveBackward) == GLFW_PRESS) moveDir += forwardDir;
    if (glfwGetKey(window, keys.moveRight) == GLFW_PRESS) moveDir += rightDir;
    if (glfwGetKey(window, keys.moveLeft) == GLFW_PRESS) moveDir -= rightDir;
    if (glfwGetKey(window, keys.moveUp) == GLFW_PRESS) moveDir += upDir;
    if (glfwGetKey(window, keys.moveDown) == GLFW_PRESS) moveDir -= upDir;

    if (glm::dot(moveDir, moveDir) > std::numeric_limits<float>::epsilon()) {
        cam.viewerObject.transform.translation += moveSpeed * dt * glm::normalize(moveDir);
    }
}

void MovementController::addpointLight(GLFWwindow* window, float dt, Scene& s) {
    if (glfwGetKey(window, keys.space) == GLFW_PRESS && !waspressed) {
        glm::vec3 pos{s.cameras[0].getPosition()};
        glm::vec3 color{
            static_cast<float>(rand()) / static_cast<float>(RAND_MAX), static_cast<float>(rand()) / static_cast<float>(RAND_MAX),
            static_cast<float>(rand()) / static_cast<float>(RAND_MAX)};
        s.lights.push_back({pos, color, static_cast<float>(0.5+(static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/20))))});
        
        waspressed = true;
    }
    else if (glfwGetKey(window, keys.space) != GLFW_PRESS) {
        
        waspressed = false;
    }
}

void MovementController::mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    auto windowObj = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) {
        windowObj->mouseDirection = {0, 0, 0};
        windowObj->mousePosx = xpos;
        windowObj->mousePosy = ypos;
        return;
    }
    double xOffset = xpos - windowObj->mousePosx;
    double yOffset = windowObj->mousePosy - ypos;
    double sensitivity = 1.5f;
    xOffset *= sensitivity;
    yOffset *= sensitivity;
    windowObj->mousePosx = xpos;
    windowObj->mousePosy = ypos;
    windowObj->mouseDirection = {-yOffset, xOffset, 0};
}

}  // namespace vk_stage