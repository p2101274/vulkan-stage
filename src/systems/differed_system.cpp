
#include "differed_system.hpp"

#include <vulkan/vulkan_core.h>

#include <glm/ext/vector_float4.hpp>
#include <vector>

#include "app.hpp"
#include "descriptor/descriptorPool.hpp"
#include "renderer.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
#include "structs_vk.hpp"

namespace vk_stage {

DifferedSystem::DifferedSystem(Device* device, Scene* scene, DynamicRenderPass* renderpass)
    : device(device),
      scene(scene),
      renderpass(renderpass),
      descpool(
          device,
          20,
          {{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0.2},
           {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0.7},
           {(device->RTenabled) ? VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR : VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0.1}}) {
    GraphicPipelineCreateInfo c;

    if (device->RTenabled) {
        c.fragmentShaderFile = "differed_rendering_rq.frag";
        c.vexterShaderFile = "differed_rendering_rq.vert";
    } else {
        c.fragmentShaderFile = "differed_rendering.frag";
        c.vexterShaderFile = "differed_rendering.vert";
    }

    differedPipeline = new GraphicPipeline(device, c);

    if (device->RTenabled) {
        c.fragmentShaderFile = "differed_rendering_point_rq.frag";
        c.vexterShaderFile = "differed_rendering_point_rq.vert";
    } else {
        c.fragmentShaderFile = "differed_rendering_point.frag";
        c.vexterShaderFile = "differed_rendering_point.vert";
    }
    differedPointPipeline = new GraphicPipeline(device, c);
    imageSize = renderpass->getFrameSize();
    createDescriptorSet();
}

void DifferedSystem::destroy() {
    differedPipeline->destroy();
    differedPointPipeline->destroy();
    delete differedPipeline;
    delete differedPointPipeline;
    descpool.destroy();
}

void DifferedSystem::renderLight(VkCommandBuffer cmdBuffer, size_t frameIndex, float time) {
    if (imageSize.width != renderpass->getFrameSize().width || imageSize.height != renderpass->getFrameSize().height) {
        imageSize = renderpass->getFrameSize();
        for (auto& d : descriptorSet) {
            descpool.clearPools();
            descriptorSet.clear();
        }
        createDescriptorSet();
    }

    // differedPipeline->bindPipeline(cmdBuffer);
    // if (device->RTenabled) {
    //     DescriptorSet::bindDescriptorSet(
    //         cmdBuffer, {descriptorSet[frameIndex].getDescriptorSet(), descriptorSet.back().getDescriptorSet()},
    //         differedPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_GRAPHICS);
    // } else {
    //     DescriptorSet::bindDescriptorSet(
    //         cmdBuffer, {descriptorSet[frameIndex].getDescriptorSet()}, differedPipeline->getPipelineLayout(),
    //         VK_PIPELINE_BIND_POINT_GRAPHICS);
    // }

    // vkCmdPushConstants(
    //     cmdBuffer, differedPipeline->getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, 4, &time);
    // vkCmdDraw(cmdBuffer, 3, 1, 0, 0);

    differedPointPipeline->bindPipeline(cmdBuffer);
    if (device->RTenabled) {
        DescriptorSet::bindDescriptorSet(
            cmdBuffer, {descriptorSet[frameIndex].getDescriptorSet(), descriptorSet.back().getDescriptorSet()},
            differedPointPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_GRAPHICS);
    } else {
        DescriptorSet::bindDescriptorSet(
            cmdBuffer, {descriptorSet[frameIndex].getDescriptorSet()}, differedPointPipeline->getPipelineLayout(),
            VK_PIPELINE_BIND_POINT_GRAPHICS);
    }
    for (auto& light : scene->lights) {
        std::vector<glm::vec4> lightInfo = {{light.pos, 1.0}, {light.color, light.intensity}};
        vkCmdPushConstants(
            cmdBuffer, differedPointPipeline->getPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0,
            sizeof(glm::vec4) * 2, lightInfo.data());
        vkCmdDraw(cmdBuffer, 3, 1, 0, 0);
    }
}

void DifferedSystem::createDescriptorSet() {
    auto attachements = renderpass->getimageAttachement();
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        descriptorSet.push_back(descpool.allocateDescriptorSet(differedPipeline->getDescriptorSetLayout(0)));
        VkDescriptorBufferInfo cam = scene->uboBuffers[i][0].descriptorInfo();
        VkDescriptorImageInfo albedo = attachements[i][0].descriptorInfo();
        albedo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        albedo.sampler = attachements[i][0].getSampler();
        VkDescriptorImageInfo normal = attachements[i][1].descriptorInfo();
        normal.sampler = attachements[i][1].getSampler();
        normal.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        VkDescriptorImageInfo world = attachements[i][2].descriptorInfo();
        world.sampler = attachements[i][2].getSampler();
        world.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        VkDescriptorImageInfo MR = attachements[i][3].descriptorInfo();
        MR.sampler = attachements[i][3].getSampler();
        MR.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        descriptorSet[i].writeBuffer(0, &cam);
        descriptorSet[i].writeImage(1, &albedo);
        descriptorSet[i].writeImage(2, &normal);
        descriptorSet[i].writeImage(3, &world);
        descriptorSet[i].writeImage(4, &MR);
        descriptorSet[i].writeToDescriptorSet();
    }

    if (device->RTenabled) {
        descriptorSet.push_back(descpool.allocateDescriptorSet(differedPipeline->getDescriptorSetLayout(1)));
        auto descriptorAccelerationStructureInfo = make<VkWriteDescriptorSetAccelerationStructureKHR>();
        descriptorAccelerationStructureInfo.accelerationStructureCount = 1;
        descriptorAccelerationStructureInfo.pAccelerationStructures = &scene->accelerationStructureHandle;
        descriptorSet.back().writeAccelerationStructure(0, &descriptorAccelerationStructureInfo);
        descriptorSet.back().writeToDescriptorSet();
    }
}

}  // namespace vk_stage