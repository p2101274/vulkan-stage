#pragma once


#include <vector>
#include "descriptor/descriptorPool.hpp"
#include "descriptor/descriptorSet.hpp"
#include "ressources/buffer.hpp"
#include "scene/scene.hpp"
namespace vk_stage {
class DrawSystem {
   public:
    DrawSystem(Device* devic,Scene* scene, std::vector<std::shared_ptr<Buffer>> drawCommandBuffer);
    void drawScene(VkCommandBuffer cmdBuffer, size_t frameIndex, VkPipelineLayout pipelineLayout);
    
   private:
    std::vector<std::shared_ptr<Buffer>> drawCommandBuffer;

    Scene* scene;
    Device* device;
};
}  // namespace vk_stage
