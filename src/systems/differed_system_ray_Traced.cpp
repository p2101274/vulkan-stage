
#include "differed_system_ray_Traced.hpp"

#include <vulkan/vulkan_core.h>

#include <cstdint>
#include <glm/ext/vector_float4.hpp>
#include <glm/fwd.hpp>
#include <iostream>
#include <thread>
#include <vector>

#include "app.hpp"
#include "descriptor/descriptorPool.hpp"
#include "renderer.hpp"
#include "ressources/image.hpp"
#include "shader/pipeline/compute_pipeline.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
#include "structs_vk.hpp"

namespace vk_stage {

DifferedRayTracingSystem::DifferedRayTracingSystem(
    Device* device, Scene* scene, DynamicRenderPass* renderpass, std::vector<Image>& swapChainImages)
    : device(device),
      scene(scene),
      renderpass(renderpass),
      swapChainImages(swapChainImages),
      descpool(
          device,
          3000,
          {{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0.1},
           {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 0.1},
           {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0.7},
           {VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR, 0.1}}) {
    std::vector<std::string> shaderList;
    std::vector<SBTLine> SBT_conf;
    shaderList.push_back("defaultRayGen.rgen");
    shaderList.push_back("defaultMiss.rmiss");
    shaderList.push_back("defaultClosestHit.rchit");
    shaderList.push_back("defaultAnyHit.rahit");
    shaderList.push_back("customShapeClosestHit.rchit");
    shaderList.push_back("sphereIntersect.rint");
    shaderList.push_back("boxIntersect.rint");

    SBT_conf.push_back({0, VK_SHADER_UNUSED_KHR, VK_SHADER_UNUSED_KHR, VK_SHADER_UNUSED_KHR});
    SBT_conf.push_back({1, VK_SHADER_UNUSED_KHR, VK_SHADER_UNUSED_KHR, VK_SHADER_UNUSED_KHR});
    SBT_conf.push_back({VK_SHADER_UNUSED_KHR, 2, 3, VK_SHADER_UNUSED_KHR});
    SBT_conf.push_back({VK_SHADER_UNUSED_KHR, 4, VK_SHADER_UNUSED_KHR, 5});
    SBT_conf.push_back({VK_SHADER_UNUSED_KHR, 4, VK_SHADER_UNUSED_KHR, 6});

    rtDifferedPipeline = new RayTracingPipeline(device, shaderList, SBT_conf);
    imageSize = renderpass->getFrameSize();

    horFilterPipeline = new ComputePipeline(device, "denoiseHor.comp");
    vertFilterPipeline = new ComputePipeline(device, "denoiseVert.comp");
    createDescriptorSet();
    genSceneDescriptorSet();
}

void DifferedRayTracingSystem::renderLight(
    VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapChainIndex, float time, bool camMouv) {
    if (camMouv) numberOfframe = {0, 0, 0};
    if (imageSize.width != renderpass->getFrameSize().width || imageSize.height != renderpass->getFrameSize().height) {
        imageSize = renderpass->getFrameSize();
        for (auto& img : renderImages) {
            img.destroy();
        }
        renderImages.clear();
        for (auto& d : descriptorSet) {
            descpool.clearPools();
            descriptorSet.clear();
        }
        createDescriptorSet();
    }
    VkImageLayout baseSCLayout = swapChainImages[swapChainIndex].getActualImageLayout();
    // swapChainImages[swapChainIndex].transitionImageLayout(baseSCLayout, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);

    rtDifferedPipeline->bindPipeline(cmdBuffer);

    DescriptorSet::bindDescriptorSet(
        cmdBuffer,
        {descriptorSet[frameIndex].getDescriptorSet(), descriptorSet[MAX_FRAMES_IN_FLIGHT + frameIndex].getDescriptorSet(),
         descriptorSet.back().getDescriptorSet(), sceneDescSet.getDescriptorSet()},
        rtDifferedPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR);

    const uint32_t handleSizeAligned = rtDifferedPipeline->handleSizeAligned;

    VkStridedDeviceAddressRegionKHR raygenShaderSbtEntry{};
    raygenShaderSbtEntry.deviceAddress = rtDifferedPipeline->rayGenBuffer.getBufferDeviceAddress();
    raygenShaderSbtEntry.stride = handleSizeAligned;
    raygenShaderSbtEntry.size = handleSizeAligned;

    VkStridedDeviceAddressRegionKHR missShaderSbtEntry{};
    missShaderSbtEntry.deviceAddress = rtDifferedPipeline->missBuffer.getBufferDeviceAddress();
    missShaderSbtEntry.stride = handleSizeAligned;
    missShaderSbtEntry.size = handleSizeAligned;

    VkStridedDeviceAddressRegionKHR hitShaderSbtEntry{};
    hitShaderSbtEntry.deviceAddress = rtDifferedPipeline->hitBuffer.getBufferDeviceAddress();
    hitShaderSbtEntry.stride = handleSizeAligned;
    hitShaderSbtEntry.size = handleSizeAligned * 2;

    VkStridedDeviceAddressRegionKHR callableShaderSbtEntry{};
    // callableShaderSbtEntry.deviceAddress = rtDifferedPipeline->callBuffer.getBufferDeviceAddress();
    // callableShaderSbtEntry.stride = handleSizeAligned;
    // callableShaderSbtEntry.size = handleSizeAligned;

    VkShaderStageFlags flags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR | VK_SHADER_STAGE_ANY_HIT_BIT_KHR | VK_SHADER_STAGE_RAYGEN_BIT_KHR |
                               VK_SHADER_STAGE_INTERSECTION_BIT_KHR;
    VkDeviceAddress address[3] = {
        scene->IndexBuffer->getBufferDeviceAddress(), scene->vertexBuffer->getBufferDeviceAddress(),
        scene->instanceBuffer.getBufferDeviceAddress()};
    vkCmdPushConstants(cmdBuffer, rtDifferedPipeline->getPipelineLayout(), flags, 0, sizeof(VkDeviceAddress) * 3, &address);

    vkCmdPushConstants(
        cmdBuffer, rtDifferedPipeline->getPipelineLayout(), flags, sizeof(VkDeviceAddress) * 3, sizeof(uint), &numberOfframe[frameIndex]);

    vkCmdTraceRaysKHR(
        cmdBuffer, &raygenShaderSbtEntry, &missShaderSbtEntry, &hitShaderSbtEntry, &callableShaderSbtEntry, imageSize.width,
        imageSize.height, 1);

    flags = VK_SHADER_STAGE_COMPUTE_BIT;

    // renderImages[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // horFilterPipeline->bindPipeline(cmdBuffer);
    // vkCmdPushConstants(cmdBuffer, horFilterPipeline->getPipelineLayout(), flags, 0, sizeof(glm::vec2), &imageSize);
    // DescriptorSet::bindDescriptorSet(
    //     cmdBuffer, {/*descriptorSet[frameIndex].getDescriptorSet(),*/ renderToDenoiseDescriptorSet[frameIndex].getDescriptorSet()},
    //     horFilterPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_COMPUTE);
    // vkCmdDispatch(cmdBuffer, (1280 / 32) + 32, (720 / 32) + 32, 1);

    // renderImages[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // denoiseImage[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // vertFilterPipeline->bindPipeline(cmdBuffer);
    // vkCmdPushConstants(cmdBuffer, vertFilterPipeline->getPipelineLayout(), flags, 0, sizeof(glm::vec2), &imageSize);
    // DescriptorSet::bindDescriptorSet(
    //     cmdBuffer, {/*descriptorSet[frameIndex].getDescriptorSet(),*/ denoiseToRenderDescriptorSet[frameIndex].getDescriptorSet()},
    //     vertFilterPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_COMPUTE);
    // vkCmdDispatch(cmdBuffer, (1280 / 32) + 32, (720 / 32) + 32, 1);

    // renderImages[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // denoiseImage[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // horFilterPipeline->bindPipeline(cmdBuffer);
    // vkCmdPushConstants(cmdBuffer, horFilterPipeline->getPipelineLayout(), flags, 0, sizeof(glm::vec2), &imageSize);
    // DescriptorSet::bindDescriptorSet(
    //     cmdBuffer, {/*descriptorSet[frameIndex].getDescriptorSet(),*/ renderToDenoiseDescriptorSet[frameIndex].getDescriptorSet()},
    //     horFilterPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_COMPUTE);
    // vkCmdDispatch(cmdBuffer, (1280 / 32) + 32, (720 / 32) + 32, 1);

    // renderImages[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // denoiseImage[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // vertFilterPipeline->bindPipeline(cmdBuffer);
    // vkCmdPushConstants(cmdBuffer, vertFilterPipeline->getPipelineLayout(), flags, 0, sizeof(glm::vec2), &imageSize);
    // DescriptorSet::bindDescriptorSet(
    //     cmdBuffer, {/*descriptorSet[frameIndex].getDescriptorSet(),*/ denoiseToRenderDescriptorSet[frameIndex].getDescriptorSet()},
    //     vertFilterPipeline->getPipelineLayout(), VK_PIPELINE_BIND_POINT_COMPUTE);
    // vkCmdDispatch(cmdBuffer, (1280 / 32) + 32, (720 / 32) + 32, 1);
    // renderImages[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    // denoiseImage[frameIndex].transitionImageLayout(VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL, cmdBuffer);
    Image::copyImage(device, renderImages[frameIndex], swapChainImages[swapChainIndex], cmdBuffer);
    numberOfframe[frameIndex]++;

    // if(numberOfframe[frameIndex] == 51){
    //     std::cout << "go" << std::endl;
    //     // std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    //     numberOfframe[frameIndex] = 0;
    // }
}

void DifferedRayTracingSystem::createDescriptorSet() {
    auto attachements = renderpass->getimageAttachement();
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        descriptorSet.push_back(descpool.allocateDescriptorSet(rtDifferedPipeline->getDescriptorSetLayout(0)));
        VkDescriptorBufferInfo cam = scene->uboBuffers[i][0].descriptorInfo();
        descriptorSet[i].writeBuffer(0, &cam);
        descriptorSet[i].writeToDescriptorSet();
    }
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        ImageCreateInfo imageCreateInfo{};
        imageCreateInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        imageCreateInfo.height = imageSize.height;
        imageCreateInfo.width = imageSize.width;
        imageCreateInfo.usageFlags = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
        imageCreateInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
        renderImages.push_back(Image(device, imageCreateInfo));
        denoiseImage.push_back(Image(device, imageCreateInfo));

        imageCreateInfo.usageFlags = VK_IMAGE_USAGE_STORAGE_BIT;
        imageCreateInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
        dataImage.push_back(Image(device, imageCreateInfo));

        descriptorSet.push_back(descpool.allocateDescriptorSet(rtDifferedPipeline->getDescriptorSetLayout(1)));
        VkDescriptorImageInfo out = renderImages.back().descriptorInfo();
        out.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        VkDescriptorImageInfo outData = dataImage.back().descriptorInfo();
        outData.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        descriptorSet.back().writeImage(0, &out);
        descriptorSet.back().writeImage(1, &outData);
        descriptorSet.back().writeToDescriptorSet();

        renderToDenoiseDescriptorSet.push_back(descpool.allocateDescriptorSet(horFilterPipeline->getDescriptorsSetLayout()[0]));
        VkDescriptorImageInfo renderImg = renderImages.back().descriptorInfo();
        out.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        VkDescriptorImageInfo denoiseImg = dataImage.back().descriptorInfo();
        outData.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
        renderToDenoiseDescriptorSet.back().writeImage(0, &renderImg);
        renderToDenoiseDescriptorSet.back().writeImage(1, &denoiseImg);
        renderToDenoiseDescriptorSet.back().writeToDescriptorSet();

        denoiseToRenderDescriptorSet.push_back(descpool.allocateDescriptorSet(horFilterPipeline->getDescriptorsSetLayout()[0]));
        denoiseToRenderDescriptorSet.back().writeImage(1, &renderImg);
        denoiseToRenderDescriptorSet.back().writeImage(0, &denoiseImg);
        denoiseToRenderDescriptorSet.back().writeToDescriptorSet();
    }
    descriptorSet.push_back(descpool.allocateDescriptorSet(rtDifferedPipeline->getDescriptorSetLayout(2)));
    auto descriptorAccelerationStructureInfo = make<VkWriteDescriptorSetAccelerationStructureKHR>();
    descriptorAccelerationStructureInfo.accelerationStructureCount = 1;
    descriptorAccelerationStructureInfo.pAccelerationStructures = &scene->accelerationStructureHandle;
    descriptorSet.back().writeAccelerationStructure(0, &descriptorAccelerationStructureInfo);
    descriptorSet.back().writeToDescriptorSet();
}

void DifferedRayTracingSystem::genSceneDescriptorSet() {
    sceneDescSet = descpool.allocateDescriptorSet(rtDifferedPipeline->getDescriptorSetLayout(3));
    auto samplerInfo = make<VkDescriptorImageInfo>();
    samplerInfo.sampler = scene->baseSampler;

    std::vector<VkDescriptorBufferInfo> bufferInfos(scene->materials.size());
    std::vector<VkDescriptorImageInfo> imageInfos(scene->textures.size());
    for (int i = 0; i < scene->materials.size(); i++) {
        bufferInfos[i] = scene->materialBuffer->descriptorInfoForIndex(i);
    }
    for (int i = 0; i < scene->textures.size(); i++) {
        imageInfos[i] = scene->textures[i].descriptorInfo();
    }

    sceneDescSet.writeImage(0, &samplerInfo);
    sceneDescSet.writeBuffers(1, &bufferInfos);
    sceneDescSet.writeImages(2, &imageInfos);
    sceneDescSet.writeToDescriptorSet();
}

}  // namespace vk_stage