
#include "cull_system.hpp"

#include <vulkan/vulkan_core.h>

#include <cstddef>
#include <cstdint>
#include <glm/fwd.hpp>
#include <memory>
#include <vector>

#include "ressources/image.hpp"
#include "shader/pipeline/compute_pipeline.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

#include "descriptor/descriptorSet.hpp"
#include "renderer.hpp"
#include "ressources/buffer.hpp"
#include "shader/shader.hpp"
#include "structs_vk.hpp"
#include "utils.hpp"

namespace vk_stage {

bool checkBlockVisibility(glm::vec3 frustrumBoxPoints[8], glm::vec3 pmin, glm::vec3 pmax, glm::mat4 VPMatrix) {
    int compteur[6] = {0};
    for (int i = 0; i < 8; i++) {
        if (frustrumBoxPoints[i].x < pmin.x) {
            compteur[0]++;
        }
        if (frustrumBoxPoints[i].x > pmax.x) {
            compteur[1]++;
        }
        if (frustrumBoxPoints[i].y < pmin.y) {
            compteur[2]++;
        }
        if (frustrumBoxPoints[i].y > pmax.y) {
            compteur[3]++;
        }
        if (frustrumBoxPoints[i].z < pmin.z) {
            compteur[4]++;
        }
        if (frustrumBoxPoints[i].z > pmax.z) {
            compteur[5]++;
        }
    }
    for (int i = 0; i < 6; i++)
        if (compteur[i] == 8) return false;

    glm::vec4 boxPoint[8] = {
        {pmin.x, pmin.y, pmin.z, 1.0}, {pmin.x, pmin.y, pmax.z, 1.0}, {pmin.x, pmax.y, pmin.z, 1.0}, {pmin.x, pmax.y, pmax.z, 1.0},
        {pmax.x, pmin.y, pmin.z, 1.0}, {pmax.x, pmin.y, pmax.z, 1.0}, {pmax.x, pmax.y, pmin.z, 1.0}, {pmax.x, pmax.y, pmax.z, 1.0},
    };
    for (int i = 0; i < 8; i++) boxPoint[i] = VPMatrix * boxPoint[i];

    int n[6] = {0};
    for (int i = 0; i < 8; i++) {
        if (boxPoint[i].x < -boxPoint[i].w) n[0]++;  // trop a gauche
        if (boxPoint[i].x > boxPoint[i].w) n[1]++;   // a droite

        if (boxPoint[i].y < -boxPoint[i].w) n[2]++;  // en bas
        if (boxPoint[i].y > boxPoint[i].w) n[3]++;   // en haut

        if (boxPoint[i].z < -boxPoint[i].w) n[4]++;  // derriere
        if (boxPoint[i].z > boxPoint[i].w) n[5]++;   // devant
    }
    for (int i = 0; i < 6; i++)
        if (n[i] == 8) return false;

    return true;
}

void matrixTOPminAndPmax(glm::vec3 &pmin, glm::vec3 &pmax, glm::mat4 matrix) {
    glm::vec3 BoxPoints[8];
    BoxPoints[0] = glm::vec3(matrix * glm::vec4(pmin.x, pmin.y, pmin.z, 1));
    BoxPoints[1] = glm::vec3(matrix * glm::vec4(pmin.x, pmin.y, pmax.z, 1));
    BoxPoints[2] = glm::vec3(matrix * glm::vec4(pmin.x, pmax.y, pmin.z, 1));
    BoxPoints[3] = glm::vec3(matrix * glm::vec4(pmin.x, pmax.y, pmax.z, 1));
    BoxPoints[4] = glm::vec3(matrix * glm::vec4(pmax.x, pmin.y, pmin.z, 1));
    BoxPoints[5] = glm::vec3(matrix * glm::vec4(pmax.x, pmin.y, pmax.z, 1));
    BoxPoints[6] = glm::vec3(matrix * glm::vec4(pmax.x, pmax.y, pmin.z, 1));
    BoxPoints[7] = glm::vec3(matrix * glm::vec4(pmax.x, pmax.y, pmax.z, 1));
    pmin = {FLT_MAX, FLT_MAX, FLT_MAX};
    pmax = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
    for (int i = 1; i < 8; i++) {
        pmin = min(pmin, BoxPoints[i]);
        pmax = max(pmax, BoxPoints[i]);
    }
}

CullSystem::CullSystem(Device *device, Scene *scene, DynamicRenderPass *renderpass, VkExtent2D frameSize)
    : device(device),
      scene(scene),
      renderpass(renderpass),
      frameSize(frameSize),
      cullDescriptorPool(
          device,
          150,
          {{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0.05},
           {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 0.25},
           {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0.4},
           {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 0.3}}) {
    drawCommandBuffer.resize(MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < drawCommandBuffer.size(); i++) {
        drawCommandBuffer[i] = std::make_shared<Buffer>(
            device, 1000000 * sizeof(VkDrawIndexedIndirectCommand) + 16, 1,
            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
        drawCommandBuffer[i]->map();
    }

    createShader();
    createSampler();

    createDepthInfo(frameSize);

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        cullDescriptorSet.push_back(cullDescriptorPool.allocateDescriptorSet(frustrumShader->getDescriptorsSetLayout()[0]));
        VkDescriptorBufferInfo uboDescriptor = scene->uboBuffers[i][0].descriptorInfo();
        VkDescriptorBufferInfo blockDesciptor = scene->meshBlockBuffer.descriptorInfo();
        VkDescriptorBufferInfo drawCmd = drawCommandBuffer[i]->descriptorInfo(drawCommandBuffer[i]->getBufferSize() - 16, 16);
        VkDescriptorBufferInfo drawCmdCount = drawCommandBuffer[i]->descriptorInfo(16, 0);
        VkDescriptorBufferInfo instance = scene->instanceBuffer.descriptorInfo();
        VkDescriptorImageInfo depthInfo = cullDepthImages[i].descriptorInfo();
        depthInfo.sampler = depthSampler;
        cullDescriptorSet[i].writeBuffer(0, &uboDescriptor);
        cullDescriptorSet[i].writeBuffer(1, &blockDesciptor);
        cullDescriptorSet[i].writeBuffer(2, &drawCmd);
        cullDescriptorSet[i].writeBuffer(3, &drawCmdCount);
        cullDescriptorSet[i].writeBuffer(4, &instance);
        cullDescriptorSet[i].writeImage(5, &depthInfo);
        cullDescriptorSet[i].writeToDescriptorSet();
    }
}
void CullSystem::destroy() {
    for (auto &db : drawCommandBuffer) {
        db->destroy();
    }
    for (auto &depthimg : cullDepthImages) {
        depthimg.destroy();
    }
    for (auto &mipmapViewList : mipmapViews) {
        for (auto &mipmapView : mipmapViewList) {
            vkDestroyImageView(device->device(), mipmapView, nullptr);
        }
    }
    frustrumShader->destroy();
    mipmapShader->destroy();
    occlusionShader->destroy();
    delete frustrumShader;
    delete mipmapShader;
    delete occlusionShader;
    cullDescriptorPool.destroy();
    vkDestroySampler(device->device(), maxDepthSampler, nullptr);
    vkDestroySampler(device->device(), depthSampler, nullptr);
}

void CullSystem::createShader() {
    frustrumShader = new ComputePipeline(device, "frustrum_cull.comp");
    occlusionShader = new ComputePipeline(device, "occlusion_cull.comp");
    mipmapShader = new ComputePipeline(device, "mipmap.comp");
}

void CullSystem::createSampler() {
    auto samplerInfo = make<VkSamplerCreateInfo>();
    samplerInfo.magFilter = VK_FILTER_LINEAR;  // VK_FILTER_LINEAR
    samplerInfo.minFilter = VK_FILTER_LINEAR;  // VK_FILTER_LINEAR
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.compareOp = VK_COMPARE_OP_NEVER;  // VK_COMPARE_OP_NEVER
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 16;                                     // VK_FALSE
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;  // VK_BORDER_COLOR_INT_OPAQUE_BLACK
                                                                 // add a extension struct to enable Min mode
    VkSamplerReductionModeCreateInfoEXT createInfoReduction = {};

    createInfoReduction.sType = VK_STRUCTURE_TYPE_SAMPLER_REDUCTION_MODE_CREATE_INFO_EXT;
    createInfoReduction.reductionMode = VK_SAMPLER_REDUCTION_MODE_MAX;
    samplerInfo.pNext = &createInfoReduction;

    vkCreateSampler(device->device(), &samplerInfo, nullptr, &maxDepthSampler);

    samplerInfo = make<VkSamplerCreateInfo>();
    samplerInfo.magFilter = VK_FILTER_NEAREST;  // VK_FILTER_LINEAR
    samplerInfo.minFilter = VK_FILTER_NEAREST;  // VK_FILTER_LINEAR
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;  // VK_SAMPLER_ADDRESS_MODE_REPEAT
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.compareOp = VK_COMPARE_OP_NEVER;  // VK_COMPARE_OP_NEVER
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 16;                                     // VK_FALSE
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;  // VK_BORDER_COLOR_INT_OPAQUE_BLACK
    vkCreateSampler(device->device(), &samplerInfo, nullptr, &depthSampler);
}

void CullSystem::createDepthInfo(VkExtent2D frameSize) {
    ImageCreateInfo imageCreateInfo{};
    std::vector<Image> depthImages = renderpass->getDepthAndStencilAttachement();
    imageCreateInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
    imageCreateInfo.enableMipMap = true;
    imageCreateInfo.format = VK_FORMAT_R32_SFLOAT;
    imageCreateInfo.height = frameSize.height;
    imageCreateInfo.width = frameSize.width;
    imageCreateInfo.usageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
    cullDepthImages.reserve(MAX_FRAMES_IN_FLIGHT);
    mipmapViews.resize(MAX_FRAMES_IN_FLIGHT);
    // mipmapdescriptor.resize(depthImages.size());

    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        cullDepthImages.emplace_back(device, imageCreateInfo);
        for (int j = 0; j < cullDepthImages[i].getMipmapLvl(); j++) {
            auto imageViewInfo = make<VkImageViewCreateInfo>();
            imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;  // VK_IMAGE_VIEW_TYPE_2D
            imageViewInfo.format = imageCreateInfo.format;
            imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            imageViewInfo.subresourceRange.baseMipLevel = j;
            imageViewInfo.subresourceRange.baseArrayLayer = 0;
            imageViewInfo.subresourceRange.layerCount = imageCreateInfo.layers;
            imageViewInfo.subresourceRange.levelCount = 1;
            imageViewInfo.image = cullDepthImages[i].getImage();

            VkImageView imageView;
            vkCreateImageView(device->device(), &imageViewInfo, nullptr, &imageView);
            mipmapViews[i].push_back(imageView);
            auto imageIn = make<VkDescriptorImageInfo>();
            auto imageOut = make<VkDescriptorImageInfo>();
            if (j == 0) {
                imageIn.imageView = depthImages[i].getImageView();
            } else {
                imageIn.imageView = mipmapViews[i][j - 1];
            }
            imageIn.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            imageIn.sampler = maxDepthSampler;
            imageOut.imageView = mipmapViews[i][j];
            imageOut.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

            for (uint k = 0; k < depthImages.size(); k++) {
                mipmapdescriptor[{i, k}].push_back(cullDescriptorPool.allocateDescriptorSet(mipmapShader->getDescriptorsSetLayout()[0]));
                mipmapdescriptor[{i, k}][j].writeImage(0, &imageIn);
                mipmapdescriptor[{i, k}][j].writeImage(1, &imageOut);
                mipmapdescriptor[{i, k}][j].writeToDescriptorSet();
            }
        }
    }
}

void CullSystem::resize(VkExtent2D frameSize) {
    this->frameSize = frameSize;
    for (int i = 0; i < cullDepthImages.size(); i++) {
        for (auto &view : mipmapViews[i]) {
            vkDestroyImageView(device->device(), view, nullptr);
        }
        cullDepthImages[i].destroy();
    }
    createDepthInfo(frameSize);
}

void CullSystem::cpuCull(size_t frameIndex) {
    glm::mat4 IVPmatrix = glm::inverse(scene->cameras[0].getProjection() * scene->cameras[0].getView());
    glm::mat4 VPmatrix = scene->cameras[0].getProjection() * scene->cameras[0].getView();
    glm::vec4 frustrumBoxPointW[8];
    frustrumBoxPointW[0] = (IVPmatrix * glm::vec4(1, 1, 1, 1));
    frustrumBoxPointW[1] = (IVPmatrix * glm::vec4(1, 1, -1, 1));
    frustrumBoxPointW[2] = (IVPmatrix * glm::vec4(1, -1, 1, 1));
    frustrumBoxPointW[3] = (IVPmatrix * glm::vec4(1, -1, -1, 1));
    frustrumBoxPointW[4] = (IVPmatrix * glm::vec4(-1, 1, 1, 1));
    frustrumBoxPointW[5] = (IVPmatrix * glm::vec4(-1, 1, -1, 1));
    frustrumBoxPointW[6] = (IVPmatrix * glm::vec4(-1, -1, 1, 1));
    frustrumBoxPointW[7] = (IVPmatrix * glm::vec4(-1, -1, -1, 1));
    glm::vec3 frustrumBoxPoint[8];
    for (int i = 0; i < 8; i++) {
        frustrumBoxPoint[i] = glm::vec3(frustrumBoxPointW[i].x, frustrumBoxPointW[i].y, frustrumBoxPointW[i].z) / frustrumBoxPointW[i].w;
    }
    drawCommandList.vkCmd.clear();
    drawCommandList.drawCount = 0;
    for (auto &meshBlock : scene->meshBlockList) {
        TransformComponent t = scene->objects[meshBlock.instancesID].transform;
        glm::vec3 pmin = meshBlock.pmin;
        glm::vec3 pmax = meshBlock.pmax;
        matrixTOPminAndPmax(pmin, pmax, t.mat4());
        if (checkBlockVisibility(frustrumBoxPoint, pmin, pmax, VPmatrix)) {
            glm::mat4 oeoeoe = t.mat4();
            drawCommandList.vkCmd.push_back(VkDrawIndexedIndirectCommand{
                meshBlock.indexSize, 1, meshBlock.indexOffset, meshBlock.vertexOffset, uint32_t(meshBlock.instancesID)});
            drawCommandList.drawCount++;
        }
    }

    drawCommandBuffer[frameIndex]->writeToBuffer(&drawCommandList.drawCount, 4, 0);
    drawCommandBuffer[frameIndex]->writeToBuffer(
        drawCommandList.vkCmd.data(), drawCommandList.vkCmd.size() * sizeof(VkDrawIndexedIndirectCommand), 16);
    drawCommandBuffer[frameIndex]->flush(
        drawCommandList.vkCmd.size() * sizeof(VkDrawIndexedIndirectCommand) + 16 +
        (64 - (drawCommandList.vkCmd.size() * sizeof(VkDrawIndexedIndirectCommand) + 16) % 64));
}

void CullSystem::frustrumCull(VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapchainImageIndex) {
    uint32_t drawCount[4] = {0};

    drawCommandBuffer[frameIndex]->writeToBuffer(&drawCount, 16, 0);
    drawCommandBuffer[frameIndex]->flush(64, 0);

    frustrumShader->bindPipeline(cmdBuffer);
    uint32_t size = uint32_t(scene->meshBlockList.size());
    uint32_t firstDraw = 1;
    vkCmdPushConstants(cmdBuffer, frustrumShader->getPipelineLayout(), VK_SHADER_STAGE_COMPUTE_BIT, 0, 4, &size);
    vkCmdPushConstants(cmdBuffer, frustrumShader->getPipelineLayout(), VK_SHADER_STAGE_COMPUTE_BIT, 4, 4, &firstDraw);

    DescriptorSet::bindDescriptorSet(
        cmdBuffer, {cullDescriptorSet[frameIndex].getDescriptorSet()}, frustrumShader->getPipelineLayout(), VK_PIPELINE_BIND_POINT_COMPUTE);

    uint32_t groupCount = (size + 255) / 256;

    vkCmdDispatch(cmdBuffer, groupCount, 1, 1);

    auto barrier = make<VkMemoryBarrier>();
    barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
    vkCmdPipelineBarrier(
        cmdBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &barrier, 0, nullptr, 0, nullptr);
}

void CullSystem::occlusionCull(VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapchainImageIndex) {
    if (firstDrawVector[frameIndex]) {
        firstDrawVector[frameIndex] = false;
        return;
    }
    mipmapShader->bindPipeline(cmdBuffer);
    std::vector<Image> depthImages = renderpass->getDepthAndStencilAttachement();
    int numberOfmipmap = mipmapdescriptor[{frameIndex, frameIndex}].size();
    for (int i = 0; i < numberOfmipmap; i++) {
        if (i == 0) {
            depthImages[frameIndex].transitionImageLayout(
                depthImages[frameIndex].getActualImageLayout(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, cmdBuffer);
        } else {
            cullDepthImages[frameIndex].transitionImageLayout(
                cullDepthImages[frameIndex].getActualImageLayout(), VK_IMAGE_LAYOUT_GENERAL, i - 1, 1, cmdBuffer);
        }
        cullDepthImages[frameIndex].transitionImageLayout(
            cullDepthImages[frameIndex].getActualImageLayout(), VK_IMAGE_LAYOUT_GENERAL, i, 1, cmdBuffer);
        uint32_t levelWidth = cullDepthImages[frameIndex].getWidth() >> i;
        uint32_t levelHeight = cullDepthImages[frameIndex].getHeight() >> i;
        if (levelHeight < 1) levelHeight = 1;
        if (levelWidth < 1) levelWidth = 1;
        glm::vec2 imageSize = {levelWidth, levelHeight};
        vkCmdPushConstants(cmdBuffer, mipmapShader->getPipelineLayout(), VK_SHADER_STAGE_COMPUTE_BIT, 0, 8, &imageSize);

        DescriptorSet::bindDescriptorSet(
            cmdBuffer, {mipmapdescriptor[{frameIndex, frameIndex}][i].getDescriptorSet()}, mipmapShader->getPipelineLayout(),
            VK_PIPELINE_BIND_POINT_COMPUTE);

        vkCmdDispatch(cmdBuffer, (levelWidth + 15) / 16, (levelHeight + 15) / 16, 1);

        auto barrier = make<VkMemoryBarrier>();
        barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        VkPipelineStageFlags sourceStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
        VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;

        vkCmdPipelineBarrier(cmdBuffer, sourceStage, destinationStage, 0, 1, &barrier, 0, nullptr, 0, nullptr);
    }

    depthImages[frameIndex].transitionImageLayout(
        depthImages[frameIndex].getActualImageLayout(), depthImages[frameIndex].getImageLayout(), cmdBuffer);
    auto barrier = make<VkMemoryBarrier>();
    barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    vkCmdPipelineBarrier(
        cmdBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &barrier, 0, nullptr, 0, nullptr);
    uint32_t drawCount[4] = {0};

    drawCommandBuffer[frameIndex]->writeToBuffer(&drawCount, 16, 0);
    drawCommandBuffer[frameIndex]->flush(64, 0);

    occlusionShader->bindPipeline(cmdBuffer);
    uint32_t size = uint32_t(scene->meshBlockList.size());
    bool firstDraw = true;
    glm::vec2 imageSize(frameSize.width, frameSize.height);
    vkCmdPushConstants(cmdBuffer, occlusionShader->getPipelineLayout(), VK_SHADER_STAGE_COMPUTE_BIT, 0, 8, &imageSize);
    vkCmdPushConstants(cmdBuffer, occlusionShader->getPipelineLayout(), VK_SHADER_STAGE_COMPUTE_BIT, 8, 4, &size);

    DescriptorSet::bindDescriptorSet(
        cmdBuffer, {cullDescriptorSet[frameIndex].getDescriptorSet()}, occlusionShader->getPipelineLayout(),
        VK_PIPELINE_BIND_POINT_COMPUTE);

    uint32_t groupCount = (size + 255) / 256;

    vkCmdDispatch(cmdBuffer, groupCount, 1, 1);

    barrier = make<VkMemoryBarrier>();
    barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
    vkCmdPipelineBarrier(
        cmdBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &barrier, 0, nullptr, 0, nullptr);
}
}  // namespace vk_stage