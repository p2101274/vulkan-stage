
#include "draw_system.hpp"

#include <cstdint>
#include <vector>

#include "renderer.hpp"

namespace vk_stage {

DrawSystem::DrawSystem(Device* device, Scene* scene, std::vector<std::shared_ptr<Buffer>> drawCommandBuffer)
    : device(device),
      scene(scene),
      drawCommandBuffer(drawCommandBuffer){
}

void DrawSystem::drawScene(VkCommandBuffer cmdBuffer, size_t frameIndex, VkPipelineLayout pipelineLayout) {

    DescriptorSet::bindDescriptorSet(
        cmdBuffer, {scene->uboDescriptorsSests[frameIndex][0].getDescriptorSet(), scene->sceneDescriptorsSet.getDescriptorSet()},
        pipelineLayout, VK_PIPELINE_BIND_POINT_GRAPHICS);
    uint32_t maxDrawCall = 0;
    uint32_t actualDrawCall = 0;
    VkBuffer vbuffers[] = {scene->vertexBuffer->getBuffer()};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(cmdBuffer, 0, 1, vbuffers, offsets);
    vkCmdBindIndexBuffer(cmdBuffer, scene->IndexBuffer->getBuffer(), 0, VK_INDEX_TYPE_UINT32);
    auto test = scene->instanceBuffer.getBufferDeviceAddress();
    vkCmdPushConstants(
            cmdBuffer, pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0,
            sizeof(VkDeviceAddress), &test);
    vkCmdDrawIndexedIndirectCount(
        cmdBuffer, drawCommandBuffer[frameIndex]->getBuffer(), sizeof(uint32_t)*4, drawCommandBuffer[frameIndex]->getBuffer(), 0, 500000,
        sizeof(VkDrawIndexedIndirectCommand));
}

}  // namespace vk_stage