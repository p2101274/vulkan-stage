


#include <glm/ext/vector_float4.hpp>



#include "differed_system_ray_Traced_CACA.hpp"
#include <vulkan/vulkan_core.h>
#include "shader/pipeline/graphic_pipeline.hpp"

namespace vk_stage {

DifferedRayTracingSystemCACA::DifferedRayTracingSystemCACA(
    Device* device, Scene* scene)
    : device(device),
      scene(scene) {
    GraphicPipelineCreateInfo c;
    c.fragmentShaderFile = "slowRT.frag";
    c.vexterShaderFile = "slowRT.vert";

    rtpip = new GraphicPipeline(device, c);
}

void DifferedRayTracingSystemCACA::renderLight(
    VkCommandBuffer cmdBuffer, size_t frameIndex) {

 
    rtpip->bindPipeline(cmdBuffer);

    DescriptorSet::bindDescriptorSet(
        cmdBuffer,
        {scene->uboDescriptorsSests[frameIndex][0].getDescriptorSet()},
        rtpip->getPipelineLayout(), VK_PIPELINE_BIND_POINT_GRAPHICS);



    VkShaderStageFlags flags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    VkDeviceAddress address[3] = {
        scene->IndexBuffer->getBufferDeviceAddress(), scene->vertexBuffer->getBufferDeviceAddress(),
        scene->instanceBuffer.getBufferDeviceAddress()};
    vkCmdPushConstants(cmdBuffer, rtpip->getPipelineLayout(), flags, 0, sizeof(VkDeviceAddress) * 3, &address);
    uint nb = 786801;
    vkCmdPushConstants(
        cmdBuffer, rtpip->getPipelineLayout(), flags, sizeof(VkDeviceAddress) * 3, sizeof(uint), &nb);

     vkCmdDraw(cmdBuffer, 3, 1, 0, 0);

}





}  // namespace vk_stage