

#pragma once




#include <vector>
#include "descriptor/descriptorPool.hpp"
#include "descriptor/descriptorSet.hpp"
#include "dynamic_renderpass.hpp"

#include "scene/scene.hpp"
#include "shader/pipeline.hpp"
#include "shader/pipeline/compute_pipeline.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
#include "shader/pipeline/ray_tracing_pipeline.hpp"
namespace vk_stage {
class DifferedRayTracingSystem {
   public:
    DifferedRayTracingSystem(Device* device, Scene * scene,  DynamicRenderPass* renderpass, std::vector<Image> &swapChainImages);
    void renderLight(VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapChainIndex, float time, bool camMouv);
    void destroy();
   private:
    void createDescriptorSet();
    void genSceneDescriptorSet();
    DynamicRenderPass* renderpass;
    std::vector<DescriptorSet> descriptorSet;
    std::vector<DescriptorSet> dataImgdescriptorSet;

    std::vector<DescriptorSet> renderToDenoiseDescriptorSet;
    std::vector<DescriptorSet> denoiseToRenderDescriptorSet;

    DescriptorSet sceneDescSet;

    std::vector<Image> &swapChainImages;
    std::vector<Image> renderImages;
    std::vector<Image> dataImage;
    std::vector<Image> denoiseImage;


    VkExtent2D imageSize;
    DescriptorPool descpool;
    RayTracingPipeline* rtDifferedPipeline;
    ComputePipeline *horFilterPipeline;
    ComputePipeline *vertFilterPipeline;
    std::vector<uint> numberOfframe = {0,0,0};
   

    Scene * scene;
    Device* device;
};
}  // namespace vk_stage
