

#pragma once




#include <vector>
#include "descriptor/descriptorPool.hpp"
#include "descriptor/descriptorSet.hpp"
#include "dynamic_renderpass.hpp"

#include "scene/scene.hpp"
#include "shader/pipeline.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
namespace vk_stage {
class DifferedSystem {
   public:
    DifferedSystem(Device* device, Scene * scene,  DynamicRenderPass* renderpass);
    void renderLight(VkCommandBuffer cmdBuffer, size_t frameIndex, float time);
    void destroy();
    
   private:
    void createDescriptorSet();
    DynamicRenderPass* renderpass;
    std::vector<DescriptorSet> descriptorSet;

    VkExtent2D imageSize;
    DescriptorPool descpool;
    GraphicPipeline* differedPipeline;
    GraphicPipeline* differedPointPipeline;
    Scene * scene;
    Device* device;
};
}  // namespace vk_stage
