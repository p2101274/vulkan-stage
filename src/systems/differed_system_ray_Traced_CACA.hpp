#pragma once





#include "scene/scene.hpp"
#include "shader/pipeline/graphic_pipeline.hpp"
namespace vk_stage {
class DifferedRayTracingSystemCACA {
   public:
    DifferedRayTracingSystemCACA(Device* device, Scene * scenes);
    void renderLight(VkCommandBuffer cmdBuffer, size_t frameIndex);
    void destroy();
   private:
    GraphicPipeline* rtpip;

    Scene * scene;
    Device* device;
};
}  // namespace vk_stage
