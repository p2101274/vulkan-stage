#pragma once




#include <cstdint>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

#include "device.hpp"
#include "dynamic_renderpass.hpp"
#include "renderer.hpp"
#include "resizable.hpp"
#include "ressources/image.hpp"
#include "scene/scene.hpp"
#include "shader/pipeline/compute_pipeline.hpp"
#include "shader/shader.hpp"
#include "utils.hpp"

namespace vk_stage {

class CullSystem : Resizable {
   public:
    CullSystem(Device* device, Scene* scene, DynamicRenderPass* renderpass,VkExtent2D extent);
    void destroy();

    void frustrumCull(VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapchainImageIndex);
    void occlusionCull(VkCommandBuffer cmdBuffer, size_t frameIndex, uint32_t swapchainImageIndex);
    //void updateCull(VkCommandBuffer cmdBuffer, RenderMethod method, size_t frameIndex, uint32_t swapchainImageIndex);


    std::vector<std::shared_ptr<Buffer>> drawCommandBuffer;
    void resize(VkExtent2D frameSize);

    
   private:
    void createShader();
    void createSampler();


    void cpuCull(size_t frameIndex);
    void createDepthInfo(VkExtent2D frameSize);
    

    ComputePipeline *frustrumShader;
    ComputePipeline *occlusionShader;
    ComputePipeline *mipmapShader;

    std::vector<Image> cullDepthImages;
    bool firstDrawVector[MAX_FRAMES_IN_FLIGHT] = {true};
    DynamicRenderPass* renderpass;
    std::vector<std::vector<VkImageView>> mipmapViews;
    std::unordered_map<std::pair<uint32_t, uint32_t>, std::vector<DescriptorSet>> mipmapdescriptor;
    VkSampler maxDepthSampler;
    VkSampler depthSampler;
    VkExtent2D frameSize;

    std::vector<DescriptorSet> cullDescriptorSet;
    DescriptorPool cullDescriptorPool;
    drawCommandStruct drawCommandList;
    Scene* scene;
    Device* device;
    
};
}  // namespace vk_stage