#pragma once

#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <mutex>
#include <vector>

#include "commandBuffer/commandBuffer.hpp"
#include "device.hpp"
#include "resizable.hpp"
#include "swapchain.hpp"
#include "synchronisation/semaphore.hpp"
#include "utils.hpp"
#include "volk.h"
#include "window.hpp"

namespace vk_stage {
struct RendererCommandBuffer {
    CommandBuffer preComputeCmdBuffer;
    CommandBuffer renderingCmdBuffer;
    CommandBuffer postProcessingCmdBuffer;
    RendererCommandBuffer(CommandBuffer preComputeCmdBuffer, CommandBuffer renderingCmdBuffer, CommandBuffer postProcessingCmdBuffer)
        : preComputeCmdBuffer(preComputeCmdBuffer),
          renderingCmdBuffer(renderingCmdBuffer),
          postProcessingCmdBuffer(postProcessingCmdBuffer){};
};

static constexpr int MAX_FRAMES_IN_FLIGHT = 2;
class Renderer {
   public:
    Renderer(Device *device, Window &window);
    ~Renderer();
    SwapChain *getSwapChain() { return &swapChain; }

    bool startFrame();

    VkCommandBuffer startPrecompute();
    void endPrecompute();
    VkCommandBuffer startRendering();
    void endRendering(bool signalFence);
    VkCommandBuffer startPostProcessing();
    void endPostProcessing(bool signalFence);
    void addRenderPassResize(Resizable *r) { resizableObjects.push_back(r); }

    void endAndPresentFrame();
    float getAspectRatio() { return swapChain.extentAspectRatio(); }
    uint32_t getCurrentSwapChainImageIndex() { return swapChainImageIndex; }
    size_t getCurrentFrameIndex() { return currentFrameIndex; }


    static bool stopRendering;
    static std::condition_variable waitToStartRendering;

   private:
    void createCommandBuffers();
    void createSyncObject();
    void resize();

    Window &window;
    Device *device;
    SwapChain swapChain;
    uint32_t swapChainImageIndex{0};
    size_t currentFrameIndex{0};

    std::mutex stopRenderingMutex;

    std::vector<RendererCommandBuffer> commandBuffers;
    std::vector<Semaphore> preComputeSemaphores;
    std::vector<Semaphore> renderingSemaphores;
    std::vector<Semaphore> postProcessingSemaphores;
    std::vector<SyncObject> sync;
    std::vector<Resizable *> resizableObjects;
    std::chrono::system_clock::time_point frameChrono;
};
}  // namespace vk_stage