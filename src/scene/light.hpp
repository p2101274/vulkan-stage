#pragma once
#include <glm/fwd.hpp>
#include <glm/glm.hpp>

namespace vk_stage {
class Light {
   public:
    Light(glm::vec3 pos, glm::vec3 color, float intensity) : pos(pos), color(color), intensity(intensity) {}

    glm::vec3 pos;
    glm::vec3 color;
    float intensity;

   private:
};
}  // namespace vk_stage