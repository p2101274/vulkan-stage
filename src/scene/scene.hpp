#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include "camera.hpp"
#include "descriptor/descriptorPool.hpp"
#include "descriptor/descriptorSet.hpp"
#include "device.hpp"
#include "object.hpp"
#include "ressources/buffer.hpp"
#include "ressources/image.hpp"
#include "scene/light.hpp"
#include "scene/mesh.hpp"
#include "utils.hpp"
namespace vk_stage {
class Scene {
    struct Batch {
        std::vector<Object *> instances;
        uint32_t instanceToDraw = 0;
        DescriptorSet descriptorSet;
        Mesh *mesh;
        std::vector<MeshBlock> indexGroupList;
        glm::vec3 pmin;
        glm::vec3 pmax;
    };

    struct BoundBox{
        glm::vec3 pmin;
        glm::vec3 pmax;
    };

    struct IntersectionAcceleration {
        uint64_t accelerationStructureDeviceAddress = 0;
        Buffer accelerationStructureBuffer;
        VkAccelerationStructureKHR accelerationStructureHandle;
    };

   public:
    Scene(Device *device)
        : device(device),
          sceneDescriptorPool(
              device,
              1000,
              {PoolSizeRatio{VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0.33}, PoolSizeRatio{VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 0.33},
               PoolSizeRatio{VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 0.33}, PoolSizeRatio{VK_DESCRIPTOR_TYPE_SAMPLER, 0.01}}) {}
    void destroy();

    std::vector<Mesh> meshes;
    std::vector<Object> objects;
    std::vector<Camera> cameras;
    std::vector<Light> lights;
    std::vector<Image> textures;
    std::vector<Material> materials;
    std::vector<std::vector<Buffer>> uboBuffers;
    std::vector<std::vector<DescriptorSet>> uboDescriptorsSests;

    // MESH
    std::shared_ptr<Buffer> vertexBuffer;
    std::shared_ptr<Buffer> IndexBuffer;
    std::vector<MeshBlock> meshBlockList;
    std::vector<ObjectInfo> instanceInfoList;

    // OLD MESH BLOCK
    Buffer meshBlockBuffer;

    Buffer instanceBuffer;

    DescriptorSet sceneDescriptorsSet;
    void genBufferAndDescriptorData(std::shared_ptr<DescriptorSetLayout> uboSetLayout, std::shared_ptr<DescriptorSetLayout> sceneSetLayout);

    void createSphereAccelerationStructure();
    void buildAccelerationStructure(VkBuildAccelerationStructureModeKHR buildMode, VkCommandBuffer cmdBuffer = VK_NULL_HANDLE);

    void updateInstanceData(VkCommandBuffer cmdBuffer = VK_NULL_HANDLE);

    VkAccelerationStructureKHR accelerationStructureHandle;
    VkSampler baseSampler;

    // MATERIAL
    Buffer *materialBuffer;

   private:
    void genUboBuffers(std::shared_ptr<DescriptorSetLayout> uboSetLayout);
    void genMaterialBuffers();
    void genInstanceBuffer();
    void genSceneDescriptorSet(std::shared_ptr<DescriptorSetLayout> sceneSetLayout);

    // GENERAL / CAMERA

    DescriptorPool sceneDescriptorPool;
    Device *device;

    IntersectionAcceleration sphere;

    // RAY TRACING DATA
    uint64_t accelerationStructureDeviceAddress = 0;
    Buffer accelerationStructureBuffer;
    Buffer accelerationInstanceBuffer;
    Buffer scratchBuffer;

    VkAccelerationStructureGeometryKHR accelerationStructureObjectInfo;
    VkAccelerationStructureBuildGeometryInfoKHR accelerationStructureObjectBuildInfo;
    VkAccelerationStructureBuildRangeInfoKHR accelerationStructureBuildRange;
    VkAccelerationStructureBuildSizesInfoKHR accelerationStructureBuildSizesInfo;
};
}  // namespace vk_stage