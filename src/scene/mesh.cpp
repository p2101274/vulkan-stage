
#include "mesh.hpp"

#include <cstddef>
#include <cstdint>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <utility>

#include "commandBuffer/commandBufferPoolHandler.hpp"
#include "structs_vk.hpp"
#define GLM_ENABLE_EXPERIMENTAL

#include <unordered_map>
#include <vector>

#include "glm/gtx/hash.hpp"
#include "ressources/buffer.hpp"

namespace vk_stage {

Mesh::Mesh(Device *device, std::shared_ptr<Buffer> vertexBuffer, std::shared_ptr<Buffer> indexBuffer) : device(device) {
    if (indexBuffer != nullptr) {
        this->vertexBuffer = vertexBuffer;
        this->indexBuffer = indexBuffer;
    }
}

void Mesh::destroy() {
    if (hasOwnBuffer) {
        indexBuffer->destroy();
        vertexBuffer->destroy();
    }
}

void Mesh::bind(VkCommandBuffer commandBuffer) {
    VkBuffer vbuffers[] = {vertexBuffer->getBuffer()};
    VkDeviceSize offsets[] = {firstVertex};
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, vbuffers, offsets);
    vkCmdBindIndexBuffer(commandBuffer, indexBuffer->getBuffer(), firstIndex, VK_INDEX_TYPE_UINT32);
}

void Mesh::createVertexBuffer(const std::vector<Vertex> &vertices, uint32_t firstVertex) {
    this->firstVertex = firstVertex;
    vertexCount = static_cast<uint32_t>(vertices.size());
    assert(vertexCount >= 3 && "Vertex count must be at least 3");
    VkDeviceSize bufferSize = sizeof(vertices[0]) * vertexCount;
    uint32_t vertexSize = sizeof(vertices[0]);

    Buffer stagingBuffer{
        device,
        vertexSize,
        vertexCount,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };

    stagingBuffer.map();
    stagingBuffer.writeToBuffer((void *)vertices.data());
    if (vertexBuffer == nullptr) {
        vertexBuffer = std::make_shared<Buffer>(
            device, vertexSize, vertexCount, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    }

    Buffer::copyBuffer(device, stagingBuffer.getBuffer(), vertexBuffer->getBuffer(), bufferSize, firstVertex * vertexSize);
    stagingBuffer.destroy();
}

void Mesh::createIndexBuffer(std::vector<uint32_t> &indices, const std::vector<Vertex> &vertices, uint32_t firstIndex, bool splited) {
    indexCount = static_cast<uint32_t>(indices.size());
    this->firstIndex = firstIndex;
    struct BoundBox {
        glm::vec3 pmin = {FLT_MAX, FLT_MAX, FLT_MAX};
        glm::vec3 pmax = {-FLT_MAX, -FLT_MAX, -FLT_MAX};
    };

    if (!indexCount) {
        return;
    }
    if (splited) {
        std::unordered_map<glm::ivec3, std::pair<std::vector<uint>, BoundBox>> triangleSplitMap;
        for (int i = 0; i + 2 < indices.size(); i += 3) {
            int x =
                (int(vertices[indices[i]].position.x + vertices[indices[i + 1]].position.x + vertices[indices[i + 2]].position.x) / 300);
            int y =
                (int(vertices[indices[i]].position.y + vertices[indices[i + 1]].position.y + vertices[indices[i + 2]].position.y) / 300);
            int z =
                (int(vertices[indices[i]].position.z + vertices[indices[i + 1]].position.z + vertices[indices[i + 2]].position.z) / 300);
            triangleSplitMap[glm::ivec3(x, y, z)].second.pmin =
                min(triangleSplitMap[glm::ivec3(x, y, z)].second.pmin, vertices[indices[i]].position);
            triangleSplitMap[glm::ivec3(x, y, z)].second.pmin =
                min(triangleSplitMap[glm::ivec3(x, y, z)].second.pmin, vertices[indices[i + 1]].position);
            triangleSplitMap[glm::ivec3(x, y, z)].second.pmin =
                min(triangleSplitMap[glm::ivec3(x, y, z)].second.pmin, vertices[indices[i + 2]].position);

            triangleSplitMap[glm::ivec3(x, y, z)].second.pmax =
                max(triangleSplitMap[glm::ivec3(x, y, z)].second.pmax, vertices[indices[i]].position);
            triangleSplitMap[glm::ivec3(x, y, z)].second.pmax =
                max(triangleSplitMap[glm::ivec3(x, y, z)].second.pmax, vertices[indices[i + 1]].position);
            triangleSplitMap[glm::ivec3(x, y, z)].second.pmax =
                max(triangleSplitMap[glm::ivec3(x, y, z)].second.pmax, vertices[indices[i + 2]].position);

            triangleSplitMap[glm::ivec3(x, y, z)].first.push_back(indices[i]);
            triangleSplitMap[glm::ivec3(x, y, z)].first.push_back(indices[i + 1]);
            triangleSplitMap[glm::ivec3(x, y, z)].first.push_back(indices[i + 2]);
        }

        indices.clear();
        uint firstblockIndex = 0;

        for (auto &newindices : triangleSplitMap) {
            indices.insert(indices.end(), newindices.second.first.begin(), newindices.second.first.end());
            meshBlockList.push_back(MeshBlock{
                newindices.second.second.pmin, int32_t(this->firstVertex), newindices.second.second.pmax,

                firstblockIndex + this->firstIndex, uint32_t(newindices.second.first.size()), -1});
            firstblockIndex += newindices.second.first.size();
        }
    }
    VkDeviceSize bufferSize = sizeof(indices[0]) * indexCount;
    uint32_t indexSize = sizeof(indices[0]);

    Buffer stagingBuffer{
        device,
        indexSize,
        indexCount,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    };

    stagingBuffer.map();
    stagingBuffer.writeToBuffer((void *)indices.data());
    if (indexBuffer == nullptr) {
        indexBuffer = std::make_shared<Buffer>(
            device, indexSize, indexCount, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    }
    Buffer::copyBuffer(device, stagingBuffer.getBuffer(), indexBuffer->getBuffer(), bufferSize, firstIndex * indexSize);
    stagingBuffer.destroy();
}

std::vector<VkVertexInputBindingDescription> Vertex::getBindingDescriptions() {
    std::vector<VkVertexInputBindingDescription> returnValue;
    return returnValue;
}

std::vector<VkVertexInputAttributeDescription> Vertex::getAttributeDescriptions() {
    std::vector<VkVertexInputAttributeDescription> returnValue;
    return returnValue;
}

void Mesh::createRaytracingData() {
    geometryAcc = make<VkAccelerationStructureGeometryKHR>();
    geometryAcc.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    geometryAcc.flags = 0;
    geometryAcc.geometry.triangles.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    geometryAcc.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
    geometryAcc.geometry.triangles.vertexStride = sizeof(Vertex);
    geometryAcc.geometry.triangles.maxVertex = vertexCount;
    geometryAcc.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
    geometryAcc.geometry.triangles.transformData = {};
    geometryAcc.geometry.triangles.indexData.deviceAddress = indexBuffer->getBufferDeviceAddress();
    geometryAcc.geometry.triangles.vertexData.deviceAddress = vertexBuffer->getBufferDeviceAddress();
    
    

    geometryBuildAcc = make<VkAccelerationStructureBuildGeometryInfoKHR>();
    geometryBuildAcc.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    geometryBuildAcc.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    geometryBuildAcc.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    geometryBuildAcc.geometryCount = 1;
    geometryBuildAcc.pGeometries = &geometryAcc;
    geometryBuildAcc.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    

    accelerationStructureBuildRange = make<VkAccelerationStructureBuildRangeInfoKHR>();
    accelerationStructureBuildRange.firstVertex = firstVertex;
    accelerationStructureBuildRange.primitiveCount = indexCount / 3;
    accelerationStructureBuildRange.primitiveOffset = firstIndex * sizeof(uint32_t);

    // Get size for create/build/update acceleration structure
    const uint32_t numTriangles = indexCount / 3;
    accelerationStructureBuildSizesInfo = make<VkAccelerationStructureBuildSizesInfoKHR>();
    accelerationStructureBuildSizesInfo.pNext = NULL;
    
    vkGetAccelerationStructureBuildSizesKHR(
        device->device(), VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &geometryBuildAcc, &numTriangles,
        &accelerationStructureBuildSizesInfo);

    accelerationStructureBuffer = Buffer(
        device, accelerationStructureBuildSizesInfo.accelerationStructureSize, 1,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    accelerationStructureCreateInfo = make<VkAccelerationStructureCreateInfoKHR>();
    accelerationStructureCreateInfo.buffer = accelerationStructureBuffer.getBuffer();
    accelerationStructureCreateInfo.size = accelerationStructureBuildSizesInfo.accelerationStructureSize;
    accelerationStructureCreateInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;

    vkCreateAccelerationStructureKHR(device->device(), &accelerationStructureCreateInfo, nullptr, &accelerationStructureHandle);
    geometryBuildAcc.dstAccelerationStructure = accelerationStructureHandle;
}

void Mesh::buildAccelerationStructure(VkBuildAccelerationStructureModeKHR buildMode, VkCommandBuffer cmdBuffer) {
    VkCommandBuffer cmd = cmdBuffer;
    accelerationScratchBuffer = Buffer(
        device, accelerationStructureBuildSizesInfo.buildScratchSize, 1,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    if (buildMode == VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR) {
        geometryBuildAcc.srcAccelerationStructure = accelerationStructureHandle;
    } else {
        geometryBuildAcc.srcAccelerationStructure = VK_NULL_HANDLE;
    }

    geometryBuildAcc.scratchData.deviceAddress = accelerationScratchBuffer.getBufferDeviceAddress();

    if (cmdBuffer == VK_NULL_HANDLE) {
        cmd = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();
    }
    const VkAccelerationStructureBuildRangeInfoKHR *buildOffsetInfo = &accelerationStructureBuildRange;
    vkCmdBuildAccelerationStructuresKHR(cmd, 1, &geometryBuildAcc, &buildOffsetInfo);

    VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo{};
    accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    accelerationDeviceAddressInfo.accelerationStructure = accelerationStructureHandle;
    accelerationStructureDeviceAddress = vkGetAccelerationStructureDeviceAddressKHR(device->device(), &accelerationDeviceAddressInfo);

    if (cmdBuffer == VK_NULL_HANDLE) {
        CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(cmd);
        accelerationScratchBuffer.destroy();
    }
}

}  // namespace vk_stage