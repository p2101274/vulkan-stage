#pragma once

#include <cstdint>
#include <glm/glm.hpp>

#include "ressources/buffer.hpp"
#include "volk.h"

namespace vk_stage {

struct TransformComponent {
    glm::vec3 translation{};
    glm::vec3 scale{1.f, 1.f, 1.f};
    glm::vec3 rotation{};

    // Matrix corrsponds to Translate * Ry * Rx * Rz * Scale
    // Rotations correspond to Tait-bryan angles of Y(1), X(2), Z(3)
    // https://en.wikipedia.org/wiki/Euler_angles#Rotation_matrix
    glm::mat4 mat4();
    glm::mat3 normalMatrix();
};

enum objectType {
    mesh = 0,
    sphere = 1,
    cube = 2,
    cylinder = 3,
    plan = 4
};

class Object {
   public:
    TransformComponent transform;


    glm::vec3 AABBmin;
    glm::vec3 AABBmax;
    objectType objType = mesh;
    int customObjMaterial;
    
    uint32_t mesh_id;
    Buffer* instanceBuffer;
    uint instanceIndex;
    bool needGPUupdate;

   private:
};

}  // namespace vk_stage