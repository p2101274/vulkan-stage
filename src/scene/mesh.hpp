#pragma once


#include <cstdint>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <memory>
#include <vector>

#include "device.hpp"
#include "ressources/buffer.hpp"
#include "volk.h"

namespace vk_stage {

struct Vertex {
    glm::vec3 position{};
    glm::vec3 normal{};
    glm::vec2 uv{};
    uint32_t material_id{};

    static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
    static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

    bool operator==(const Vertex &other) const {
        return position == other.position && material_id == other.material_id && normal == other.normal && uv == other.uv;
    }
};

struct MeshBlock {
    glm::vec3 pmin;
    int32_t vertexOffset;

    glm::vec3 pmax;
    uint32_t indexOffset;
    
    uint32_t indexSize;

    int32_t instancesID = -1;
    uint32_t checkFrustrumCull = 0;
    uint32_t wasDraw = 0;
};




class Mesh {
   public:
    Mesh(Device *device, std::shared_ptr<Buffer> vertexBuffer = nullptr, std::shared_ptr<Buffer> indexBuffer = nullptr);
    void createVertexBuffer(const std::vector<Vertex> &vertices, uint32_t firstVertex = 0);
    void createIndexBuffer(
        std::vector<uint32_t> &indices, const std::vector<Vertex> &vertices = {}, uint32_t firstIndex = 0, bool splited = false);

    void bind(VkCommandBuffer commandBuffer);
    void destroy();
    void destroySractchBuffer(){accelerationScratchBuffer.destroy();}

    void createRaytracingData();
    void buildAccelerationStructure(VkBuildAccelerationStructureModeKHR buildMode,VkCommandBuffer cmdBuffer = VK_NULL_HANDLE);

    glm::vec3 pmin = {FLT_MAX, FLT_MAX, FLT_MAX};
    glm::vec3 pmax = {-FLT_MAX, -FLT_MAX, -FLT_MAX};



    //DATA
    std::shared_ptr<Buffer> vertexBuffer;
    std::shared_ptr<Buffer> indexBuffer;

    bool hasOwnBuffer =false;
    uint32_t firstIndex;
    uint32_t firstVertex;
    uint32_t indexCount;
    uint32_t vertexCount;

    std::vector<MeshBlock> meshBlockList;
    
    
    //RAY TRACING DATA
    VkAccelerationStructureKHR accelerationStructureHandle;
    VkDeviceAddress accelerationStructureDeviceAddress = 0;
    Buffer accelerationStructureBuffer;

    VkAccelerationStructureGeometryKHR geometryAcc;
    VkAccelerationStructureBuildGeometryInfoKHR geometryBuildAcc;
    VkAccelerationStructureBuildSizesInfoKHR accelerationStructureBuildSizesInfo{};
    VkAccelerationStructureBuildRangeInfoKHR accelerationStructureBuildRange;
    VkAccelerationStructureCreateInfoKHR accelerationStructureCreateInfo;
    Buffer accelerationScratchBuffer;

   private:
    Device *device;
};
}  // namespace vk_stage