#pragma once
//! \file gltf.h scene glTF.

#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <vector>

#include "external/cgltf.hpp"
#include "device.hpp"
#include "ressources/image.hpp"
#include "scene/camera.hpp"
#include "scene/scene.hpp"
// #include "image_io.h"
#include "utils.hpp"



namespace vk_stage {
    /*! representation d'une scene statique glTF.

        resume du format glTF : https://github.com/KhronosGroup/glTF-Tutorials/blob/master/gltfTutorial/README.md

        specification complete : https://registry.khronos.org/glTF/specs/2.0/glTF-2.0.html

        parser gltf : https://github.com/jkuhlmann/cgltf

        une scene est un ensemble de maillages places et orientes dans un repere.
        un GLTFNode permet de dessiner un maillage GLTFMesh a sa place.
        un maillage est un ensemble de groupes de triangles / primitives. cf GLTFPrimitives.
        un groupe de primitives est associe a une matiere. cf GLTFMaterial.
     */
class GLTFLoader {
    public:

        //! charge un fichier .gltf et construit une scene statique, sans animation.
    static Scene read_gltf_scene(std::string filename, Device *device);


    //! charge un fichier .gltf et charge les images referencees par les matieres.
    

    //! groupe de triangles d'un maillage. chaque groupe est associe a une matiere.
    struct GLTFPrimitives {
        int primitives_mode;   //!< triangles.
        int primitives_index;  //!< indice unique.
        int material_index;    //!< indice de la matiere des primitives.

        glm::vec3 pmin, pmax;  //!< points extremes de l'englobant dans le repere objet

        // buffers...
        std::vector<unsigned> indices;
        std::vector<glm::vec3> positions;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> normals;
    };

    //! instances d'un maillage.
    struct GLTFInstances {
        std::vector<TransformComponent> transforms;  //!< transformation model de chaque instance
        int mesh_index;                              //!< indice du maillage instancie.
    };

    //! position et orientation d'un maillage dans la scene.
    struct GLTFNode {
        TransformComponent model;  //!< transformation model pour dessiner le maillage.
        int mesh_index;            //!< indice du maillage.
    };
    private:

    static std::vector<Camera> read_cameras(cgltf_data *data);

    static std::vector<Light> read_lights(cgltf_data *data);

    static std::vector<Material> read_materials(cgltf_data *data, std::vector<bool> &isAlbedoTexture);

    static std::vector<Image> read_gltf_images(Device *device, cgltf_data *data, std::vector<bool> &isAlbedoTexture);

};
}  // namespace vk_stage
