
#include "gltf.hpp"

#include <omp.h>

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <glm/fwd.hpp>
#include <glm/geometric.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/matrix.hpp>
#include <glm/trigonometric.hpp>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "commandBuffer/commandBufferPoolHandler.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <chrono>
#include <glm/gtx/string_cast.hpp>

#include "device.hpp"
#include "external/cgltf.hpp"
#include "ressources/image.hpp"
#include "scene/camera.hpp"
#include "scene/light.hpp"
#include "scene/mesh.hpp"
#include "scene/object.hpp"
#include "scene/scene.hpp"
#include "utils.hpp"

#ifndef GLTF_DIR
#define GLTF_DIR "../gltf/"
#endif

namespace vk_stage {

void decomposeTransform(const glm::mat4 &mat, TransformComponent &t) {
    // Extraire la translation
    t.translation = glm::vec3(mat[3]);

    // Extraire les colonnes de rotation et d'échelle
    glm::highp_vec3 col1(mat[0][0], mat[0][1], mat[0][2]);
    glm::highp_vec3 col2(mat[1][0], mat[1][1], mat[1][2]);
    glm::highp_vec3 col3(mat[2][0], mat[2][1], mat[2][2]);

    // Extraire l'échelle
    t.scale.x = glm::length(col1);
    t.scale.y = glm::length(col2);
    t.scale.z = glm::length(col3);

    // Supprimer l'échelle des colonnes de rotation
    if (t.scale.x != 0) col1 /= t.scale.x;
    if (t.scale.y != 0) col2 /= t.scale.y;
    if (t.scale.z != 0) col3 /= t.scale.z;

    // Construire une matrice de rotation 3x3
    glm::highp_mat3 rotMatrix{
        {col1.x, col1.y, col1.z},
        {col2.x, col2.y, col2.z},
        {col3.x, col3.y, col3.z},
    };

    int sinTable[8][3] = {{-1, -1, -1}, {-1, -1, 1}, {-1, 1, -1}, {-1, 1, 1}, {1, -1, -1}, {1, -1, 1}, {1, 1, -1}, {1, 1, 1}};

    for (int i = 0; i < 8; i++) {
        float x = sinTable[i][0] * glm::asin(-col3.y);
        float y = sinTable[i][1] * glm::asin(col3.x / glm::cos(x));
        float z = sinTable[i][2] * glm::asin(col1.y / glm::cos(x));
        t.rotation = {x, y, z};
        if (mat == t.mat4()) {
            std::cout << "ok" << std::endl;
            return;
        }
    }
    assert(!("shit"));

    // rotation = {x, y, z};
}

std::vector<Camera> GLTFLoader::read_cameras(cgltf_data *data) {
    std::vector<Camera> cameras;
    for (unsigned i = 0; i < data->nodes_count; i++) {
        std::cout << "loading camera " << i + 1 << "/" << data->nodes_count << std::endl;
        cgltf_node *node = &data->nodes[i];
        if (node->camera == nullptr) continue;

        cgltf_camera_perspective *perspective = &node->camera->data.perspective;

        Camera cam{};
        cam.setPerspectiveProjection(perspective->yfov, perspective->aspect_ratio, perspective->znear, perspective->zfar);

        float model_matrix[16];
        cgltf_node_transform_world(node, model_matrix);  // transformation globale

        glm::mat4 viewMatrix = glm::make_mat4(model_matrix);
        decomposeTransform(viewMatrix, cam.viewerObject.transform);
        cam.setViewYXZ(cam.viewerObject.transform.translation, cam.viewerObject.transform.translation);

        cameras.push_back(cam);
    }

    return cameras;
}

std::vector<Light> GLTFLoader::read_lights(cgltf_data *data) {
    std::vector<Light> lights;
    // retrouve les transformations associees aux sources
    for (unsigned i = 0; i < data->nodes_count; i++) {
        std::cout << "loading light " << i + 1 << "/" << data->nodes_count << std::endl;
        cgltf_node *node = &data->nodes[i];
        if (node->light == nullptr) continue;

        // position de la source
        float model_matrix[16];
        cgltf_node_transform_world(node, model_matrix);  // transformation globale

        glm::mat4 model = glm::make_mat4(model_matrix);
        glm::vec3 transla;
        glm::vec3 rotation;
        glm::vec3 scale;
        TransformComponent t;
        decomposeTransform(model, t);

        glm::vec3 position = transla;

        // proprietes de la source
        cgltf_light *light = node->light;

        lights.push_back({position, glm::vec3(light->color[0], light->color[1], light->color[2]), light->intensity});
    }

    // for (int i = 0; i < 10; i++){
    //     glm::vec3 pos {static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/10)), static_cast <float> (rand()) / (static_cast
    //     <float> (RAND_MAX/10)), static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/10))}; glm::vec3 color {static_cast <float>
    //     (rand()) / static_cast <float> (RAND_MAX), static_cast <float> (rand()) / static_cast <float> (RAND_MAX), static_cast <float>
    //     (rand()) / static_cast <float> (RAND_MAX)};
    //      lights.push_back({pos, color, 2});
    // }

    return lights;
}

std::string pathname(const std::string &filename) {
    std::string path = filename;
#ifndef WIN32
    std::replace(path.begin(), path.end(), '\\', '/');  // linux, macos : remplace les \ par /.
    size_t slash = path.find_last_of('/');
    if (slash != std::string::npos)
        return path.substr(0, slash + 1);  // inclus le slash
    else
        return "./";
#else
    std::replace(path.begin(), path.end(), '/', '\\');  // windows : remplace les / par \.
    size_t slash = path.find_last_of('\\');
    if (slash != std::string::npos)
        return path.substr(0, slash + 1);  // inclus le slash
    else
        return ".\\";
#endif
}

std::vector<Material> GLTFLoader::read_materials(cgltf_data *data, std::vector<bool> &isAlbedoTexture) {
    std::vector<Material> materials;
    for (unsigned i = 0; i < data->materials_count; i++) {
        std::cout << "loading material " << i + 1 << "/" << data->materials_count << std::endl;
        cgltf_material *material = &data->materials[i];

        Material m = {};
        // m.color = glm::vec4(0.8, 0.8, 0.8, 1);
        // m.metallic = 0;
        // m.roughness = 1;
        // m.transmission = 0;
        // m.ior = 0;
        // m.specular = 0;
        // m.specular_color = Black();
        // m.thickness = 0;
        // m.attenuation_distance = 0;
        // m.attenuation_color = Black();
        // m.color_texture = -1;
        // m.metallic_roughness_texture = -1;
        // m.occlusion_texture = -1;
        // m.normal_texture = -1;
        // m.emission_texture = -1;
        // m.transmission_texture = -1;
        // m.specular_texture = -1;
        // m.specular_color_texture = -1;
        // m.thickness_texture = -1;

        if (material->has_pbr_metallic_roughness) {
            cgltf_pbr_metallic_roughness *pbr = &material->pbr_metallic_roughness;

            m.color = glm::vec4(pbr->base_color_factor[0], pbr->base_color_factor[1], pbr->base_color_factor[2], pbr->base_color_factor[3]);
            if (pbr->base_color_texture.texture && pbr->base_color_texture.texture->image)
                m.color_texture_id = int(std::distance(data->images, pbr->base_color_texture.texture->image));

            m.metallic = pbr->metallic_factor;

            m.roughness = pbr->roughness_factor;
            if (pbr->metallic_roughness_texture.texture && pbr->metallic_roughness_texture.texture->image) {
                m.metallic_roughness_texture_id = int(std::distance(data->images, pbr->metallic_roughness_texture.texture->image));
                isAlbedoTexture[m.metallic_roughness_texture_id] = false;
            }

            // printf("  pbr metallic roughness\n");
            // printf("    base color %f %f %f, texture %d\n", m.color.r, m.color.g, m.color.b, m.color_texture);
            // printf("    metallic %f, roughness %f, texture %d\n", m.metallic, m.roughness, m.metallic_roughness_texture);
        }
        // if (material->has_clearcoat) printf("  clearcoat\n");
        // //~ if(material->has_sheen)
        // //~ printf("  sheen\n");
        //
        if (material->normal_texture.texture && material->normal_texture.texture->image) {
            //~ printf("  normal texture %d\n", int(std::distance(data->images, material->normal_texture.texture->image)));
            m.normalMap_texture_id = int(std::distance(data->images, material->normal_texture.texture->image));
            isAlbedoTexture[m.normalMap_texture_id] = false;
        }
        //
        // //~ printf("  emissive color %f %f %f\n", material->emissive_factor[0], material->emissive_factor[1],
        // material->emissive_factor[2]); m.emission = Color(material->emissive_factor[0], material->emissive_factor[1],
        // material->emissive_factor[2]); if (material->emissive_texture.texture && material->emissive_texture.texture->image) {
        //     //~ printf("    texture %d\n", int(std::distance(data->images, material->emissive_texture.texture->image)));
        //     m.emission_texture = int(std::distance(data->images, material->emissive_texture.texture->image));
        // }
        //
        // if (material->has_ior) {
        //     m.ior = material->ior.ior;
        //     if (m.ior == float(1.5)) m.ior = 0;  // valeur par defaut
        //
        //     if (m.ior) printf("    ior %f\n", m.ior);
        // }
        //
        // if (material->has_specular) {
        //     m.specular = material->specular.specular_factor;
        //     if (material->specular.specular_texture.texture && material->specular.specular_texture.texture->image)
        //         m.specular_texture = std::distance(data->images, material->specular.specular_texture.texture->image);
        //
        //     m.specular_color = Color(
        //         material->specular.specular_color_factor[0], material->specular.specular_color_factor[1],
        //         material->specular.specular_color_factor[2]);
        //     if (material->specular.specular_color_texture.texture && material->specular.specular_color_texture.texture->image)
        //         m.specular_color_texture = std::distance(data->images, material->specular.specular_color_texture.texture->image);
        //
        //     if (m.specular_color.r * m.specular + m.specular_color.g * m.specular + m.specular_color.b * m.specular == 0) {
        //         // parametres incoherents... valeur par defaut / desactive ce comportement
        //         m.specular = 0;
        //         m.specular_color = Black();
        //     }
        //
        //     if (m.specular)
        //         printf(
        //             "    specular %f color %f %f %f, texture %d\n", m.specular, m.specular_color.r, m.specular_color.g,
        //             m.specular_color.b, m.specular_texture);
        // }
        //
        // if (material->has_transmission) {
        //     m.transmission = material->transmission.transmission_factor;
        //     if (material->transmission.transmission_texture.texture && material->transmission.transmission_texture.texture->image)
        //         m.transmission_texture = std::distance(data->images, material->transmission.transmission_texture.texture->image);
        //
        //     if (m.transmission) printf("    transmission %f, texture %d\n", m.transmission, m.transmission_texture);
        // }
        //
        // if (material->has_volume) {
        //     m.thickness = material->volume.thickness_factor;
        //     if (material->volume.thickness_texture.texture && material->volume.thickness_texture.texture->image)
        //         m.thickness_texture = std::distance(data->images, material->volume.thickness_texture.texture->image);
        //
        //     m.attenuation_distance = material->volume.attenuation_distance;
        //     m.attenuation_color =
        //         Color(material->volume.attenuation_color[0], material->volume.attenuation_color[1],
        //         material->volume.attenuation_color[2]);
        //     printf("    volume thickness %f, texture %d\n", m.thickness, m.thickness_texture);
        //     printf(
        //         "    volume attenation distance %f, color %f %f %f\n", m.attenuation_distance, m.attenuation_color.r,
        //         m.attenuation_color.g, m.attenuation_color.b);
        // }

        materials.push_back(m);
    }

    return materials;
}

std::vector<Image> GLTFLoader::read_gltf_images(Device *device, cgltf_data *data, std::vector<bool> &isAlbedoTexture) {
    cgltf_result code;
    cgltf_options options = {};
     std::vector<Image> images;
     
    if (data->images_count == 0) {
        auto cmdBuffer = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();
        printf("[warning] no images...\n");

        ImageCreateInfo imageCreateInfo{};
        imageCreateInfo.filename = {GLTF_DIR + std::string("white.png")};
        imageCreateInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        imageCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;

        imageCreateInfo.usageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

        images.push_back(Image(device, imageCreateInfo, cmdBuffer));
        CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(cmdBuffer, true);
        return images;
    }

    auto start = std::chrono::high_resolution_clock::now();
   
    images.resize(data->images_count);
   
// #pragma omp parallel for schedule(dynamic, 1)
    for (unsigned i = 0; i < data->images_count; i++) {
        std::cout << "loading image " << i + 1 << "/" << data->images_count << std::endl;
        if (data->images[i].uri) {
            ImageCreateInfo imageCreateInfo{};
            imageCreateInfo.filename = {GLTF_DIR + std::string(data->images[i].uri)};
            imageCreateInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            if (!isAlbedoTexture[i]) {
                imageCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
            }
            imageCreateInfo.usageFlags = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

            images[i] = (Image(device, imageCreateInfo));
        } else if (data->images[i].buffer_view) {
            // extraire l'image du glb...
            cgltf_buffer_view *view = data->images[i].buffer_view;
            assert(view->buffer->data);
            // //~ printf("  [%u] %s offset %lu size %lu, type '%s'\n", i, data->images[i].name, view->offset, view->size,
            // //data->images[i].mime_type);

            // SDL_RWops *read = SDL_RWFromConstMem((uint8_t *)view->buffer->data + view->offset, view->size);
            // assert(read);

            // images[i] = image_data(IMG_Load_RW(read, /* free RWops */ 1));
        }
    }
    vkDeviceWaitIdle(device->device());

    
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> duration = end - start;

    std::cout << "Temps écoulé: " << duration.count() << " secondes" << std::endl;

    return images;
}

Scene GLTFLoader::read_gltf_scene(std::string filename, Device *device) {
    printf("loading glTF scene '%s'...\n", filename.c_str());

    cgltf_options options = {};
    cgltf_data *data = nullptr;
    cgltf_result code = cgltf_parse_file(&options, (GLTF_DIR + filename).c_str(), &data);
    if (code != cgltf_result_success) {
        throw std::runtime_error("[error] loading glTF mesh");
    }

    if (cgltf_validate(data) != cgltf_result_success) {
        throw std::runtime_error("[error] invalid glTF mesh");
    }

    code = cgltf_load_buffers(&options, data, (GLTF_DIR + filename).c_str());
    if (code != cgltf_result_success) {
        throw std::runtime_error("[error] invalid glTF buffers");
    }

    //
    Scene scene{device};

    // etape 1 : construire les meshs et les groupes de triangles / primitives
    int primitives_index = 0;
    std::vector<float> buffer;
    // parcourir tous les meshs de la scene

    uint32_t numberOfVertex = 0;
    uint32_t numberOfIndices = 0;
    for (unsigned mesh_id = 0; mesh_id < data->meshes_count; mesh_id++) {
        cgltf_mesh *mesh = &data->meshes[mesh_id];
        for (unsigned primitive_id = 0; primitive_id < mesh->primitives_count; primitive_id++) {
            cgltf_primitive *primitives = &mesh->primitives[primitive_id];
            if (primitives->indices) {
                numberOfIndices += primitives->indices->count;
            }
            for (unsigned attribute_id = 0; attribute_id < primitives->attributes_count; attribute_id++) {
                cgltf_attribute *attribute = &primitives->attributes[attribute_id];

                if (attribute->type == cgltf_attribute_type_position) {
                    assert(attribute->data->type == cgltf_type_vec3);

                    numberOfVertex += cgltf_accessor_unpack_floats(attribute->data, nullptr, 0) / 3;
                }
            }
        }
    }
    uint32_t indexSize = sizeof(uint32_t);
    uint32_t vertexSize = sizeof(Vertex);
    scene.IndexBuffer = std::make_shared<Buffer>(
        device, indexSize, numberOfIndices,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
            VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT);
    scene.vertexBuffer = std::make_shared<Buffer>(
        device, vertexSize, numberOfVertex,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
            VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT);

    uint32_t globalIndicesIndex = 0;
    uint32_t globalVertexIndex = 0;

    for (unsigned mesh_id = 0; mesh_id < data->meshes_count; mesh_id++) {
        std::cout << "loading mesh " << mesh_id + 1 << "/" << data->meshes_count << std::endl;
        uint32_t previous_max_index = 0;
        Mesh m{device, scene.vertexBuffer, scene.IndexBuffer};
        std::vector<Vertex> vertexs;
        std::vector<uint32_t> indices;

        cgltf_mesh *mesh = &data->meshes[mesh_id];
        // parcourir les groupes de triangles du mesh...
        for (unsigned primitive_id = 0; primitive_id < mesh->primitives_count; primitive_id++) {
            cgltf_primitive *primitives = &mesh->primitives[primitive_id];
            assert(primitives->type == cgltf_primitive_type_triangles);

            // GLTFPrimitives p = {};

            // matiere associee au groupe de triangles
            int material = -1;
            // p.material_index = -1;
            if (primitives->material) material = std::distance(data->materials, primitives->material);

            // indices
            if (primitives->indices) {
                for (unsigned i = 0; i < primitives->indices->count; i++) {
                    indices.push_back((cgltf_accessor_read_index(primitives->indices, i) + previous_max_index));
                }
                assert(indices.size() % 3 == 0);
            }
            std::vector<glm::vec3> pos;
            std::vector<glm::vec3> normal;
            std::vector<glm::vec2> uv;

            // attributs
            for (unsigned attribute_id = 0; attribute_id < primitives->attributes_count; attribute_id++) {
                cgltf_attribute *attribute = &primitives->attributes[attribute_id];

                if (attribute->type == cgltf_attribute_type_position) {
                    assert(attribute->data->type == cgltf_type_vec3);

                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());
                    // transforme les positions des sommets
                    for (unsigned i = 0; i + 2 < buffer.size(); i += 3) pos.push_back(glm::vec3(buffer[i], buffer[i + 1], buffer[i + 2]));
                }

                if (attribute->type == cgltf_attribute_type_normal) {
                    assert(attribute->data->type == cgltf_type_vec3);

                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());

                    // transforme les normales des sommets
                    for (unsigned i = 0; i + 2 < buffer.size(); i += 3)
                        normal.push_back(glm::vec3(buffer[i], buffer[i + 1], buffer[i + 2]));
                }

                if (attribute->type == cgltf_attribute_type_texcoord) {
                    assert(attribute->data->type == cgltf_type_vec2);

                    buffer.resize(cgltf_accessor_unpack_floats(attribute->data, nullptr, 0));
                    cgltf_accessor_unpack_floats(attribute->data, buffer.data(), buffer.size());

                    for (unsigned i = 0; i + 1 < buffer.size(); i += 2) uv.push_back(glm::vec2(buffer[i], buffer[i + 1]));
                }
            }

            for (int i = 0; i < pos.size(); i++) {
                Vertex v;
                v.position = pos[i];
                v.normal = normal[i];
                v.uv = uv[i];
                v.material_id = material;
                m.pmin = min(m.pmin, v.position);
                m.pmax = max(m.pmax, v.position);

                vertexs.push_back(v);
            }

            previous_max_index = vertexs.size();
        }
        m.createVertexBuffer(vertexs, globalVertexIndex);
        globalVertexIndex += vertexs.size();
        m.createIndexBuffer(indices, vertexs, globalIndicesIndex, true);
        globalIndicesIndex += indices.size();

        scene.meshes.push_back(m);
    }

    // etape 2 : parcourir les noeuds, retrouver les transforms pour placer les meshes
    for (unsigned node_id = 0; node_id < data->nodes_count; node_id++) {
        std::cout << "loading object " << node_id + 1 << "/" << data->nodes_count << std::endl;
        cgltf_node *node = &data->nodes[node_id];
        if (node->mesh == nullptr)
            // pas de mesh associe, rien a dessiner
            continue;

        // recuperer la transformation pour placer le mesh dans la scene
        float model_matrix[16];

        cgltf_node_transform_world(node, model_matrix);
        for (int i = 0; i < 16; i++) {
            if (!(model_matrix[i] > 0.000001 || model_matrix[i] < -0.000001)) {
                model_matrix[i] = 0;
            }
        }
        if (node_id == 43) {
            int x = 1;
        }
        Object obj;
        glm::mat4 model = glm::make_mat4(model_matrix);
        // on transforme la matrice de transformation pour obtenir les paramètre de transformation affin de pouvoir bouger les objets
        decomposeTransform(model, obj.transform);
        glm::mat4 test = obj.transform.mat4();
        // cout the diff between the two matrix
        assert(test == model && "shit");
        std::cout << glm::to_string(model - test) << std::endl;

        // model.column_major(model_matrix);  // gltf organise les 16 floats par colonne...
        //~ Transform normal= model.normal();       // transformation pour les normales

        int mesh_index = std::distance(data->meshes, node->mesh);
        obj.mesh_id = mesh_index;
        scene.objects.push_back(obj);
    }

    std::vector<bool> isAlbedoTexture(data->images_count, true);

    // etape 3 : recuperer les autres infos...
    scene.materials = read_materials(data, isAlbedoTexture);
    scene.lights = read_lights(data);
    scene.cameras = read_cameras(data);
    scene.textures = read_gltf_images(device, data, isAlbedoTexture);
    Camera cam{};
    if (scene.cameras.size() == 0) {
        cam.viewerObject.transform.translation = {8, 13, 0};
        cam.viewerObject.transform.rotation = {-3.14 / 4.0, 3.14 / 2.0, 0};
        scene.cameras.push_back(cam);
    }
    scene.cameras[0] = cam;

    // etape : nettoyage...
    cgltf_free(data);

    return scene;
}

// std::vector<GLTFInstances> GLTFLoader::instances() const {
//     std::vector<GLTFInstances> instances(meshes.size());
//     for (unsigned i = 0; i < meshes.size(); i++) instances[i].mesh_index = i;

//     for (unsigned i = 0; i < nodes.size(); i++) {
//         int index = nodes[i].mesh_index;
//         assert(index < int(instances.size()));
//         instances[index].transforms.push_back(nodes[i].model);
//     }

//     return instances;
// }

// void GLTFLoader::bounds(Point &pmin, Point &pmax) const {
//     pmin = Point(FLT_MAX, FLT_MAX, FLT_MAX);
//     pmax = Point(-FLT_MAX, -FLT_MAX, -FLT_MAX);
//     for (unsigned node_id = 0; node_id < nodes.size(); node_id++) {
//         const GLTFNode &node = nodes[node_id];
//         const GLTFMesh &mesh = meshes[node.mesh_index];
//         for (unsigned primitive_id = 0; primitive_id < mesh.primitives.size(); primitive_id++) {
//             const GLTFPrimitives &primitives = mesh.primitives[primitive_id];
//             for (unsigned i = 0; i < primitives.positions.size(); i++) {
//                 //~ Point p= node.model(Point(primitives.positions[i]));
//                 Point p = Point(primitives.positions[i]);
//                 pmin = min(pmin, p);
//                 pmax = max(pmax, p);
//             }
//         }
//     }
// }
}  // namespace vk_stage