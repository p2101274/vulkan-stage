#pragma once
// Copyright (c) 2020 Brendan Galea - https://github.com/blurrypiano/littleVulkanEngine

// libs
#include <glm/ext/quaternion_common.hpp>
#include "scene/object.hpp"
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

namespace vk_stage {

class Camera {
   public:
    void setOrthographicProjection(float left, float right, float top, float bottom, float near, float far);
    void setPerspectiveProjection(float fovy, float aspect, float near, float far);

    void setViewDirection(glm::vec3 position, glm::vec3 direction, glm::vec3 up = glm::vec3{0.f, -1.f, 0.f});
    void setViewTarget(glm::vec3 position, glm::vec3 target, glm::vec3 up = glm::vec3{0.f, -1.f, 0.f});
    void setViewYXZ(glm::vec3 position, glm::vec3 rotation);
    void setViewMatrix(glm::mat4 newViewMatrix){
        viewMatrix = newViewMatrix;
        inverseViewMatrix = glm::inverse(viewMatrix);
     }

     
    
    const glm::mat4& getProjection() const { return projectionMatrix; }
    const glm::mat4& getView() const { return viewMatrix; }
    const glm::mat4& getInverseView() const { return inverseViewMatrix; }
    const glm::vec3 getPosition() const { return glm::vec3(inverseViewMatrix[3]); }
    Object viewerObject;
   private:
    glm::mat4 projectionMatrix{1.f};
    glm::mat4 viewMatrix{1.f};
    glm::mat4 inverseViewMatrix{1.f};
};
}  // namespace vk_stage