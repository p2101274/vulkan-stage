
#include "scene.hpp"

#include <vulkan/vulkan_core.h>

#include <cstddef>
#include <cstdint>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <iostream>

#include "commandBuffer/commandBufferPoolHandler.hpp"
#include "scene/object.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include <vector>

#include "renderer.hpp"
#include "ressources/buffer.hpp"
#include "ressources/image.hpp"
#include "structs_vk.hpp"
#include "utils.hpp"

namespace vk_stage {

void Scene::destroy() {
    for (auto &m : meshes) {
        m.destroy();
    }
    for (auto &t : textures) {
        t.destroy();
    }
    for (auto &bl : uboBuffers) {
        for (auto &b : bl) {
            b.destroy();
        }
    }
    if (baseSampler != VK_NULL_HANDLE) vkDestroySampler(device->device(), baseSampler, nullptr);
    materialBuffer->destroy();
    delete materialBuffer;
    sceneDescriptorPool.destroy();
    meshBlockBuffer.destroy();
    instanceBuffer.destroy();
    vertexBuffer->destroy();
    IndexBuffer->destroy();
}

void Scene::genBufferAndDescriptorData(
    std::shared_ptr<DescriptorSetLayout> uboSetLayout, std::shared_ptr<DescriptorSetLayout> sceneSetLayout) {
    genUboBuffers(uboSetLayout);
    genMaterialBuffers();
    genInstanceBuffer();
    genSceneDescriptorSet(sceneSetLayout);
}

void Scene::genUboBuffers(std::shared_ptr<DescriptorSetLayout> uboSetLayout) {
    uboBuffers.resize(MAX_FRAMES_IN_FLIGHT);
    uboDescriptorsSests.resize(MAX_FRAMES_IN_FLIGHT);
    for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        for (auto &cam : cameras) {
            GlobalUbo camData{cam.getProjection(), cam.getView(), cam.getInverseView()};
            uboBuffers[i].push_back(Buffer(
                device, sizeof(GlobalUbo), 1, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT));
            uboBuffers[i].back().map();
            uboBuffers[i].back().writeToBuffer(&camData);
            uboBuffers[i].back().flush();

            uboDescriptorsSests[i].push_back(sceneDescriptorPool.allocateDescriptorSet(uboSetLayout));
            auto bufferInfo = uboBuffers[i].back().descriptorInfo();
            uboDescriptorsSests[i].back().writeBuffer(0, &bufferInfo);
            uboDescriptorsSests[i].back().writeToDescriptorSet();
        }
    }
}

void Scene::genMaterialBuffers() {
    materialBuffer = new Buffer(
        device, sizeof(Material), materials.size(), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    materialBuffer->map();
    materialBuffer->writeToBuffer(materials.data());
    materialBuffer->flush();

    baseSampler = Image::createSampler(device, 8);

    meshBlockBuffer = Buffer(
        device, sizeof(MeshBlock), 200000 + 4, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    int i = 0;
    uint32_t meshBlockNumber = 0;

    for (auto &obj : objects) {
        if (obj.objType != objectType::mesh) continue;
        for (auto meshBlock : meshes[obj.mesh_id].meshBlockList) {
            meshBlock.instancesID = i;
            meshBlockList.push_back(meshBlock);
            meshBlockNumber++;
        }
        i++;
    }

    meshBlockBuffer.map(meshBlockList.size() * sizeof(MeshBlock) + 64 - ((instanceInfoList.size() * sizeof(MeshBlock)) % 64));
    meshBlockBuffer.writeToBuffer(meshBlockList.data(), meshBlockList.size() * sizeof(MeshBlock));
    meshBlockBuffer.flush(instanceInfoList.size() * sizeof(MeshBlock) + 64 - ((instanceInfoList.size() * sizeof(MeshBlock)) % 64));
}

void Scene::genInstanceBuffer() {
    uint i = 0;
    for (auto &obj : objects) {
        instanceInfoList.push_back({obj.transform.mat4(), obj.transform.normalMatrix()});

        obj.instanceIndex = i;
        i++;
    }
    instanceBuffer = Buffer(
        device, sizeof(ObjectInfo), 100000, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT);
    instanceBuffer.map(instanceInfoList.size() * sizeof(ObjectInfo) + (instanceInfoList.size() * sizeof(ObjectInfo) % 64));
    instanceBuffer.writeToBuffer(instanceInfoList.data(), instanceInfoList.size() * sizeof(ObjectInfo));
    instanceBuffer.flush(instanceInfoList.size() * sizeof(ObjectInfo));
}

void Scene::genSceneDescriptorSet(std::shared_ptr<DescriptorSetLayout> sceneSetLayout) {
    sceneDescriptorsSet = sceneDescriptorPool.allocateDescriptorSet(sceneSetLayout);
    auto samplerInfo = make<VkDescriptorImageInfo>();
    samplerInfo.sampler = baseSampler;

    std::vector<VkDescriptorBufferInfo> bufferInfos(materials.size());
    std::vector<VkDescriptorImageInfo> imageInfos(textures.size());
    for (int i = 0; i < materials.size(); i++) {
        bufferInfos[i] = materialBuffer->descriptorInfoForIndex(i);
    }
    for (int i = 0; i < textures.size(); i++) {
        imageInfos[i] = textures[i].descriptorInfo();
    }

    sceneDescriptorsSet.writeImage(0, &samplerInfo);
    sceneDescriptorsSet.writeBuffers(1, &bufferInfos);
    sceneDescriptorsSet.writeImages(2, &imageInfos);
    sceneDescriptorsSet.writeToDescriptorSet();
}

void Scene::buildAccelerationStructure(VkBuildAccelerationStructureModeKHR buildMode, VkCommandBuffer cmdBuffer) {
    std::vector<VkAccelerationStructureInstanceKHR> instanceDataList;
    std::vector<VkAccelerationStructureGeometryKHR> objectList;

    // Création des info pour chaque instance
    for (auto &obj : objects) {
        auto instanceInfo = make<VkAccelerationStructureInstanceKHR>();
        auto m = obj.transform.mat4();
        instanceInfo.transform = {obj.transform.mat4()[0][0], obj.transform.mat4()[1][0], obj.transform.mat4()[2][0],
                                  obj.transform.mat4()[3][0], obj.transform.mat4()[0][1], obj.transform.mat4()[1][1],
                                  obj.transform.mat4()[2][1], obj.transform.mat4()[3][1], obj.transform.mat4()[0][2],
                                  obj.transform.mat4()[1][2], obj.transform.mat4()[2][2], obj.transform.mat4()[3][2]};
        instanceInfo.mask = 0xFF;
        instanceInfo.instanceShaderBindingTableRecordOffset = obj.objType;
        instanceInfo.instanceCustomIndex = obj.customObjMaterial;
        instanceInfo.flags = 0;
        if (obj.objType == objectType::mesh) {
            instanceInfo.accelerationStructureReference = meshes[obj.mesh_id].accelerationStructureDeviceAddress;
        } else {
            instanceInfo.accelerationStructureReference = sphere.accelerationStructureDeviceAddress;
        }

        instanceDataList.push_back(instanceInfo);
    }
    // Buffer + large par défaut pour ajouter des instance + tard
    // TODO : gestion dynamique du buffer serait cool
    accelerationInstanceBuffer = Buffer{
        device,
        sizeof(VkAccelerationStructureInstanceKHR),
        uint32_t(10000),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR};

    accelerationInstanceBuffer.map();
    accelerationInstanceBuffer.writeToBuffer(instanceDataList.data(), sizeof(VkAccelerationStructureInstanceKHR) * instanceDataList.size());
    accelerationInstanceBuffer.flush();

    // Création des information pour la création de l'acceleration structure
    accelerationStructureObjectInfo = make<VkAccelerationStructureGeometryKHR>();
    accelerationStructureObjectInfo.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    accelerationStructureObjectInfo.flags = 0;
    accelerationStructureObjectInfo.geometry.instances.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    accelerationStructureObjectInfo.geometry.instances.arrayOfPointers = VK_FALSE;
    accelerationStructureObjectInfo.geometry.instances.data.deviceAddress = accelerationInstanceBuffer.getBufferDeviceAddress();

    accelerationStructureObjectBuildInfo = make<VkAccelerationStructureBuildGeometryInfoKHR>();
    accelerationStructureObjectBuildInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    accelerationStructureObjectBuildInfo.flags =
        VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR;
    accelerationStructureObjectBuildInfo.geometryCount = 1;
    accelerationStructureObjectBuildInfo.pGeometries = &accelerationStructureObjectInfo;

    uint32_t primitive_count = instanceDataList.size();

    accelerationStructureBuildRange = make<VkAccelerationStructureBuildRangeInfoKHR>();
    accelerationStructureBuildRange.primitiveCount = instanceDataList.size();
    accelerationStructureBuildRange.primitiveOffset = 0;
    accelerationStructureBuildRange.firstVertex = 0;
    accelerationStructureBuildRange.transformOffset = 0;

    // Récupération des taille pour la création et le stockage pour l'acceleration structure

    accelerationStructureBuildSizesInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    accelerationStructureBuildSizesInfo.pNext = NULL;
    vkGetAccelerationStructureBuildSizesKHR(
        device->device(), VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &accelerationStructureObjectBuildInfo, &primitive_count,
        &accelerationStructureBuildSizesInfo);

    accelerationStructureBuffer = Buffer(
        device, accelerationStructureBuildSizesInfo.accelerationStructureSize, 1,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    // Créé l'acceleration structure
    VkAccelerationStructureCreateInfoKHR accelerationStructureCreateInfo{};
    accelerationStructureCreateInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    accelerationStructureCreateInfo.buffer = accelerationStructureBuffer.getBuffer();
    accelerationStructureCreateInfo.size = accelerationStructureBuildSizesInfo.accelerationStructureSize;
    accelerationStructureCreateInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;

    vkCreateAccelerationStructureKHR(device->device(), &accelerationStructureCreateInfo, nullptr, &accelerationStructureHandle);

    Buffer scratchBuffer(
        device, accelerationStructureBuildSizesInfo.buildScratchSize, 1,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    accelerationStructureObjectBuildInfo.srcAccelerationStructure = VK_NULL_HANDLE;
    accelerationStructureObjectBuildInfo.dstAccelerationStructure = accelerationStructureHandle;
    accelerationStructureObjectBuildInfo.scratchData.deviceAddress = scratchBuffer.getBufferDeviceAddress();
    // BUILD de l'acceleration structure

    VkAccelerationStructureBuildRangeInfoKHR *rangePtr = &accelerationStructureBuildRange;

    VkCommandBuffer cmd = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();

    vkCmdBuildAccelerationStructuresKHR(cmd, 1, &accelerationStructureObjectBuildInfo, &rangePtr);

    VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo{};
    accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    accelerationDeviceAddressInfo.accelerationStructure = accelerationStructureHandle;
    accelerationStructureDeviceAddress = vkGetAccelerationStructureDeviceAddressKHR(device->device(), &accelerationDeviceAddressInfo);

    CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(cmd);
}

void Scene::updateInstanceData(VkCommandBuffer cmdBuffer) {
    bool updated = false;
    for (auto &obj : objects) {
        if (obj.needGPUupdate) {
            ObjectInfo OI = {obj.transform.mat4(), obj.transform.normalMatrix()};
            instanceBuffer.writeToIndex(&OI, obj.instanceIndex);

            auto instanceInfo = make<VkAccelerationStructureInstanceKHR>();
            auto m = obj.transform.mat4();
            instanceInfo.transform = {obj.transform.mat4()[0][0], obj.transform.mat4()[1][0], obj.transform.mat4()[2][0],
                                      obj.transform.mat4()[3][0], obj.transform.mat4()[0][1], obj.transform.mat4()[1][1],
                                      obj.transform.mat4()[2][1], obj.transform.mat4()[3][1], obj.transform.mat4()[0][2],
                                      obj.transform.mat4()[1][2], obj.transform.mat4()[2][2], obj.transform.mat4()[3][2]};
            instanceInfo.mask = 0xFF;
            instanceInfo.instanceShaderBindingTableRecordOffset = obj.objType;
            instanceInfo.instanceCustomIndex = obj.customObjMaterial;
            instanceInfo.flags = 0;
            if (obj.objType == objectType::mesh) {
                instanceInfo.accelerationStructureReference = meshes[obj.mesh_id].accelerationStructureDeviceAddress;
            } else {
                instanceInfo.accelerationStructureReference = sphere.accelerationStructureDeviceAddress;
            }

            accelerationInstanceBuffer.writeToIndex(&instanceInfo, obj.instanceIndex);
            obj.needGPUupdate = false;
            updated = true;
        }
    }

    if (updated) {
        if (scratchBuffer.getBuffer() == VK_NULL_HANDLE) {
            scratchBuffer = Buffer(
                device, accelerationStructureBuildSizesInfo.buildScratchSize, 1,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);
        }

        accelerationStructureObjectBuildInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        accelerationStructureObjectBuildInfo.srcAccelerationStructure = VK_NULL_HANDLE;
        accelerationStructureObjectBuildInfo.dstAccelerationStructure = accelerationStructureHandle;
        accelerationStructureObjectBuildInfo.scratchData.deviceAddress = scratchBuffer.getBufferDeviceAddress();
        // BUILD de l'acceleration structure

        VkAccelerationStructureBuildRangeInfoKHR *rangePtr = &accelerationStructureBuildRange;

        vkCmdBuildAccelerationStructuresKHR(cmdBuffer, 1, &accelerationStructureObjectBuildInfo, &rangePtr);

        VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo{};
        accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
        accelerationDeviceAddressInfo.accelerationStructure = accelerationStructureHandle;
        accelerationStructureDeviceAddress = vkGetAccelerationStructureDeviceAddressKHR(device->device(), &accelerationDeviceAddressInfo);
    }
}

void Scene::createSphereAccelerationStructure() {
    auto ASGS = make<VkAccelerationStructureGeometryKHR>();
    BoundBox b;
    b.pmax = glm::vec3(1.);
    b.pmin = glm::vec3(-1.);

    Buffer aabbBuff = Buffer(
        device, sizeof(BoundBox), 1,
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);
    aabbBuff.map();
    aabbBuff.writeToBuffer(&b);
    aabbBuff.flush();

    ASGS.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    // Instead of providing actual geometry (e.g. triangles), we only provide the axis aligned bounding boxes (AABBs) of the spheres
    // The data for the actual spheres is passed elsewhere as a shader storage buffer object
    ASGS.geometryType = VK_GEOMETRY_TYPE_AABBS_KHR;
    ASGS.geometry.aabbs.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR;
    ASGS.geometry.aabbs.data.deviceAddress = aabbBuff.getBufferDeviceAddress();
    ASGS.geometry.aabbs.stride = sizeof(BoundBox);

    auto GBAS = make<VkAccelerationStructureBuildGeometryInfoKHR>();
    GBAS.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    GBAS.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    GBAS.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    GBAS.geometryCount = 1;
    GBAS.pGeometries = &ASGS;
    GBAS.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;

    auto ASBRS = make<VkAccelerationStructureBuildRangeInfoKHR>();
    ASBRS.primitiveCount = 1;

    auto ASBSIS = make<VkAccelerationStructureBuildSizesInfoKHR>();
    ASBSIS.pNext = nullptr;

    vkGetAccelerationStructureBuildSizesKHR(
        device->device(), VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR, &GBAS, &ASBRS.primitiveCount, &ASBSIS);

    sphere.accelerationStructureBuffer = Buffer(
        device, ASBSIS.accelerationStructureSize, 1,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    auto ASCI = make<VkAccelerationStructureCreateInfoKHR>();
    ASCI.buffer = sphere.accelerationStructureBuffer.getBuffer();
    ASCI.size = ASBSIS.accelerationStructureSize;
    ASCI.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;

    vkCreateAccelerationStructureKHR(device->device(), &ASCI, nullptr, &sphere.accelerationStructureHandle);

    auto accelerationScratchBuffer = Buffer(
        device, ASBSIS.buildScratchSize, 1, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT_KHR);

    GBAS.srcAccelerationStructure = VK_NULL_HANDLE;
    GBAS.dstAccelerationStructure = sphere.accelerationStructureHandle;

    GBAS.scratchData.deviceAddress = accelerationScratchBuffer.getBufferDeviceAddress();

    auto cmd = CommandBufferPoolHandler::defaultPool(device)->beginSingleTimeCommand();

    const VkAccelerationStructureBuildRangeInfoKHR *buildOffsetInfo = &ASBRS;

    vkCmdBuildAccelerationStructuresKHR(cmd, 1, &GBAS, &buildOffsetInfo);

    VkAccelerationStructureDeviceAddressInfoKHR accelerationDeviceAddressInfo{};
    accelerationDeviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    accelerationDeviceAddressInfo.accelerationStructure = sphere.accelerationStructureHandle;

    sphere.accelerationStructureDeviceAddress =
        vkGetAccelerationStructureDeviceAddressKHR(device->device(), &accelerationDeviceAddressInfo);

    CommandBufferPoolHandler::defaultPool(device)->endSingleTimeCommand(cmd);
    accelerationScratchBuffer.destroy();
}

}  // namespace vk_stage