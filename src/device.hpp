#pragma once

#include "volk.h"

#include <cstdint>

#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"

#include "window.hpp"
namespace vk_stage {
class Device {
   public:
    Device(Window &window);
    ~Device();

    VkDevice device() { return vkbDevice.device; }
    VkPhysicalDevice physical_device() { return vkbDevice.physical_device; }
    vkb::Device getvkbDevice() { return vkbDevice; }

    vkb::Result<VkQueue> get_dedicated_queue(vkb::QueueType type) { return vkbDevice.get_dedicated_queue(type); }
    vkb::Result<uint32_t> get_dedicated_queue_index(vkb::QueueType type) { return vkbDevice.get_dedicated_queue_index(type); }
    vkb::Result<VkQueue> get_queue(vkb::QueueType type) { return vkbDevice.get_queue(type); }
    vkb::Result<uint32_t> get_queue_index(vkb::QueueType type) { return vkbDevice.get_queue_index(type); }
    VkSurfaceKHR surface() { return vkbDevice.surface; }

    // VkCommandBuffer beginSingleTimeCommand() { return defaultPool->beginSingleTimeCommand(); };
    
    // void endSingleTimeCommand2(VkCommandBuffer vkcommandBuffer) { defaultPool->endSingleTimeCommand2(vkcommandBuffer); };
    
    // std::vector<CommandBuffer> createCommandBuffer(unsigned int commandBufferCount) {
    //     return defaultPool->createCommandBuffer(commandBufferCount);
    // }

    // vkb::Device getvkbDevice(){ return vkbDevice;}

    uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
    VkFormat findSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

    // CommandBufferPoolHandler getDefaultPool() { return *defaultPool; }
    bool RTenabled = false;
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR  rayTracingPipelineProperties;
   private:

#ifdef NDEBUG
    const bool enableValidationLayers = false;
#else
    const bool enableValidationLayers = true;
#endif

    void getProperties();
    void createInstance();
    void checkIfRayTracingSupported();
    void setRequiredExtensions(vkb::PhysicalDeviceSelector &phys_device_selector);
    void setRequiredFeatures(vkb::PhysicalDeviceSelector &phys_device_selector);

    vkb::Device vkbDevice;
    vkb::Instance vkbInstance;
    vkb::PhysicalDevice vkPhysicalDevice;
    VkSurfaceKHR vkSurface;
    // CommandBufferPoolHandler *defaultPool;
};
}  // namespace vk_stage