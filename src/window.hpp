#pragma once

#include <glm/glm.hpp>
#include "volk.h"

#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <string>

namespace vk_stage {
class Window {
   public:
    Window(unsigned int width, unsigned int height, std::string name);
    ~Window();

    /**
     * @brief Renvoie un handler Vulkan de surface qui sert à créer la swapChain
     *
     * @param vkInstance
     * @return VkSurfaceKHR
     */
    VkSurfaceKHR getSurface(vkb::Instance &vkInstance);

    bool shouldClose() { return glfwWindowShouldClose(window); }

    /**
     * @brief Renvoie la taille de la fenêtre
     *
     * @return VkExtent2D
     */
    VkExtent2D getExtent() { return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)}; }
    bool wasWindowResized() { return framebufferResized; }
    void resetWindowResizedFlag() { framebufferResized = false; }
    GLFWwindow *getGLFWwindow() const { return window; }
    double mousePosx = -1;
    double mousePosy = -1;
    glm::vec3 mouseDirection = {0,0,0};
   private:
    /**
     * @brief Met à jour les information de la fenêtre lorsqu'elle est redimensionné
     *
     * @param window pointeur vers la nouvelle fenêtre donné par glfw
     * @param width
     * @param height
     */
    static void framebufferResizeCallback(GLFWwindow *window, int width, int height);

    unsigned int width;
    unsigned int height;

    bool framebufferResized = false;

    GLFWwindow *window;
};
}  // namespace vk_stage