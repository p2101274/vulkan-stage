
#include "device.hpp"

#include <vulkan/vulkan_core.h>

#include <chrono>
#include <iostream>
#define VK_NO_PROTOTYPES
#include "VkBootstrap.h"
#include "structs_vk.hpp"

namespace vk_stage {

Device::Device(Window &window) {
    volkInitialize();
    auto system_info_ret = vkb::SystemInfo::get_system_info();
    if (!system_info_ret) throw std::runtime_error(system_info_ret.error().message());

    auto system_info = system_info_ret.value();

    // init instance
    createInstance();
    vkSurface = window.getSurface(vkbInstance);

    checkIfRayTracingSupported();

    // init Physical Device

    vkb::PhysicalDeviceSelector phys_device_selector(vkbInstance);
    phys_device_selector.set_surface(vkSurface);
    setRequiredExtensions(phys_device_selector);
    setRequiredFeatures(phys_device_selector);

    auto physical_device_selector_return = phys_device_selector.select();
    if (!physical_device_selector_return) {
        throw std::runtime_error(physical_device_selector_return.error().message());
    }

    vkPhysicalDevice = std::move(physical_device_selector_return.value());
    vkPhysicalDevice.enable_extension_if_present("VK_EXT_shader_object");
    // vkPhysicalDevice.enable_extension_if_present("VK_KHR_ray_tracing_pipeline");
    // vkPhysicalDevice.enable_extension_if_present("VK_KHR_acceleration_structure");
    // vkPhysicalDevice.enable_extension_if_present("VK_KHR_deferred_host_operations");
    // vkPhysicalDevice.enable_extension_if_present("VK_KHR_ray_query");

    // init device
    vkb::DeviceBuilder device_builder{vkPhysicalDevice};

    auto dev_ret = device_builder.build();
    if (!dev_ret) {
        throw std::runtime_error(dev_ret.error().message());
    }
    this->vkbDevice = std::move(dev_ret.value());

    // Vérification du minimum de queue
    auto graphics_queue_ret = vkbDevice.get_queue(vkb::QueueType::graphics);
    if (!graphics_queue_ret) {
        throw std::runtime_error(graphics_queue_ret.error().message());
    }
    auto present_queue_ret = vkbDevice.get_queue(vkb::QueueType::present);
    if (!present_queue_ret) {
        throw std::runtime_error(present_queue_ret.error().message());
    }
    // this->defaultPool = new CommandBufferPoolHandler(vkbDevice, vkb::QueueType::graphics);

    getProperties();
}

Device::~Device() {
    vkb::destroy_device(vkbDevice);
    vkb::destroy_surface(vkbInstance, vkSurface);
    vkb::destroy_instance(vkbInstance);
}

void Device::createInstance() {
    vkb::InstanceBuilder instance_builder;
    instance_builder.set_app_name("Hello Vulkan")
        .set_engine_name("Je sais pas, dans tout les cas personne ira regarder")
        .require_api_version(1, 3, 0);

    if (enableValidationLayers) {
        instance_builder.request_validation_layers()
            .use_default_debug_messenger();  //.add_validation_feature_enable(VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT);
    }

    auto instance_builder_return = instance_builder.build();

    if (!instance_builder_return) {
        throw std::runtime_error(instance_builder_return.error().message());
    }
    vkbInstance = std::move(instance_builder_return.value());
    volkLoadInstance(vkbInstance.instance);
}

void Device::checkIfRayTracingSupported() {
    vkb::PhysicalDeviceSelector phys_device_selector(vkbInstance);
    phys_device_selector.set_surface(vkSurface);
    phys_device_selector.add_required_extension("VK_KHR_acceleration_structure");

    auto physical_device_selector_return = phys_device_selector.select();
    if (physical_device_selector_return) {
        RTenabled = true;
        std::cout << "Ray tracing is supported" << std::endl;
    }
}

void Device::setRequiredExtensions(vkb::PhysicalDeviceSelector &phys_device_selector) {
    if (RTenabled) {
        phys_device_selector.add_required_extension("VK_KHR_ray_tracing_pipeline");
        phys_device_selector.add_required_extension("VK_KHR_acceleration_structure");
        phys_device_selector.add_required_extension("VK_KHR_ray_query");
        phys_device_selector.add_required_extension("VK_KHR_deferred_host_operations");
    }
    phys_device_selector.add_required_extension("VK_EXT_shader_object");
}

void setRequiredExtensionsFeatures(vkb::PhysicalDeviceSelector &phys_device_selector, bool RTenable) {
    VkPhysicalDeviceShaderObjectFeaturesEXT shaderObjFeature{};
    shaderObjFeature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_OBJECT_FEATURES_EXT;
    shaderObjFeature.shaderObject = true;

    phys_device_selector.add_required_extension_features(shaderObjFeature);
    if (RTenable) {
        VkPhysicalDeviceAccelerationStructureFeaturesKHR AccelerationStructurFeature{};
        AccelerationStructurFeature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
        AccelerationStructurFeature.accelerationStructure = true;
        phys_device_selector.add_required_extension_features(AccelerationStructurFeature);

        VkPhysicalDeviceRayQueryFeaturesKHR rayQueryFeature{};
        rayQueryFeature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
        rayQueryFeature.rayQuery = true;

        phys_device_selector.add_required_extension_features(rayQueryFeature);

        VkPhysicalDeviceRayTracingPipelineFeaturesKHR rayTraceFeature{};
        rayTraceFeature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
        rayTraceFeature.rayTracingPipeline = true;
        phys_device_selector.add_required_extension_features(rayTraceFeature);
    }
}

// Features paramater

void setRequiredFeatures10(vkb::PhysicalDeviceSelector &phys_device_selector) {
    auto required_features = make<VkPhysicalDeviceFeatures>();
    required_features.samplerAnisotropy = true;
    phys_device_selector.set_required_features(required_features);
}

void setRequiredFeatures11(vkb::PhysicalDeviceSelector &phys_device_selector) {
    auto required_features = make<VkPhysicalDeviceVulkan11Features>();
    phys_device_selector.set_required_features_11(required_features);
}

void setRequiredFeatures12(vkb::PhysicalDeviceSelector &phys_device_selector) {
    auto required_features12 = make<VkPhysicalDeviceVulkan12Features>();
    required_features12.descriptorBindingPartiallyBound = true;
    required_features12.runtimeDescriptorArray = true;
    required_features12.descriptorBindingVariableDescriptorCount = true;
    required_features12.drawIndirectCount = true;
    required_features12.samplerFilterMinmax = true;
    required_features12.bufferDeviceAddress = true;
    required_features12.descriptorIndexing = true;
    required_features12.scalarBlockLayout = true;

    phys_device_selector.set_required_features_12(required_features12);
}

void setRequiredFeatures13(vkb::PhysicalDeviceSelector &phys_device_selector) {
    auto required_features13 = make<VkPhysicalDeviceVulkan13Features>();
    required_features13.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
    required_features13.dynamicRendering = true;

    phys_device_selector.set_required_features_13(required_features13);
}

void Device::setRequiredFeatures(vkb::PhysicalDeviceSelector &phys_device_selector) {
    setRequiredExtensionsFeatures(phys_device_selector, RTenabled);
    setRequiredFeatures10(phys_device_selector);
    setRequiredFeatures12(phys_device_selector);
    setRequiredFeatures13(phys_device_selector);
}

// Device properties

uint32_t Device::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(vkbDevice.physical_device, &memProperties);
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }
    throw std::runtime_error("failed to find suitable memory type!");
}

VkFormat Device::findSupportedFormat(const std::vector<VkFormat> &candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(vkbDevice.physical_device, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
            return format;
        } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }
    throw std::runtime_error("failed to find supported format!");
}

void Device::getProperties() {
    rayTracingPipelineProperties = make<VkPhysicalDeviceRayTracingPipelinePropertiesKHR>();
    VkPhysicalDeviceProperties2 deviceProperties2{};
    deviceProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    deviceProperties2.pNext = &rayTracingPipelineProperties;
    vkGetPhysicalDeviceProperties2(vkbDevice.physical_device.physical_device, &deviceProperties2);
}
}  // namespace vk_stage