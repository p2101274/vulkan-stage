
#include "hot_reload.hpp"

#include <iostream>

#include "VkBootstrap.h"
#include "device.hpp"
#include "renderer.hpp"
#include "shader/shader.hpp"

namespace vk_stage {

Device* HotReload::device = nullptr;
std::mutex HotReload::m;
std::thread HotReload::uv_thread;
std::thread HotReload::uv_threadSPV;

uv_fs_event_t HotReload::fs_event;
uv_fs_event_t HotReload::fs_eventSPV;
uv_loop_t HotReload::loop;
uv_loop_t HotReload::loopSPV;
std::map<std::string, std::vector<Pipeline*>> HotReload::shaderPipelineMap;

void HotReload::init(Device* device) {
    HotReload::device = device;
    uv_loop_init(&loop);

    uv_fs_event_init(&loop, &fs_event);

    // Spécifiez le dossier à surveiller et la fonction de rappel
    uv_fs_event_start(&fs_event, on_file_change, "../shaders", 0);

    uv_thread = std::thread([]() { uv_run(&loop, UV_RUN_DEFAULT); });

    uv_loop_init(&loopSPV);

    uv_fs_event_init(&loopSPV, &fs_eventSPV);

    // Spécifiez le dossier à surveiller et la fonction de rappel
    uv_fs_event_start(&fs_eventSPV, on_file_changeSPV, "../shaders/spirv", 0);

    uv_threadSPV = std::thread([]() { uv_run(&loopSPV, UV_RUN_DEFAULT); });
}

void HotReload::stop() {
    uv_stop(&loop);

    // Libérer les ressources
    uv_fs_event_stop(&fs_event);
    uv_loop_close(&loop);
    uv_thread.join();  // Attendre la fin du thread

    uv_stop(&loopSPV);

    // Libérer les ressources
    uv_fs_event_stop(&fs_eventSPV);
    uv_loop_close(&loopSPV);
    uv_threadSPV.join();  // Attendre la fin du thread
}

void HotReload::addShaderToWatch(std::string shaderName, Pipeline* pipelineToNotify) {
    shaderPipelineMap[shaderName].push_back(pipelineToNotify);
}

void HotReload::reloadShaders(std::string name) {
    for (auto& pipeline : shaderPipelineMap[name]) {
        pipeline->reloadShader(Shader::getShaderStageFlagsBitFromFileName(name));
    }
}

inline bool ends_with(std::string const& value, std::string const& ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

void HotReload::on_file_change(uv_fs_event_t* handle, const char* filename, int events, int status) {
    m.lock();
    if (status == 0) {
        std::string filenameStr = std::string(filename);

        // create command to compile shader
        std::string command = "glslc --target-env=vulkan1.3 ../shaders/" + filenameStr + " -o ../shaders/spirv/" + filenameStr + ".spv";
        system(command.c_str());
        std::string shaderName = filenameStr.substr(0, filenameStr.find_first_of("."));
    

    } else {
        fprintf(stderr, "Erreur lors de la surveillance du fichier: %s\n", uv_strerror(status));
    }
    m.unlock();
}

void HotReload::on_file_changeSPV(uv_fs_event_t* handle, const char* filename, int events, int status) {
    m.lock();
    if (status == 0) {
        std::string filenameStr = std::string(filename);

        Renderer::stopRendering = true;
        vkDeviceWaitIdle(device->device());
        vkQueueWaitIdle(device->get_queue(vkb::QueueType::graphics).value());

        std::string st = filenameStr.substr(0, filenameStr.size() - 4);
        reloadShaders(st);
        Renderer::stopRendering = false;
        Renderer::waitToStartRendering.notify_all();

    } else {
        fprintf(stderr, "Erreur lors de la surveillance du fichier: %s\n", uv_strerror(status));
    }
    m.unlock();
}

}  // namespace vk_stage